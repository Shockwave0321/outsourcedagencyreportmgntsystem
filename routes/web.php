<?php

Auth::routes();

Route::get('/unauthorized', function() {
    return view('vendor.adminlte.errors.404');
});

// ====== LOGINS ======= //

// maker
Route::get('/', function () {
    return view('adminlte::auth.login');
})->name('maker.login');
// approver
Route::get('/approver', function () {
    return view('adminlte::auth.approver-login');
})->name('approver.login');
// sysadmin
Route::get('/sysadmin', function () {
    return view('adminlte::auth.sysadmin-login');
})->name('sysadmin.login');
// secadmin
Route::get('/secadmin', function () {
    return view('adminlte::auth.secadmin-login');
})->name('secadmin.login');
Route::post('logout-secadmin','Auth\SecadminLoginController@logoutSecadmin')->name('logout.secadmin.submit');


// MAKER
// =========
Route::get('/dashboard', 'HomeController@dashboard')->name('app.dashboard');
Route::get('/employee-list', 'EmployeeController@getEmployees')->name('show.employee.list');
Route::get('/employee/create', 'EmployeeController@createNewEmployee')->name('employee.create');
Route::get('/pending-requests', 'EmployeeController@showPendingRequests')->name('show.pending.request');
Route::get('/timesheet-list', 'TimesheetManagerController@showTimesheetList')->name('show.timesheet.list');


Route::post('insert-new-employee','EmployeeController@insertNewEmployee');
Route::post('update-employee', ['uses' => 'EmployeeController@updateEmployee']);
Route::post('insert-new-timelog','TimesheetManagerController@insertNewTimeLog');

// HO Maker
Route::get('/agency-list', 'AgencyController@getAgencies')->name('admin.agency.list');
Route::get('/create-new-agency', 'AgencyController@createAgency')->name('create.new.agency');
Route::post('insert-new-agency','AgencyController@insertNewAgency')->name('insert.agency');
Route::post('update-agency', ['uses' => 'AgencyController@updateAgency']);

Route::get('/exception/upload-report','ReportController@showEmployeeUploadException');
Route::post('generate', ['uses' => 'ReportController@displayReport']);
Route::get('generate-pdf','ReportController@generatePDF');

// end: MAKER
// ==========
//


// APPROVER
Route::post('loginapprover', 'Auth\ApproverLoginController@loginApprover')->name('approver.login.submit');
Route::get('/approver-dashboard', 'ApproverController@index')->name('approver.dashboard');
Route::get('/review-employee-details/{employeeNo}', 'ApproverController@showReviewEmployeeDetails');
Route::get('/review-agency-details/{agencyCode}', 'ApproverController@showReviewAgencyDetails');

Route::post('approve-request', 'ApproverController@approveRequest')->name('approve.request');
Route::post('approve-agency-request', 'ApproverController@approvedRequestedAgency')->name('approve.agency.request');
Route::get('/approvalarray', 'ApproverController@getPendingRequests');
Route::post('approve-ot-reg/{id}', 'TimesheetManagerController@approveOTReg');



// SYSTEM ADMIN
Route::post('syslogin', 'Auth\SysadminLoginController@loginSysAdmin')->name('sysadmin.login.submit');
Route::get('/sysadmin-dashboard', 'SystemAdminController@showDashboard')->name('sysadmin.dashboard');

Route::get('/branch-list', 'SystemAdminController@getBranches')->name('sysadmin.branch.list');
Route::get('/calendar-setup', 'SystemAdminController@showCalendarSetup')->name('sysadmin.calendar.setup');

Route::post('insert-new-branch','SystemAdminController@insertNewBranch');
Route::post('update-branch','SystemAdminController@updateBranch');

Route::get('/bank-list', 'SystemAdminController@getBanks')->name('sysadmin.bank.list');
Route::post('insert-new-bank', 'SystemAdminController@insertNewBank')->name('sysadmin.insert-new-bank');
Route::post('update-bank', 'SystemAdminController@updateBank')->name('sysadmin.update-bank');

//SECURITY ADMIN
Route::post('seclogin', 'Auth\SecadminLoginController@loginSecAdmin')->name('secadmin.login.submit');
Route::get('/secadmin-dashboard', 'SecurityAdminController@showDashboard')->name('secadmin.dashboard');
Route::get('/users-list', 'SecurityAdminController@getUsers')->name('get.users');
Route::get('/security/parameters', 'SecurityAdminController@showSecurityParameters')->name('secadmin.security.parameters');
Route::get('/create-user', 'SecurityAdminController@createUser')->name('create.user');
Route::post('insert-user', 'SecurityAdminController@insertNewUser')->name('insert.new.user');
Route::post('update-user', 'SecurityAdminController@updateUser')->name('update.user');

//UPLOAD
Route::get('export-file/{type}', 'UploadController@exportFile')->name('export.file');
Route::get('template/bulk-upload', 'UploadController@importExportView')->name('bulk.upload');


//IMPORT
Route::post('import-file', 'UploadController@importFile')->name('import.file');
Route::post('import-employee', 'ImportController@import')->name('import.employee');

Route::post('import-agency', 'ImportController@importAgencies')->name('import.agency');


// VIEW: REPORT
Route::get('/report/employee-list', 'EmployeeReportController@getReportEmployeeList')->name('report.employee.list');
Route::get('/report/employee-list-per-agency', 'EmployeeReportController@getReportEmployeeListByAgency')->name('report.employee.list.per.agency');

Route::get('/report/timesheet', 'ReportController@getReportEmployeeTimesheet')->name('get.report.timesheet');

// EXPORT
Route::get('/export-timesheet-conso', 'ReportController@exportTimesheetConso')->name('export.timesheet.conso');
Route::get('/export-upload-exception', 'ReportController@exportExceptionFromUpload')->name('export.exception.from.upload');

Route::get('/generate-timesheet-conso', 'ReportController@generateTimesheetConso');
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Exception Report
@endsection


@section('main-content')

	<div class="container-fluid spark-screen">

		@include('vendor.adminlte.layouts.partials.flashalert')

		<div class="row">
			<div class="col-md-12">

                {{-- box: employee list --}}
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">Bulk Upload Exceptions</h3>
						<br><br>
						<button class="btn btn-success" data-toggle="modal" data-target="#modal-upload">
                            <i class="fa fa-cloud-upload"></i> Re-Upload Template
                        </button>

					</div>

					<!-- /.box-header -->
					<div class="box-body">
						<table id="employee-table" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Module</th>
									<th>Exception</th>
									<th>Field Name</th>
									<th>User</th>
									<th>Branch</th>
									<th>Date</th>
								</tr>
							</thead>

							<tbody class="tbody-employee-list">
								@foreach ($exceptions as $exception)
                                    <tr>
                                        <td style="width:50px;">{{$exception->id}}</td>
                                        <td>{{$exception->module}}</td>
										@if($exception->exception_type == 'Duplicate record')
											<td style="width:70px; text-align: center;"><span class="badge bg-maroon badge-secondary">Duplicate</span></td>
										@else
											<td style="width:70px; text-align: center;"><span class="badge bg-purple badge-secondary">Invalid/Empty Entry</span></td>
										@endif
										<td>{{$exception->field_name}}</td>
										<td>{{$exception->name}}</td>
										<td>{{$exception->branch_name}}</td>
										<td>{{$exception->created_at}}</td>
                                    </tr>
                                    @endforeach
							</tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>

			</div>
		</div>
	</div>

@endsection

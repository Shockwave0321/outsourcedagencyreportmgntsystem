<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i>
                        @auth('web')
                            {{ Auth::user()->role == 1 ? 'MAKER' : 'HO MAKER' }} 
                        @endauth

                        @auth('approver')
                            {{ Auth::user()->role == 1 ? 'MAKER APPROVER' : 'HO APPROVER' }} 
                        @endauth

                        @auth('secadmin')
                            {{ Auth::user()->role == 1 ? 'SECURITY ADMIN' : 'INTRUDER' }} 
                        @endauth

                        @auth('sysadmin')
                            {{ Auth::user()->role == 1 ? 'SYSTEM ADMIN' : 'INTRUDER' }} 
                        @endauth
                        
                    </a>
                </div>
            </div>
        @endif

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
                
            @auth("web")               
                @if(Auth::user()->role == 1)
                    <li><a href="{{ route('app.dashboard') }}"><i class='fa fa-tachometer'></i> <span>DASHBOARD</span></a></li>
                    <li><a href="{{ route('show.employee.list') }}"><i class='fa fa-users'></i> <span>AGENCY PERSONNELS</span></a></li> 
                    <li><a href="{{ route('show.pending.request') }}"><i class='fa fa-check-square-o'></i> <span>PENDING REQUEST</span></a></li>
                    <li><a href="{{ route('show.timesheet.list') }}"><i class='fa fa-calendar-o'></i> <span>MANAGE TIMESHEET</span></a></li>  
                    <li class="treeview">
                        <a href="#"><i class='fa fa-clipboard'></i> <span>REPORTS</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/report/employee-list"><i class='fa fa-file-excel-o'></i>Employees</a></li>
                            <li><a href="#"><i class='fa fa-file-excel-o'></i>Agencies</a></li>
                            <li><a href="/generate-timesheet-conso"><i class='fa fa-file-excel-o'></i>Consolidated Timesheet</a></li>
                            <li><a href="{{ route('get.report.timesheet') }}"><i class='fa fa-file-excel-o'></i>Individual Timesheet</a></li>
                            <li><a href="/exception/upload-report"><i class='fa fa-file-code-o'></i>Exception</a></li>
                        </ul>
                    </li> 

                @else
                    <li><a href="{{ url('home') }}"><i class='fa fa-tachometer'></i> <span>DASHBOARD</span></a></li>
                    <li><a href="{{ url('/employee-list') }}"><i class='fa fa-users'></i> <span>AGENCY PERSONNELS</span></a></li>  
                    <li><a href="{{ url('agency-list') }}"><i class='fa fa-briefcase'></i> <span> AGENCIES</span></a></li> 
                    <li><a href="{{ route('show.pending.request') }}"><i class='fa fa-check-square-o'></i> <span>PENDING REQUEST</span></a></li>
                    <li><a href="{{ route('show.timesheet.list') }}"><i class='fa fa-calendar-o'></i> <span>MANAGE TIMESHEET</span></a></li>
                    <li class="treeview">
                        <a href="#"><i class='fa fa-clipboard'></i> <span>REPORTS</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="/report/employee-list"><i class='fa fa-file-excel-o'></i>Employees</a></li>
                            <li><a href="#"><i class='fa fa-file-excel-o'></i>Agencies</a></li>
                            <li><a href="/generate-timesheet-conso"><i class='fa fa-file-excel-o'></i>Consolidated Timesheet</a></li>
                            <li><a href="{{ route('get.report.timesheet') }}"><i class='fa fa-file-excel-o'></i>Individual Timesheet</a></li>
                            <li><a href="/exception/upload-report"><i class='fa fa-file-code-o'></i>Exception</a></li>
                        </ul>
                    </li>       
                @endif
            @endauth

            @auth("approver")
                {{-- @if(Auth::user()->role == 1) --}}
                    <li><a href="{{ route('approver.dashboard') }}"><i class='fa fa-check-square-o'></i> <span>FOR APPROVAL</span></a></li>
                {{-- @else
                    <li><a href="{{route('approver.dashboard')}}"><i class='fa fa-check-square-o'></i> <span>FOR APPROVAL</span></a></li>
                @endif  --}}
            @endauth
            
            @auth("sysadmin")
                <li class="treeview">
                    <li><a href="{{ url('bank-list') }}"><i class='fa fa-bank'></i> BANKS</a></li>
                    <li><a href="{{ url('branch-list') }}"><i class='fa fa-bank'></i> BRANCHES</a></li>
                    <li><a href="{{ url('calendar-setup') }}"><i class='fa fa-calendar'></i> CALENDAR SETUP</a></li>
                </li>
            @endauth
          
            @auth("secadmin")
                <li><a href="{{ route('secadmin.dashboard') }}"><i class='fa fa-tachometer'></i> <span>DASHBOARD</span></a></li>
                <li><a href="{{ route('get.users') }}"><i class='fa fa-group'></i> <span>USER MANAGEMENT</span></a></li>
                <li><a href="{{ route('secadmin.security.parameters') }}"><i class='fa fa-shield'></i> <span>SECURITY</span></a></li>
                <li><a href="#"><i class='fa fa-terminal'></i> <span>LOGS</span></a></li>
            @endauth

            
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

<head>
    <meta charset="UTF-8">
    <title> OARMS - @yield('htmlheader_title', 'Dashboard') </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/img/cbc-icon.ico?" type="image/x-icon">

    <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('/css/skins/skin-red.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

    {{-- Datatables --}}
    <link rel="stylesheet" href="{{ asset("/plugins/dataTables.bootstrap.min.css") }}">
    {{-- dropdown --}}
    <link rel="stylesheet" href="{{ asset("/plugins/select2/select2.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/plugins/select2/maximize-select2.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("/bower_components/font-awesome/css/font-awesome.min.css") }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset("/bower_components/Ionicons/css/ionicons.min.css") }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset("/plugins/iCheck/flat/green.css") }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset("/plugins/iCheck/all.css") }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset("/plugins/daterangepicker/daterangepicker.css") }}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset("/plugins/datepicker/datepicker3.css") }}">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{ asset("/plugins/timepicker/bootstrap-timepicker.min.css") }}">
    <!-- toastr -->
    <link rel="stylesheet" href="{{ asset("/plugins/toastr/toastr.min.css") }}">
     <!-- fullCalendar -->
    {{-- <link rel="stylesheet" href="{{ asset("/plugins/fullcalendar/fullcalendar.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/plugins/fullcalendar/fullcalendar.print.min.css") }}" media="print"> --}}


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="{{ asset("/plugins/sweetalert/sweet-alert.min.js") }}"></script>
</head>

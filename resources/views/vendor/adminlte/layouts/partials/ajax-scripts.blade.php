<script>

    $('input#chk-ot-reg').on('ifChecked', function (e) {
    
        e.preventDefault();

        var timesheetId = $(this).val();

        $.ajax({
            url: 'approve-ot-reg/' + timesheetId,
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: { 
                method: 'put',
                _token: '{!! csrf_token() !!}',           
                timesheet_id: timesheetId                
            },
            success: function(data){
                console.log('Success:', data);

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });


    $('input#chk-night-diff').on('ifChecked', function (e) {
        
        e.preventDefault();

        var timesheetId = $(this).val();

        $.ajax({
            url: 'approve-ot-reg/' + timesheetId,
            type: 'post',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: { 
                method: 'put',
                _token: '{!! csrf_token() !!}',           
                timesheet_id: timesheetId                
            },
            success: function(data){
                console.log('Success:', data);

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });

</script>
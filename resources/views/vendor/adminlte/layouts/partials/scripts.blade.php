<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>

<!-- DataTables -->
<script src="{{ asset("/plugins/dataTables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset("/plugins/dataTables/dataTables.bootstrap.min.js") }}"></script>

<!-- dropdown -->
<script src="{{ asset("/plugins/select2/select2.full.min.js") }}"></script>
<!-- iCheck -->
<script src="{{ asset("/plugins/iCheck/icheck.min.js") }}"></script>
<!-- SlimScroll -->
<script src="{{ asset("/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
<!-- FastClick -->
<script src="{{ asset("/plugins/fastclick/fastclick.js") }}"></script>
<!-- InputMask -->
<script src="{{ asset("/plugins/input-mask/jquery.inputmask.js") }}"></script>
<script src="{{ asset("/plugins/input-mask/jquery.inputmask.date.extensions.js") }}"></script>
<script src="{{ asset("/plugins/input-mask/jquery.inputmask.extensions.js") }}"></script>
<!-- date-range-picker -->
<script src="{{ asset("/plugins/daterangepicker/moment.min.js") }}"></script>
<script src="{{ asset("/plugins/daterangepicker/daterangepicker.js") }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset("/plugins/datepicker/bootstrap-datepicker.js") }}"></script>
<!-- bootstrap time picker -->
<script src="{{ asset("/plugins/timepicker/bootstrap-timepicker.min.js") }}"></script>
<!-- toasttr -->
<script src="{{ asset("/plugins/toastr/toastr.min.js") }}"></script>
<!-- fullCalendar -->
{{-- <script src="{{ asset("/plugins/moment/moment.js") }}"></script>
<script src="{{ asset("/plugins/fullcalendar/fullcalendar.min.js") }}"></script> --}}

<script>
    $(function() {

        $('#employee-table').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
        })

        $('#agency-table').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
        })

        $('#branch-table').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
        })

        $('#users-table').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            
        })
        
        $('#filter-usertype').on('change', function () {
            $('#users-table').DataTable().columns(3).search( this.value ).draw();
        } );


    });
</script>


<script>
    $(function () {

        $('input').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat_green',
        increaseArea: '20%' /* optional */
        });

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
        })

        //Initialize Select2 Elements
        $('.select2').select2()

        $('.select2').val(null).trigger("change")

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#date-report-range').daterangepicker()

        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'})

        //Date range as a button
        $('#daterange-btn').daterangepicker(
        {
            ranges   : {
            'Today'       : [moment(), moment()],
            'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month'  : [moment().startOf('month'), moment().endOf('month')],
            'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            startDate: moment().subtract(29, 'days'),
            endDate  : moment()
        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        }
        )

        //Date picker
        $('#create-bdate').datepicker({
            autoclose: true
        })

        //Date picker1
        $('#edit-bdate').datepicker({
            autoclose: true,
            forceParse: false
        })

        //Date picker2
        $('#create-reg-date').datepicker({
            autoclose: true
        })

        //Date picker2
        $('#create-date-started').datepicker({
            autoclose: true
        })

        $('#create-accre-expiry-date').datepicker({
            autoclose: true
        })

        //Date picker3
        $('#datepicker3').datepicker({
            autoclose: true
        })

        //Date picker4
        $('#datepicker4').datepicker({
        autoclose: true
        })

        //Date picker5
        $('#datepicker5').datepicker({
        autoclose: true
        })

        //Date picker6 : use ofr birthdate
        $('#datepicker6').datepicker({
            autoclose: true
        })

        $('#timelog-am').datepicker({
        autoclose: true
        })

        $('#timelog-pm').datepicker({
            autoclose: true
        })

        $('#timelog-ot').datepicker({
            autoclose: true
        })
        

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass   : 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
        })


        //Timepicker
        $('.timepicker').timepicker({
        showInputs: false
        })

        //Timepicker
        $('.timepicker1').timepicker({
        showInputs: false
        })

        //Timepicker
        $('.timepicker2').timepicker({
        showInputs: false
        })

        //Timepicker
        $('.timepicker3').timepicker({
        showInputs: false
        })

        //maximize the select2 size
        $('#select-gender').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 318
        });
        $('#select-cstatus').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 318
        });

        $('#edit-select-agency').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 570
        });
        $('#create-service-type').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 318
        });
        $('#create-overtime-hrs').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 318
        });
        $('#edit-select-gender').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 170
        });
        $('#edit-select-cstatus').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 170
        });
        $('#edit-select-emp-status').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 270
        });
        $('#edit-select-status-reason').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 270
        });
        $('#create-select-bankgroup').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 170
        });
        $('#create-working-days').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 318
        });
        $('#edit-select-bankgroup').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 170
        });
        $('#select-report-options').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 360
        });
        $('#edit-select-branch').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 570
        });
        $('#select-report-agency').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 360
        });

        $('#select-report-employees').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 360
        });
        $('#select-reject-reason').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 240
        });
        $('#edit-select-service').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 270
        });
        $('#branch').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 300
        });
        $('#role').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 300
        });
        $('#usertype').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 300
        });
        $('#filter-usertype').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 170
        });
        $('#edit-user-branch').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 565
        });
        $('#edit-user-role').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 565
        });


        // REPORTS
        $('#report-option-agency').select2({
            allowClear: true,
            minimumResultsForSearch: -1,
            width: 360
        });
    })


    $(document).ready(function() {




        $("#edit-select-emp-status").change(function(){          
            var value = $("#edit-select-emp-status option:selected").val();
           
            if (value == 1) {
                $("#edit-select-status-reason").attr('disabled','disabled');
            } 
            else {
                $("#edit-select-status-reason").removeAttr('disabled');
            }
            
            
        });

    })

    // EDIT EMPLOYEE DETAILS
    $('.tbody-employee-list #btn-edit-employee').on('click', function(){

        $('.modal-body #edit-employeeid').val($(this).data('employeeid'));
        $('.modal-body #edit-employee-no').val($(this).data('employeeno'));
        $('.modal-body #edit-firstname').val($(this).data('firstname'));
        $('.modal-body #edit-middlename').val($(this).data('middlename'));
        $('.modal-body #edit-lastname').val($(this).data('lastname'));
        $('.modal-body #edit-bdate').val($(this).data('bdate'));
        $('.modal-body #edit-select-gender').val($(this).data('gender')).trigger('change');
        $('.modal-body #edit-select-cstatus').val($(this).data('cstatus')).trigger('change');
        $('.modal-body #edit-mobile-no').val($(this).data('mobileno'));
        $('.modal-body #edit-landline-no').val($(this).data('landline'));
        $('.modal-body #edit-address1').val($(this).data('address1'));
        $('.modal-body #edit-address2').val($(this).data('address2'));
        $('.modal-body #edit-email1').val($(this).data('email1'));
        $('.modal-body #edit-email2').val($(this).data('email2'));
        $('.modal-body #edit-select-agency').val($(this).data('agency')).trigger('change');
        $('.modal-body #edit-select-service').val($(this).data('service')).trigger('change');
        $('.modal-body #edit-select-branch').val($(this).data('branch')).trigger('change');
        $('.modal-body #edit-agencyid').val($(this).data('agencyid'));
        $('.modal-body #edit-serviceid').val($(this).data('serviceid'));
        $('.modal-body #edit-branchid').val($(this).data('branchid'));
        $('.modal-body #edit-workingdays').val($(this).data('workingdays'));
        $('.modal-body #edit-workinghrs').val($(this).data('workinghrs'));
        $('.modal-body #edit-overtimehrs').val($(this).data('overtimehrs'));
        $('.modal-body #edit-isovertime').val($(this).data('isovertime'));
        $('.modal-body #edit-supervisor').val($(this).data('supervisor'));

        if ($(this).data('status') == 1) {
            $('.modal-body #edit-select-emp-status').select2("val", "1");
        }
        else {
            $('.modal-body #edit-select-emp-status').select2("val", "0");
        }

        $('#modal-edit-employee').modal('show');
    });


    // EDIT AGENCY DETAILS
    $('.tbody-agency-list #btn-edit-agency').on('click', function(){

        $('.modal-body #edit-agency-code').val($(this).data('agencycode'));

        $('.modal-body #edit-agency-code').val($(this).data('agencycode'));
        $('.modal-body #edit-agency-name').val($(this).data('agencyname'));
        $('.modal-body #edit-address').val($(this).data('address'));
        $('.modal-body #edit-tin-no').val($(this).data('tin'));
        $('.modal-body #edit-reg-date').val($(this).data('regdate'));
        $('.modal-body #edit-sec-reg-no').val($(this).data('secregno'));
        $('.modal-body #edit-mobile-no').val($(this).data('mobileno'));
        $('.modal-body #edit-landline-no').val($(this).data('landlineno'));
        $('.modal-body #edit-email1').val($(this).data('email1'));
        $('.modal-body #edit-email2').val($(this).data('email2'));
        $('.modal-body #edit-owner').val($(this).data('owner'));

        $('#modal-edit-agency').modal('show');

    });


    // EDIT branch DETAILS
    $('.tbody-branch-list #btn-edit-branch').on('click', function(){

        $('.modal-body #edit-branchid').val($(this).data('branchid'));
        $('.modal-body #edit-branch-code').val($(this).data('branchcode'));
        $('.modal-body #edit-branch-name').val($(this).data('branchname'));
        $('.modal-body #edit-address').val($(this).data('address'));
        $('.modal-body #edit-select-bankgroup').val($(this).data('bankgroup')).trigger('change');
        $('#modal-edit-branch').modal('show');

    });

    // EDIT BANK DETAILS
    $('.tbody-branch-list #btn-edit-bank').on('click', function(){

        $('.modal-body #edit-bankid').val($(this).data('bankid'));
        $('.modal-body #edit-bank-code').val($(this).data('bankcode'));
        $('.modal-body #edit-bank-name').val($(this).data('bankname'));
        $('#modal-edit-bank').modal('show');

    });

    // Reject Request
    $('#approval-table .tbody-approval-list #btn-reject').on('click', function(){

        // $('.modal-body #edit-branchid').val($(this).data('branchid'));

        // $('.modal-body #edit-branch-code').val($(this).data('branchcode'));
        // $('.modal-body #edit-branch-name').val($(this).data('branchname'));
        // $('.modal-body #edit-address').val($(this).data('address'));
        // $('.modal-body #edit-select-bankgroup').val($(this).data('bankgroup')).trigger('change');

        $('#modal-reject-request').modal('show');

    });

    // Approval Request
    $('#approval-table .tbody-approval-list #btn-review-approval').on('click', function(){

        $('.modal-body #approval-batch-no').val($(this).data('batchno'));
        $('.modal-body #approval-action-type').val($(this).data('actiontype'));
        $('.modal-body #approval-employee-no').val($(this).data('employeeno'));
        $('.modal-body #approval-branch-id').val($(this).data('branchid'));
        $('.modal-body #approval-user-id').val($(this).data('userid'));
        
        $('.modal-body #approval-purpose').val($(this).data('purpose'));
        $('.modal-body #approval-branch').val($(this).data('branch'));
        $('.modal-body #approval-module').val($(this).data('module'))
        $('.modal-body #approval-field-name').val($(this).data('fieldname'))
        $('.modal-body #approval-old-value').val($(this).data('oldvalue'))
        $('.modal-body #approval-new-value').val($(this).data('newvalue'))

        $('#modal-review-approval').modal('show');

    });

    // Approval Request for Agency
    $('#approval-table .tbody-approval-list #btn-review-agency-approval').on('click', function(){

        $('.modal-body #approval-batch-no').val($(this).data('batchno'));
        $('.modal-body #approval-action-type').val($(this).data('actiontype'));
        $('.modal-body #approval-agency-code').val($(this).data('agencycode'));
        $('.modal-body #approval-branch-id').val($(this).data('branchid'));
        $('.modal-body #approval-user-id').val($(this).data('userid'));

        $('.modal-body #approval-purpose').val($(this).data('purpose'));
        $('.modal-body #approval-branch').val($(this).data('branch'));
        $('.modal-body #approval-module').val($(this).data('module'))

        $('#modal-review-agency-approval').modal('show');

    });

    // Edit user
    $('#users-table .tbody-users-list #btn-edit-user').on('click', function(){

        $('.modal-body #edit-user-fullname').val($(this).data('name'));
        $('.modal-body #edit-user-email').val($(this).data('email'));
        $('.modal-body #edit-user-branch').val($(this).data('branchid')).trigger('change');
        $('.modal-body #edit-user-role').val($(this).data('role')).trigger('change');
        $('.modal-body #edit-userid').val($(this).data('userid'));
        $('.modal-body #edit-user-password').val($(this).data('password'));
        $('.modal-body #edit-id').val($(this).data('id'));

        $('#modal-edit-user').modal('show');

    });




</script>





<script>

    $(document).ready(function() {
        var groupColumn = 1;
        var table = $('#timesheet-table').DataTable({
            "columnDefs": [
                { "visible": false, "targets": groupColumn }
            ],
            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],
            'info' : false,
            'ordering'    : false,
            "order": [[ groupColumn, 'asc' ]],
            // "displayLength": 15,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
    
                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                        );
    
                        last = group;
                    }
                } );
            }
        } );
    
        // Order by the grouping
        $('#timesheet-table tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
                table.order( [ groupColumn, 'desc' ] ).draw();
            }
            else {
                table.order( [ groupColumn, 'asc' ] ).draw();
            }
        } );
    } );


    $('#btn-generate-report').click(function(e) {
        e.preventDefault();
        let agency = 'COLLABERA TECHNOLOGIES'
       
        $.ajax({
            url: '{{ action('EmployeeReportController@getReportEmployeeListByAgency') }}',
            method: 'GET',
            data: {          
            _token: '{!! csrf_token() !!}',           
                agency: agency,         
            // departure: $('#departure-2way').val(),
            },
            dataType: 'json',
            success: function(data){
                console.log(data.agency);
                alert(data.agency);
                // $("#showAddedFlightRoutes").append("<tr><td>");

            }
        });
    });


</script>





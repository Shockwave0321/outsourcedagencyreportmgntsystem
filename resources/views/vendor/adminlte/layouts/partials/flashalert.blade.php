@if ($message = Session::get('user-create-success'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Success</h4>
        {{ $message }}
  </div>
@endif


@if ($message = Session::get('duplicate-entry'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Duplicate Entry Detected</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('warning-upload'))
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Uploaded with exception.</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('success-upload'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Upload Success.</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('upload-approved'))
<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Approved</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('danger-empty-file'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> No File Detected.</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('submit-update-for-approval'))
<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-hourglass-start"></i> Wait for approval</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('request-approved'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Approved!</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('request-rejected'))
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Rejected!</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('success-insert-agency-for-approval'))
<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Wait for Approval</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('success-approved-new-agency'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Approved</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('duplicate-agency-name'))
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Duplicate Entry Detected</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('success-update-agency-for-approval'))
<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Wait for Approval</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('agency-upload-approved'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Approved</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('success-insert-branch'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> New Branch</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('success-updated-branch'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Update Branch</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('success-insert-bank'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> New Bank</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('user-updated'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> User Update</h4>
        {{ $message }}
  </div>
@endif

@if ($message = Session::get('success-updated-bank'))
<div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Update Bank</h4>
        {{ $message }}
  </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
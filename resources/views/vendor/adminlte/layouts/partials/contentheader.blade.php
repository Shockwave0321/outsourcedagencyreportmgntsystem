<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Outsourced Agency Report Management System')
        <small>@yield('contentheader_description','CHINA BANK')</small>
    </h1>
    <ol class="breadcrumb">
        {{-- <li><a href="#"><i class="fa fa-dashboard"></i> {{ trans('adminlte_lang::message.le') }}</a></li>
        <li class="active">{{ trans('adminlte_lang::message.here') }}</li> --}}
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Employee List</li>
    </ol>
</section>

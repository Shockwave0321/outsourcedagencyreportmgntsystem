@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Review for approval
@endsection


@section('main-content')

<div class="container-fluid spark-screen">

    <div class="box box-warning">
        <div class="box-header with-border">
                <h3 class="box-title text-orange"><i class="fa fa-eye-slash"></i> Review Details for Approval</h3>
        </div>
        <form method="POST" enctype="multipart/form-data" action="{{ route('approve.request') }}">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                            <div class="modal-body" >
                            
                                {{-- fullname --}}
                                <div class="row">
        
                                    {{-- fullname --}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="firstname" class="col-form-label">Firstname: <em style="color: red;">*</em></label>
                                            <input type="text" class="form-control" id="review-firstname" name="review-firstname" value="{{ $employee->firstname }}" disabled>
                                        </div>
        
                                        <div class="form-group">
                                            <label for="middlename" class="col-form-label">Middlename: <em style="color: red;">*</em></label>
                                            <input type="text" class="form-control" id="review-middlename" name="review-middlename" value="{{ $employee->middlename }}" disabled>
                                        </div>
        
                                        <div class="form-group">
                                            <label for="lastname" class="col-form-label">Lastname: <em style="color: red;">*</em></label>
                                            <input type="text" class="form-control" id="review-lastname" name="review-lastname" value="{{ $employee->lastname }}" disabled>
                                        </div>
                                    </div>
        
                                    {{-- bdate, status, gender--}}
                                    <div class="col-md-4">

                                        {{-- bdate --}}                 
                                        <div class="form-group">
                                            <label for="dob" class="col-form-label">Birth Date:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" onkeydown="return false" value="{{ $employee->bdate }}" disabled> 
                                            </div>                                   
                                        </div>                                                                            
            
                                        {{-- gender --}}
                                        <div class="form-group">
                                            <label for="gender" class="col-form-label">Gender:</label>
                                            <input type="text" class="form-control" id="review-gender" name="review-gender" value="{{ $employee->gender }}" disabled>
                                        </div>      
            
                                        {{-- cstatus --}}
                                        <div class="form-group">
                                            <label for="cstatus" class="col-form-label">Civil Status:</label>
                                            <input type="text" class="form-control" id="review-cstat" name="review-cstat" value="{{ $employee->civil_status }}" disabled>
                                        </div>

                                    </div>
                                    {{-- end: bdate, gender,status --}}

                                    {{-- contacts --}}
                                    <div class="col-md-4">
                                        
                                        <div class="form-group">
                                            <label for="mobileno" class="col-form-label">Mobile No.:</label>
                                            <input type="text" class="form-control" id="review-mobile-no" name="review-mobile-no" value="{{ $employee->mobile_mo }}" disabled>
                                        </div>
        
                                        <div class="form-group">
                                            <label for="landlineno" class="col-form-label">Home Landline:</label>
                                            <input type="text" class="form-control" id="review-landline-no" name="review-landline-no" value="{{ $employee->home_landline }}" disabled>
                                        </div>

                                        <div class="form-group">
                                            <label for="email1" class="col-form-label">Email: </label>
                                            <input type="text" class="form-control" id="review-email1" name="review-email1" value="{{ $employee->email1 }}" disabled>
                                        </div>

                                    </div>
            
                                </div>
                                {{-- end: contacts --}}

                                {{-- employee address --}}
                                <div class="row">
        
                                    <div class="col-md-8">
        
                                        <div class="form-group">
                                            <label for="address2" class="col-form-label">Address 1:</label>
                                            <input type="text" class="form-control" id="review-address1" name="review-address1" value="{{ $employee->address1 }}" disabled>
                                        </div>
        
                                        <div class="form-group">
                                            <label for="address2" class="col-form-label">Address 2:</label>
                                            <input type="text" class="form-control" id="review-address2" name="review-address2" value="{{ $employee->address2 }}" disabled>
                                        </div>
        
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email2" class="col-form-label">SSS No: <em style="color: red;">*</em></label>
                                            <input type="text" class="form-control" id="review-sssno" name="review-sssno" value="{{ $employee->sss_no }}" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email2" class="col-form-label">TIN No: <em style="color: red;">*</em></label>
                                            <input type="text" class="form-control" id="review-tinno" name="review-tinno" value="{{ $employee->tin_no }}" disabled>
                                        </div>
                                    </div>
        
                                </div>
                                {{-- end: employee address --}}
        
                                {{-- agency --}}
                                <div class="row">
        

                                    <div class="col-md-4">
        
                                        <div class="form-group">
                                            <label for="agency" class="col-form-label">Agency:</label>     
                                            <input type="text" class="form-control pull-right" onkeydown="return false" value="{{ $employee->agency_name }}" disabled>                          
                                        </div>
        
                                    </div>

                                    <div class="col-md-4">
        
                                        <div class="form-group">
                                            <label for="shift" class="col-form-label">Service Type:</label>
                                            <input type="text" class="form-control pull-right" onkeydown="return false" value="{{ $employee->service_description }}" disabled> 
                                        </div>
        
                                        
                                    </div>
                                    {{-- end of 2nd column --}}

                                    {{-- date started --}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="dob" class="col-form-label">Date Started:</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" onkeydown="return false" value="{{ $employee->date_started }}" disabled> 
                                            </div>                                   
                                        </div> 
                                    </div>
                                    {{-- end: date started --}}
                    
                                </div>
                                {{-- end of agency --}}  
                                
                                {{-- branch --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="branched" class="col-form-label">Branched / HO Unit:</label>                                            
                                            <input type="text" class="form-control pull-right" onkeydown="return false" value="{{ $employee->branch_name }}" disabled> 
                                        </div>
                                    </div>

                                </div><br>

                                <div class="row">
                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="shift" class="col-form-label">Reg. Working Days:</label>
                                            <input type="text" class="form-control pull-right" onkeydown="return false" value="{{ $employee->reg_working_days }}" disabled> 
                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="shift" class="col-form-label">Reg. Working Hr.</label>
                                            <input type="text" class="form-control pull-right" onkeydown="return false" value="{{ $employee->reg_working_hrs }}" disabled> 
                                        </div>                                       
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="shift" class="col-form-label">Reg. Overtime Hrs. :</label>
                                            <input type="text" class="form-control pull-right" onkeydown="return false" value="{{ $employee->reg_overtime_hrs }}" disabled> 
                                        </div>
                                    </div>

                                </div>
                            
                                <input type="text" id="approval-batch-no" name="approval-batch-no" value="{{ $employee->approval_batch_no }}" style="color:darkblue;" hidden/>
                                <input type="text" id="approval-action-type" name="approval-action-type" value="{{ $employee->action_type }}" style="color:darkblue;" hidden/>
                                <input type="text" id="approval-employee-no" name="approval-employee-no" value="{{ $employee->employee_no }}" style="color:darkblue;" hidden/>
                                <input type="text" id="approval-branch-id" name="approval-branch-id" value="{{ $employee->branch_id }}" style="color:darkblue;" hidden/>
                                <input type="text" id="approval-user-id" name="approval-user-id" value="{{ $employee->user_id }}" style="color:darkblue;" hidden/>

                            </div>
                                                   
                    </div>
                </div>

            </div>

            <div class="box-footer">
                <button class="btn btn-flickr pull-left"><i class="fa fa-thumbs-down"></i> Reject</button>
                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-thumbs-up"></i> Approve</button>
            </div>
       
        </form>   
    </div>
{{-- container  --}}
</div> 

@endsection()
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Review | Agency
@endsection

@section('main-content')

<div class="container-fluid spark-screen">

    <div class="box box-info">
        <div class="box-header with-border">
                <h3 class="box-title text-aqua"><i class="fa fa-eye"></i> Review Agency Details for Approval</h3>
        </div>
        <form method="POST" enctype="multipart/form-data" action="{{ route('approve.agency.request') }}">
            {{ csrf_field() }}
            <div class="modal-body" >

                    {{-- row 1 : agency names--}}
                    <div class="row">

                        {{--agency name --}}
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Agency Name:</label>
                                <input type="text" class="form-control" id="review-agency-name" name="review-agency-name" value="{{ $agency->agency_name }}" disabled>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Address 1:</label>
                                <input type="text" class="form-control" id="review-address1" name="review-address1" value="{{ $agency->address1 }}" disabled>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Address 2:</label>
                                <input type="text" class="form-control" id="review-address2" name="review-address2" value="{{ $agency->address2 }}" disabled>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Branch:</label>
                                <input type="text" class="form-control" id="review-agency-branch" name="review-agency-branch" value="{{ $agency->branch_name }}" disabled>
                            </div>


                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Services:</label>
                                @if ($services_arr->isEmpty())
                                <input type="text" class="form-control" id="review-services" name="review-services" value="No Services defined" disabled>
                                @else
                                    @foreach ($services_arr as $item)
                                        <input type="text" class="form-control" id="review-services" name="review-services" value="{{ $item->services  }}" disabled>
                                    @endforeach
                                @endif
                               
                            </div>

                        </div>
                        {{-- end: agency fullname --}}

                        {{--tin#, sec reg# --}}
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <label for="lastname" class="col-form-label">TIN No.:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="glyphicon glyphicon-credit-card"></i>
                                    </div>
                                    <input type="text" class="form-control" id="review-tin-no" name="review-tin-no" value="{{$agency->tin_no}}" data-mask disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Reg. Date :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="review-reg-date" name="review-reg-date" value="{{$agency->reg_date}}" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Sec. Reg. No:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="glyphicon glyphicon-sound-5-1"></i>
                                    </div>
                                        <input type="text" class="form-control" id="review-sec-reg-no" name="review-sec-reg-no" value="{{$agency->sec_reg_no}}" disabled>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Accreditation No.:</label>
                                <input type="text" class="form-control" id="review-accre-no" name="review-accre-no" value="{{$agency->accreditation_no}}" disabled>
                            </div>
                        
                            <div class="form-group">
                                <label>Accreditation Expiry Date:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="review-accre-expiry-date" name="review-accre-expiry-date" value="{{$agency->accre_expiry_date}}" disabled>
                                </div>
                            </div>
                                    
                        </div>
                        {{--end: tin#, sec reg# --}}

                    </div>
                    {{-- end: row1  --}}

                 

                    {{-- contact --}}
                    <div class="row">

                        {{-- 1st column --}}
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="agency" class="col-form-label">Mobile#:</label>
                                <input type="text" class="form-control" id="review-mobile-no" name="review-mobile-no" value="{{$agency->mobile_no}}" disabled>
                            </div>

                            <div class="form-group">
                                <label for="branched" class="col-form-label">Landline#:</label>
                                <input type="text" class="form-control" id="review-landline-no" name="review-landline-no" value="{{$agency->landline_no}}" disabled>
                            </div>

                        </div>
                        {{-- end of first column --}}

                        {{-- 2nd column --}}
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="shift" class="col-form-label">Email1:</label>
                                <input type="text" class="form-control" id="review-email1" name="review-email1" value="{{$agency->email1}}" disabled>
                            </div>

                            <div class="form-group">
                                <label for="supervisor" class="col-form-label">Email2:</label>
                                <input type="text" class="form-control" id="review-email2" name="review-email2" value="{{$agency->email2}}" disabled>
                            </div>

                        </div>
                        {{-- end of 2nd column --}}


                    </div>
                    {{-- end of agency --}}


                    {{-- agency owner --}}
                    <div class="row">

                        {{-- 1st column --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                Principal Owner:
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="review-po-firstname" name="review-po-firstname" value="{{$agency->po_firstname}}" disabled>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="review-po-middlename" name="review-po-middlename" value="{{$agency->po_middlename}}" disabled>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="review-po-lastname" name="review-po-lastname" value="{{$agency->po_lastname}}" disabled>
                            </div>

                        </div>
                        {{-- end of first column --}}

                        <div class="col-md-6">
                            <div class="form-group">
                                Contact Person:
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="review-cp-firstname" name="review-cp-firstname" value="{{$agency->cp_firstname}}" disabled>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="review-cp-middlename" name="review-cp-middlename" value="{{$agency->cp_middlename}}" disabled>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="review-cp-lastname" name="review-cp-lastname" value="{{$agency->cp_lastname}}" disabled>
                            </div>

                        </div>

                    </div>
                    {{-- end: agency owner --}}

                    <input type="text" id="approval-batch-no" name="approval-batch-no" style="color:darkblue;" value="{{$agency->approval_batch_no}}" hidden/>
                    <input type="text" id="approval-action-type" name="approval-action-type" style="color:darkblue;" value="{{$agency->action_type}}" hidden/>
                    <input type="text" id="approval-agency-code" name="approval-agency-code" style="color:darkblue;" value="{{$agency->agency_code}}" hidden/>
                    <input type="text" id="approval-branch-id" name="approval-branch-id" style="color:darkblue;" value="{{$agency->branch_id}}" hidden/>
                    <input type="text" id="approval-user-id" name="approval-user-id" style="color:darkblue;" value="{{$agency->user_id}}" hidden/>


            </div>
            <div class="modal-footer">
                <button class="btn btn-flickr pull-left"><i class="fa fa-thumbs-down"></i> Reject</button>
                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-thumbs-up"></i> Approve</button>
            </div>
        </form> 
    </div>
{{-- container  --}}
</div> 

@endsection()
@extends('adminlte::layouts.app')

@section('htmlheader_title')
	HO | Approver
@endsection

@section('main-content')

	<div class="container-fluid spark-screen">

		@include('adminlte::layouts.partials.flashalert')

		<div class="row">
			<div class="col-md-12">

                {{-- box: pending request --}}
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">For Approval</h3>
						<br>
						{{-- <button class="btn btn-success" data-toggle="modal" data-target="#modal-create-agency" ><i class="fa fa-thumbs-up"></i> Approve</button>
						<button class="btn btn-danger" data-toggle="modal" data-target="#modal-create-agency" ><i class="fa fa-thumbs-down"></i> Reject</button> --}}

					</div>

					<!-- /.box-header -->
					<div class="box-body">
						<table id="approval-table" class="table table-bordered table-striped table-hover table-responsive">
							<thead>
								<tr>
									<th>ID</th>
									<th>Requesting Branch/PCC</th>
                                    <th>Purpose</th>
									<th>Module</th>
									<th>Action</th>
									<th></th>
								</tr>
							</thead>

							<tbody class="tbody-approval-list">
								@foreach ($pendings as $approval)
								
										<tr>
											<td>{{$approval->approval_batch_no}}</td>
											<td><span class="badge bg-purple badge-secondary">{{$approval->pcc_code}}</span> | {{$approval->branch_name}}</td>
											<td>{{$approval->purpose}}</td>
											<td>{{$approval->module}}</td>
											@if ($approval->action_type == 'update')
												<td><span class="badge bg-orange badge-secondary">Update</span></td>
											@elseif($approval->action_type == 'insert')
												<td><span class="badge bg-olive badge-secondary">Insert</span></td>
											@else
												<td><span class="badge bg-maroon badge-secondary">Upload</span></td>
											@endif
											
											<td style="text-align: center;">
												@if ($approval->action_type == 'upload')
													<button class="btn btn-info" id="btn-review-agency-approval" 
														data-batchno="{{$approval->approval_batch_no}}"
														data-actiontype="{{$approval->action_type}}" 
														data-agencycode="{{$approval->agency_code}}"
														data-branchid="{{$approval->branch_id}}" 
														data-userid="{{$approval->user_id}}"
														data-branch="{{$approval->branch_name}}" 
														data-purpose="{{$approval->purpose}}"
														data-module="{{$approval->module}}"
														data-fieldname="{{$approval->field_name}}"
														data-oldvalue="{{$approval->old_value}}" 
														data-newvalue="{{$approval->new_value}}">
															<i class="fa fa-eye"></i> 
															Review
													</button>
												@else
													<a class="btn btn-info" href="{{action('ApproverController@showReviewAgencyDetails', $approval->agency_code)}}">
														<i class="fa fa-eye"></i> 
														Review
													</a>
												@endif
												

											</td>
										</tr>
							
                                   
								@endforeach
								


								{{-- @foreach ($pendings->groupBy('approval_batch_no') as $row)
									<tbody>
										<tr class="grouplabel"><th colspan="12">{{ $row[0]['module'] }}</th></tr>
											@foreach ($row as $value)
												<tr> 
													<td>{{$row[0]['module']  }}</td>
												  
												</tr>
											@endforeach
										
									</tbody>
                            	@endforeach --}}

							</tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>

            

			</div>
		</div>
	</div>

	@include('adminlte::modals.modal-approval')
	

@endsection

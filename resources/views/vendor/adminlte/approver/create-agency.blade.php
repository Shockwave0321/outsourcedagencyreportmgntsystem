@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Create New | Agency
@endsection


@section('main-content')

<div class="container-fluid spark-screen">

    @include('vendor.adminlte.layouts.partials.flashalert')

    <div class="box box-success">
        <div class="box-header with-border">
                <h3 class="box-title text-green"><i class="fa fa-user-plus"></i> Create New Agency</h3>
        </div>
        <form method="POST" enctype="multipart/form-data" action="{{ action('AgencyController@insertNewAgency') }}">
            {{ csrf_field() }}
            <div class="modal-body" >

                    {{-- row 1 : agency names--}}
                    <div class="row">

                        {{--agency name --}}
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Agency Name:</label>
                                <input type="text" class="form-control" id="create-agency-name" name="create-agency-name" value="{{ old('create-agency-name') }}">
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Address 1:</label>
                                <input type="text" class="form-control" id="create-address1" name="create-address1" value="{{ old('create-address1') }}"></input>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Address 2:</label>
                                <input type="text" class="form-control" id="create-address2" name="create-address2" value="{{ old('create-address2') }}"></input>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Services:</label>
                                <select class="form-control select2" multiple="multiple" data-placeholder="Select services"
                                id="create-services" name="createservices[]" style="width: 100%;">
                                    @foreach($servicestypes as $service)
                                        <option>{{ $service->service_description }}</option>
                                    @endforeach()
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Branch:</label>
                                <input type="text" class="form-control" id="create-agency-branch" name="create-agency-branch" value="{{ old('create-agency-branch') }}"></input>
                            </div>

                        </div>
                        {{-- end: agency fullname --}}

                        {{--tin#, sec reg# --}}
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <label for="lastname" class="col-form-label">TIN No.:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="glyphicon glyphicon-credit-card"></i>
                                    </div>
                                    <input type="text" class="form-control" id="create-tin-no" name="create-tin-no" data-inputmask='"mask": "99-9999999"' data-mask>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Reg. Date :</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="create-reg-date" name="create-reg-date">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Sec. Reg. No:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="glyphicon glyphicon-sound-5-1"></i>
                                    </div>
                                        <input type="text" class="form-control" id="create-sec-reg-no" name="create-sec-reg-no" ></input>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Accreditation No.:</label>
                                <input type="text" class="form-control" id="create-accre-no" name="create-accre-no">
                            </div>
                        
                            <div class="form-group">
                                <label>Accreditation Expiry Date:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="create-accre-expiry-date" name="create-accre-expiry-date">
                                </div>
                            </div>
                                    
                        </div>
                        {{--end: tin#, sec reg# --}}

                    </div>
                    {{-- end: row1  --}}

                 

                    {{-- contact --}}
                    <div class="row">

                        {{-- 1st column --}}
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="agency" class="col-form-label">Mobile#:</label>
                                <input type="text" class="form-control" id="create-mobile-no" name="create-mobile-no">
                            </div>

                            <div class="form-group">
                                <label for="branched" class="col-form-label">Landline#:</label>
                                <input type="text" class="form-control" id="create-landline-no" name="create-landline-no">
                            </div>

                        </div>
                        {{-- end of first column --}}

                        {{-- 2nd column --}}
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="shift" class="col-form-label">Email1:</label>
                                <input type="text" class="form-control" id="create-email1" name="create-email1">
                            </div>

                            <div class="form-group">
                                <label for="supervisor" class="col-form-label">Email2:</label>
                                <input type="text" class="form-control" id="create-email2" name="create-email2">
                            </div>

                        </div>
                        {{-- end of 2nd column --}}


                    </div>
                    {{-- end of agency --}}


                    {{-- agency owner --}}
                    <div class="row">

                        {{-- 1st column --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                Principal Owner:
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="create-po-firstname" name="create-po-firstname" placeholder="firstname">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="create-po-middlename" name="create-po-middlename" placeholder="middlename">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="create-po-lastname" name="create-po-lastname" placeholder="lastname">
                            </div>

                        </div>
                        {{-- end of first column --}}

                        <div class="col-md-6">
                            <div class="form-group">
                                Contact Person:
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="create-cp-firstname" name="create-cp-firstname" placeholder="firstname">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="create-cp-middlename" name="create-cp-middlename" placeholder="middlename">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="create-cp-lastname" name="create-cp-lastname" placeholder="lastname">
                            </div>

                        </div>

                    </div>
                    {{-- end: agency owner --}}


            </div>
            <div class="modal-footer">
            <a href="/agency-list" class="btn bg-purple pull-left"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
            <button type="submit" class="btn btn-dropbox"><i class="fa fa-save"></i> Create agency</button>
            </div>
        </form> 
    </div>
{{-- container  --}}
</div> 

@endsection()
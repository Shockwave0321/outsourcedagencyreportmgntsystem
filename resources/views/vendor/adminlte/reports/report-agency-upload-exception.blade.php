<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')   
@show

<body>

    <section class="content">

            <div class="row">

               
                    <h1>Outsourced Agency Report Management System</h1>
                    <h2>Exception Report for Agency Upload</h2>
                    <br><br><br><br>

                    <div class="col-xs-12 table-responsive">
                           
                      <table class="table table-striped">
                        <thead>
                        <tr style="background-color:lemonchiffon;">                         
                                <th>Accredited Agency Name</th> 
                                <th>Lastname1</th>
                                <th>Firstname1</th>
                                <th>Middlename1</th>
                                <th>Lastname2</th>
                                <th>Firstname2</th>
                                <th>Middlename2</th>
                                <th>Agency Branch</th>
                                <th>Services</th>
                                <th>SEC Registration No.</th>
                                <th>Date of Registration</th> 
                                <th>Office Phone</th>
                                <th>Mobile Phone</th>
                                <th>Address 1</th>
                                <th>Address 2</th>
                                <th>Personal Email</th>
                                <th>Work/Business Email</th>
                                <th>TIN</th>
                                <th>Accreditation No.</th>
                                <th>Expiry Date</th>
                                <th>Result</th>
                        </tr>
                        </thead>
                         
                            <tbody>
                            
                                @foreach ($exceptions as $exception)
                                    <tr> 
                                        <td>{{ $exception->agency_name }}</td>
                                        <td>{{ $exception->cp_lastname }}</td>   
                                        <td>{{ $exception->cp_firstname }}</td>
                                        <td>{{ $exception->cp_middlename }}</td>  
                                        <td>{{ $exception->po_lastname }}</td>
                                        <td>{{ $exception->po_firstname }}</td>  
                                        <td>{{ $exception->po_middlename }}</td>
                                        <td>{{ $exception->branch }}</td>  
                                        <td></td>
                                        <td>{{ $exception->sec_reg_no }}</td>  
                                        <td>{{ $exception->reg_date }}</td>
                                        <td>{{ $exception->landline_no }}</td>  
                                        <td>{{ $exception->mobile_no }}</td>
                                        <td>{{ $exception->address1 }}</td>  
                                        <td>{{ $exception->address2 }}</td>
                                        <td>{{ $exception->email1 }}</td>  
                                        <td>{{ $exception->email2 }}</td>
                                        <td>{{ $exception->tin_no }}</td> 
                                        <td>{{ $exception->accreditation_no }}</td> 
                                        <td>{{ $exception->accre_expiry_date }}</td>     
                                        <td>{{ $exception->result }}</td> 
                                        <td>{{ $exception->id }}</td> 
                                    </tr>
                                @endforeach
                                
                            </tbody>
                     
                      </table>
                    </div>
                    <!-- /.col -->
                  </div>

    </section>

@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>
<html>
<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')   
@show


<style>
    /* tr.group,
    tr.group:hover {
        background-color: #ddd !important;
    } */
    td {
        border-left: 0;
        border-top: 0;
        border-bottom: 0;
        border-right: 0;
        boirder: 1px;
    }

    table {
        border: none;
        border-collapse: : collapse;
    }

    .grouplabel {
        border: 1px solid;
        border-radius: 5px;
    }
    .subtotal {
        border: 1px solid;
        border-radius: 5px;
    }

</style>

<body>

    <section class="content">

            <div class="row">
                    <div class="col-xs-12 table-responsive">
                      <table class="table table-striped">
                        <thead>
                        <tr style="background-color:lemonchiffon;">
                          <th style="width: 150px;">Day</th>
                          <th>Time In</th>
                          <th>Time Out</th>
                          <th>Reg. Hr.</th>
                          <th>OT</th>
  
                        </tr>
                        </thead>
                            @foreach ($timesheets->groupBy('employee_id') as $employee)
                                <tbody>
                                    <tr class="grouplabel"><th colspan="12">{{ $employee[0]['employee_fullname'] }}</th></tr>
                                        @foreach ($employee as $timelog)
                                            <tr> 
                                                <td>{{ $timelog->log_in_date }}</td>
                                                <td>{{ $timelog->format_time_in }}</td>
                                                <td>{{ $timelog->format_time_out }}</td>
                                                <td>{{ $timelog->sub_total_hours }}</td>
                                                <td>{{ $timelog->sub_total_hours }}</td>       
                                            </tr>
                                        @endforeach
                                    <tr class="subtotal"><th>Total Hours: {{ $employee[0]['total_hours'] }}</th></tr>
                                </tbody>
                            @endforeach
                      </table>
                    </div>
                    <!-- /.col -->
                  </div>

    </section>

@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>
<html>
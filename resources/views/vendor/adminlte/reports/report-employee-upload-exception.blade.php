<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')   
@show

<body>

    <section class="content">

        <div class="row">

            <h1>Outsourced Agency Report Management System</h1>
            <h2>Exception Report for Employee Upload</h2>
            <br><br><br><br>

            <div class="col-xs-12 table-responsive">
                    
                <table class="table table-striped">
                <thead>
                <tr style="background-color:lemonchiffon;">                         
                    <th>First Name</th>
                    <th>Middle Name</th> 
                    <th>Last Name</th>
                    <th>Profit Cost Center Code</th>
                    <th>Branch/Unit Name</th>
                    <th>Assigned Accredited Agency</th>
                    <th>Services</th>
                    <th>Bank</th>
                    <th>Reg. Working Days</th>
                    <th>Reg. Working Hrs.</th>
                    <th>Reg. Overtime Hrs.</th>
                    <th>Date of Birth</th>
                    <th>Gender</th>
                    <th>Civil Status</th> 
                    <th>Mobile Phone</th> 
                    <th>Home Phone</th>
                    <th>Address 1</th>
                    <th>Address 2</th>
                    <th>Personal Email</th>
                    <th>Work/Business Email</th>
                    <th>SSS</th>
                    <th>TIN</th>
                    <th>Start Date</th>
                    <th>ExceptionID</th>
                </tr>
                </thead>
                    
                    <tbody>
                    
                        @foreach ($exceptions as $exception)
                            <tr> 
                                <td>{{ $exception->firstname }}</td>
                                <td>{{ $exception->middlename }}</td>   
                                <td>{{ $exception->lastname }}</td>
                                <td>{{ $exception->pcc_code }}</td>  
                                <td>{{ $exception->branch_name }}</td>
                                <td>{{ $exception->agency_name }}</td>  
                                <td>{{ $exception->service_type }}</td>
                                <td>{{ $exception->bank }}</td>  
                                <td>{{ $exception->reg_working_days }}</td>
                                <td>{{ $exception->reg_working_hrs }}</td>  
                                <td>{{ $exception->reg_overtime_hrs }}</td>
                                <td>{{ $exception->bdate }}</td>  
                                <td>{{ $exception->gender }}</td>
                                <td>{{ $exception->civil_status }}</td>  
                                <td>{{ $exception->mobile_no }}</td>
                                <td>{{ $exception->home_landline }}</td>  
                                <td>{{ $exception->address1 }}</td>
                                <td>{{ $exception->address2 }}</td> 
                                <td>{{ $exception->email1 }}</td> 
                                <td>{{ $exception->email2 }}</td>     
                                <td>{{ $exception->sss_no }}</td> 
                                <td>{{ $exception->tin_no }}</td> 
                                <td>{{ $exception->date_started }}</td> 
                                <td>{{ $exception->id }}</td> 
                            </tr>
                        @endforeach
                        
                    </tbody>
                
                </table>
            </div>
        <!-- row -->
        </div>

    </section>

@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>
<html>
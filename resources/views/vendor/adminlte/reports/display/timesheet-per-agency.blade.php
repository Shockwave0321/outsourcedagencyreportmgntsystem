
@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

<style>
    thead {
        text-align: center !important;
        border: 1px solid coral;
    }
    tr.group {
        border: 1px solid coral;
        /* background-color: #ddd !important; */
    }
    th {
        border: 1px solid coral;
    }

    td {
        /* border-left: 0;
        border-top: 0;
        border-bottom: 0;
        border-right: 0; */
        border: 1px solid coral;
    }

    table {
        border: 1px solid coral;
        border-collapse: : collapse;
    }

    .grouplabel {
        border: 1px solid coral;
        border-radius: 5px;
    }
    .subtotal {
        border: 1px solid coral;
        border-radius: 5px;
    }

    .labeled {
        padding: 0;
    }
    .labeled label {
        display: inline-block;
        padding: 3px;
    }

  
    
</style>

@section('main-content')

        <!-- Main content -->
        <section class="invoice">

            <div class="row">

                {{-- <div class="col-md-5">
                    <div class="form-group">
                        <label for="firstname" class="col-form-label">Report Options:</label><br/>
                        <select type="text" id="select-report-options" style="display:none;"
                        data-minimum-results-for-search="Infinity" class="form-control"
                        name="edit-gender">
                            <option>Employee List by Agency</option>
                            <option>Employee List by Bank</option>
                            <option>Employee List by Branch</option>
                            <option>Employee Timesheets by Agency</option>
                            <option>Employee Timesheet by Employee Id</option>
                            <option>Branch List</option>
                        </select>
                    </div>
                </div> --}}

                <div class="col-md-5">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="firstname" class="col-form-label">Agency:</label><br/>
                            <select type="text" id="select-report-agency" name="select-report-agency" style="display:none;"
                            data-minimum-results-for-search="Infinity" class="form-control"
                            name="edit-gender">
                                <option>SLI CONSULTING</option>
                                <option>RADIX SYSTEMS</option>
                                <option>ARCANYS</option>
                                <option>ACCENTURE</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="form-group">
                        <label for="dob" class="col-form-label">Date:</label>
                        <div class="input-group" style="width: 360px;">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control pull-right" onkeydown="return false"
                            id="date-report-range" name="date-report-range">
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success pull-right" style="margin-top: 25px;"><i class="fa fa-gears"></i> Generate Report</button>
                    </div>
                </div>

            </div>

            <div class="row">
                

                {{-- <div class="col-md-5">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="firstname" class="col-form-label">Employee:</label><br/>
                            <select type="text" id="select-report-employees" style="display:none;"
                            data-minimum-results-for-search="Infinity" class="form-control"
                            name="edit-gender">
                                <option>KOBE BRYANT</option>
                                <option>MICHAEL JORDAN</option>
                                <option>KEVIN DURANT</option>
                                <option>LEBRON JAMES</option>
                                <option>STEPHEN CURRY</option>
                                <option>KAWHI LEONARD</option>
                            </select>
                        </div>
                    </div>
                </div> --}}

            </div>

            <form method="" action="">
              {{ @csrf_field() }}
              <!-- title row -->
                <div class="row">
                {{-- <div class="col-xs-12">
                <h2 class="page-header">
                    <span class="logo-lg"><img src="{{ asset('img/logo/china-bank-logo.png') }}" style="width:50px; height: 40px;"/> </span>
                        China Bank Corporation
                    <small class="pull-right">Date: 2/10/2014</small>
                </h2>
                </div> --}}
                <!-- /.col -->
                </div>

                <hr>
                <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <h3>Personnel Timesheet</h3>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align:center">
                        <h4>{{ $agency->agency_name }}</h4>
                        <h4>{{ $branch->branch_name }}</h4>
                        <span id="txt-cutoff">(Cutoff Period)</span>
                    </div>
                </div>
                <br>
           
            <div class="col-xs-12 table-responsive">
                <table class="timesheet-table table table-striped table-responsive">
                        <thead style="font-size:65%">

                        <tr>
                            <th rowspan="2" style="text-align:center; border: solid 1px coral; text-align:center">Log Date</th>
                            {{-- <th rowspan="2" style="text-align:center; border: solid 1px coral; width:10px;">Day</th> --}}
                            <th colspan="2" style="text-align:center; border: solid 1px coral">Time Logs</th>
                            <th colspan="21" style="text-align:center; border: solid 1px coral">Total Hours</th>
                            <th rowspan="2">Action</th>
                        </tr> 
                        
                        <tr>
                            <th>Time In</th>
                            <th>Time Out</th>
                            <th>Reg. Hr.</th>
                            <th scope="col" colspan="2">OT Reg. Hr.</th>
                            <th scope="col" colspan="2">Night Diff.</th>
                            <th scope="col" colspan="2">OT Reg. Holiday <br> (1st 8hrs)</th>
                            <th scope="col" colspan="2">OT Reg. Holiday <br>(after 8hrs)</th>
                            <th scope="col" colspan="2">OT <br>Rest Day<br>/Special <br>Holiday <br>(1st 8hrs)</th>
                            <th scope="col" colspan="2">OT <br>Rest Day<br>/Special <br>Holiday <br>(after 8hrs)</th>
                            <th scope="col" colspan="2">OT Reg. <br>Holiday <br>(1st 8hrs) <br>- Night</th>
                            <th scope="col" colspan="2">OT Reg. <br>Holiday <br>(after 8hrs) <br>- Night</th>
                            <th scope="col" colspan="2">OT <br>Rest Day<br>/Special <br>Holiday (1st 8hrs) <br> - Night</th>
                            <th scope="col" colspan="2">OT <br>Rest Day<br>/Special <br>Holiday (after 8hrs) <br> - Night</th>
                            
                        </tr>
                        </thead>
                            @foreach ($timesheets->groupBy('employee_id') as $employee)
                                <tbody class="timesheet-body">
                                    <tr class="grouplabel"><th colspan="26" style="background-color:lightcoral; font-size:75%;">{{ $employee[0]['employee_fullname_formal'] }}</th></tr>
                                        @foreach ($employee as $timelog)
                                            <tr style="font-size:65%; width: 10px; font-weight:bold;"> 
                                                <td>{{ $timelog->log_in_date }} {{ $timelog->day_name }}</td>
                                                {{-- <td style="width: 10px;">{{ $timelog->day_name }}</td> --}}
                                                <td>{{ $timelog->format_time_in }}</td>
                                                <td>{{ $timelog->format_time_out }}</td>
                                                <td>{{ $timelog->sub_total_hours }}<label></td>
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" style="zoom:0.5 !important;" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td>  
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td> 
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td> 
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td> 
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td> 
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td> 
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td>  
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td> 
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td> 
                                                <td>{{ rand(5,30) }}</td> <td> <input id="chk-ot-reg" value="{{ $timelog->timesheet_index }}" type="checkbox" /></td> 
                                                <td><button type="button" class="btn btn-foursquare"><i class="fa fa-edit"></i></button></td>                                                    
                                            </tr>
                                        @endforeach
                                    <tr class="subtotal" style="font-size:65%"><th colspan="26">Total Hours: {{ $employee[0]['total_hours'] }}</th></tr>
                                </tbody>
                            @endforeach
                      </table>
                      
                </div>
                {{-- pagination --}}
                {{ $timesheets->links() }} 
                
                <br>
                <!-- this row will not appear when printing -->
                <div class="row no-print">
                  <div class="col-xs-12">
                    <a href="{{ route('export.timesheet.conso') }}" target="_blank" class="btn btn-success pull-right" style="margin: 5px;"><i class="fa fa-file-excel-o"></i> Excel </a>
                   
                    {{-- <a href="#" target="_blank" class="btn btn-danger pull-right" style="margin: 5px;"><i class="fa fa-file-pdf-o"></i> PDF </a>

                    <a href="#" target="_blank" class="btn btn-warning pull-right" style="margin: 5px;"><i class="fa fa-print"></i> Print </a> --}}

                    {{-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                      <i class="fa fa-download"></i> Generate PDF
                    </button> --}}
                  </div>
                </div>
            </form>
       

        </section>
        <!-- /.content -->

@endsection


<style>
    tr.group {
        background-color: lemonchiffon !important;
    },
    tr.group:hover {
        /* background-color: darkorange !important; */
    }
</style>




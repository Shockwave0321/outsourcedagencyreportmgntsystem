<!DOCTYPE html>

<html lang="en">

@section('htmlheader')
    @include('adminlte::layouts.partials.htmlheader')   
@show


<style>
    thead {
        text-align: center !important;
        border: 1px solid coral;
    }
    tr.group {
        border: 1px solid coral;
        /* background-color: #ddd !important; */
    }
    th {
        border: 1px solid coral;
    }

    td {
        /* border-left: 0;
        border-top: 0;
        border-bottom: 0;
        border-right: 0; */
        border: 1px solid coral;
    }

    table {
        border: 1px solid coral;
        border-collapse: : collapse;
    }

    .grouplabel {
        border: 1px solid coral;
        border-radius: 5px;
    }
    .subtotal {
        border: 1px solid coral;
        border-radius: 5px;
    }

</style>

<body>

    <section class="content">

            <div class="row">
                   
                    {{-- <h3>Cosolidated Timesheet Report (per PCC per Agency)</h3> --}}
                    {{-- <h3>Covering Period:  03/01/2019 to 03/15/2019</h3>
                    <h3>Agency Name: RADIX SYSTEMS INC.</h3> --}}

                    <div class="col-xs-12 table-responsive">
                           
                      <table class="table table-striped">
                        <thead>
                            <tr>
                                <th colspan="21" style="text-align:center;"><h3>Outsourced Agency Report Management System</h3></th>
                            </tr>
                            <tr>
                                <th colspan="21" style="text-align:center;"><h3>Outsourced Personnel Timesheet Report</h3></th>
                            </tr>
                            <tr>
                                <th rowspan="2" style="text-align:center;">Log Date</th>
                                <th rowspan="2" style="text-align:center;">Day</th>
                                <th colspan="2" style="text-align:center;">Morning</th>
                                <th colspan="2" style="text-align:center;">Afternoon</th>
                                <th colspan="2" style="text-align:center;">Overtime</th>
                                <th colspan="11" style="text-align:center;">Total Hours</th>
                            </tr>
                                
                            <tr>
                                <th>Time In</th>
                                <th>Time Out</th>
                                <th>Time In</th>
                                <th>Time Out</th>
                                <th>Time In</th>
                                <th>Time Out</th>
                                <th>Reg. Hr.</th>
                                <th>OT Reg. Hr.</th>
                                <th>Night Diff.</th>
                                <th>OT Reg. Holiday (1st 8hrs)</th>
                                <th>OT Reg. Holiday (after 8hrs)</th>
                                <th>OT Rest Day/Special Holiday (1st 8hrs)</th>
                                <th>OT Rest Day/Special Holiday (after 8hrs)</th>

                                <th>OT Reg. Holiday (1st 8hrs) - Night</th>
                                <th>OT Reg. Holiday (after 8hrs) - Night</th>
                                <th>OT Rest Day/Special Holiday (1st 8hrs) - Night</th>
                                <th>OT Rest Day/Special Holiday (after 8hrs) - Night</th>
                            </tr>
                        </thead>
                            @foreach ($timesheets->groupBy('employee_id') as $employee)
                                <tbody>
                                    <tr class="grouplabel">
                                        <th colspan="2">{{ $employee[0]['employee_no'] }}</th>
                                        <th colspan="10">{{ $employee[0]['employee_fullname_formal'] }}</th>
                                        <th colspan="9">Reg. Working Days: W-F 8:30 AM - 5:30 PM</th>
                                    </tr>

                                        @foreach ($employee as $timelog)
                                            <tr> 
                                                <td>{{ $timelog->log_in_date }}</td>
                                                <td>{{ $timelog->log_in_date }}</td>
                                                <td>{{ $timelog->format_time_in }}</td>
                                                <td>{{ $timelog->format_time_out }}</td>
                                                <td>{{ $timelog->format_time_in }}</td>
                                                <td>{{ $timelog->format_time_out }}</td>
                                                <td>{{ $timelog->format_time_in }}</td>
                                                <td>{{ $timelog->format_time_out }}</td>
                                                <td>{{ $timelog->sub_total_hours }}</td>
                                                <td>{{ $timelog->sub_total_hours }}</td>   
                                                <td>{{ $timelog->sub_total_hours }}</td> 
                                                <td>{{ $timelog->sub_total_hours }}</td>
                                                <td>{{ $timelog->sub_total_hours }}</td>   
                                                <td>{{ $timelog->sub_total_hours }}</td>  
                                                <td>{{ $timelog->sub_total_hours }}</td>
                                                <td>{{ $timelog->sub_total_hours }}</td>   
                                                <td>{{ $timelog->sub_total_hours }}</td>
                                                <td>{{ $timelog->sub_total_hours }}</td>   
                                                <td>{{ $timelog->sub_total_hours }}</td>  
                                                     
                                            </tr>
                                        @endforeach
                                    <tr class="subtotal"><th colspan="21">Total Hours: {{ $employee[0]['total_hours'] }}</th></tr>
                                </tbody>
                            @endforeach
                      </table>
                    </div>
                    <!-- /.col -->
                  </div>

    </section>

@section('scripts')
    @include('adminlte::layouts.partials.scripts')
@show

</body>
<html>

@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')


        <!-- Content Header (Page header) -->
        {{-- <section class="content-header">
          <h1>
             Outsourced Employee List
            <small>REPORT</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Employee List</a></li>
          </ol>
        </section> --}}

        <!-- Main content -->
        <section class="invoice">

            <form method="POST" action="">
              {{ @csrf_field() }}
              <!-- title row -->
                <div class="row">
                  <div class="col-xs-12 no-print">                     
                      <button type="button" data-toggle="modal" data-target="#modal-report-options" class="btn bg-orange pull-right" style="margin: 5px;">
                        <i class="fa fa-gear"></i> Report Filter Options 
                      </button>
                  </div>
                  <!-- /.col -->
                </div>

                <div class="row">
                    <div style="text-align: center;" class="col-md-12" >
                        <h3>Agency Personnels List</h3>
                        <h4>{{ $branch->branch_name }}</h4>
                    </div>
                </div>
                <br><br>


                <!-- Table : Data -->
                <div class="row">
                  <div class="col-xs-12 table-responsive">
                      <table class="table table-striped table-responsive">
                          <thead>                      
                          <tr>
                              <th>Employee No.</th>
                              <th>Name of Personnel</th>
                              <th>Agency</th>
                              <th>Service</th>
                              <th>Work Schedule</th>
                              <th>Status</th>
                          </tr>
                          </thead>
                              @foreach ($employees->groupBy('agency_id') as $employee)
                                  <tbody>
                                      <tr class="grouplabel"><th colspan="21" style="background-color:lightcoral;">{{ $employee[0]['agency_name'] }}</th></tr>
                                          @foreach ($employee as $data)
                                              <tr> 
                                                  <td>{{ $data->employee_no }}</td>
                                                  <td>{{ $data->employee_fullname_formal }}</td>
                                                  <td>{{ $data->agency_name }}</td>
                                                  <td>{{ $data->service_description }}</td>
                                                  <td>{{ $data->reg_working_days }}</td>
                                                  <td>{{ $data->employee_status }}</td>  
                                                                        
                                              </tr>
                                          @endforeach
                                      <tr class="subtotal"><th colspan="21">Total Count: {{ $employee[0]['total_hours'] }}</th></tr>
                                  </tbody>
                              @endforeach
                      </table>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->


                <!-- total amount -->
                <div class="row">

                  <div class="col-xs-6">

                    <br >
                      <div class="table-responsive">
                        <table class="table">
                          <tr>
                            <th style="width:30%">Overall  Personnel:</th>
                            <td  style="font-weight:bold;">{{ $employees->count() }}</td>
                          </tr>

                        </table>
                      </div>

                      <div class="no-print">
                          {{ $employees->links() }}
                      </div>

                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                  <div class="col-xs-12">
                    <a href="#" target="_blank" class="btn btn-success pull-right" style="margin: 5px;"><i class="fa fa-file-excel-o"></i> Excel </a>

                    <a href="#" target="_blank" class="btn btn-danger pull-right" style="margin: 5px;"><i class="fa fa-file-pdf-o"></i> PDF </a>

                    <a href="#" target="_blank" class="btn btn-info pull-right" style="margin: 5px;" onclick="window.print()"><i class="fa fa-print"></i> Print </a>

                    {{-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                      <i class="fa fa-download"></i> Generate PDF
                    </button> --}}
                  </div>
                </div>
            </form>
        </section>
        <!-- /.content -->

        <div class="pad margin no-print">
            <div class="callout callout-info" style="margin-bottom: 0!important;">
                <h4><i class="fa fa-info"></i> Note:</h4>
                This page has been enhanced for printing. Click the <b>Print</b> button at the bottom.
            </div>
        </div>

        @include('vendor.adminlte.modals.modal-report-options')
@endsection


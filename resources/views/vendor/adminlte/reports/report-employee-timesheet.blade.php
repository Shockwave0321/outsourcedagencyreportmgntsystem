
@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')


        <!-- Content Header (Page header) -->
        {{-- <section class="content-header">
          <h1>
             Outsourced Employee List
            <small>REPORT</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#">Employee List</a></li>
          </ol>
        </section> --}}

        {{-- <div class="pad margin no-print">
          <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Note:</h4>
            This page has been enhanced for printing. Click the <b>Print</b> button at the bottom.
          </div>
        </div> --}}

        <form method="POST" action="{{ action('ReportController@displayReport') }}">
            @csrf
            <section class="invoice">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="firstname" class="col-form-label">Report Options:</label><br/>
                                <select type="text" id="select-report-options" style="display:none;"
                                data-minimum-results-for-search="Infinity" class="form-control"
                                name="edit-gender">
                                    <option>Employee List by Agency</option>
                                    <option>Employee List by Bank</option>
                                    <option>Employee List by Branch</option>
                                    <option>Employee Timesheets by Agency</option>
                                    <option>Employee Timesheet by Employee Id</option>
                                    <option>Branch List</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="dob" class="col-form-label">Date:</label>
                                <div class="input-group" style="width: 360px;">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" onkeydown="return false"
                                    id="date-report-range" name="date-report-range">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success pull-right" style="margin-top: 25px;"><i class="fa fa-gears"></i> Generate Report</button>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="firstname" class="col-form-label">Agency:</label><br/>
                                    <select type="text" id="select-report-agency" name="select-report-agency" style="display:none;"
                                    data-minimum-results-for-search="Infinity" class="form-control"
                                    name="edit-gender">
                                        <option>SLI CONSULTING</option>
                                        <option>RADIX SYSTEMS</option>
                                        <option>ARCANYS</option>
                                        <option>ACCENTURE</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="firstname" class="col-form-label">Employee:</label><br/>
                                    <select type="text" id="select-report-employees" style="display:none;"
                                    data-minimum-results-for-search="Infinity" class="form-control"
                                    name="edit-gender">
                                        <option>KOBE BRYANT</option>
                                        <option>MICHAEL JORDAN</option>
                                        <option>KEVIN DURANT</option>
                                        <option>LEBRON JAMES</option>
                                        <option>STEPHEN CURRY</option>
                                        <option>KAWHI LEONARD</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </form>
        <!-- Main content -->
        <section class="invoice">
            <form method="POST" action="">
              {{ @csrf_field() }}
              <!-- title row -->
                <div class="row">
                  <div class="col-xs-12">
                    <h2 class="page-header">
                        <span class="logo-lg"><img src="{{ asset('img/logo/cbs-logo.jpg') }}" style="width:40px; height: 40px;"/> </span>
                         China Bank Savings
                      <small class="pull-right">Date: 2/10/2014</small>
                    </h2>
                  </div>
                  <!-- /.col -->
                </div>

                <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <h3>Employee Timesheet</h3>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>{{$employee->employee_fullname}}</h4>
                    </div>
                </div>

                <!-- info row : header -->
                <div class="row invoice-info">
                  <div class="col-sm-4 invoice-col">
                    {{-- <b>From</b> --}}
                    <address>
                      Agency: <strong>{{$employee->assigned_agency}}</strong><br>
                      Address: {{$employee->address1}}<br>
                      Contact No.:{{$employee->mobile_no}}<br>
                      Email: {{$employee->email1}}
                    </address>
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 invoice-col">
                    {{-- <b>To</b> --}}
                    {{-- <address>
                    <strong>dfgdfg</strong><br>
                      Ayala Ave, Microsoft Corp.<br>
                      Phone: 345<br>
                      Email: 34345@gmail
                    </address> --}}
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 invoice-col">
                    Shift Schedule:  <b>{{ $employee->shift_schedule }}</b><br>
                    Branch Assigned:  <b>{{ $employee->branch_assigned }}</b><br>
                    For the Month of:  <b>January 2019</b><br>
                    <br>

                    {{-- <b>Plane Type:</b> thty<br> --}}

                    {{-- <b>Mode of Payment:</b> BDO Account<br>
                    <b>Account#:</b> 999-xxxx-xxx --}}
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table : Data -->
                <div class="row">
                  <div class="col-xs-12 table-responsive">
                    <table class="table table-striped" id="timesheet-table">
                      <thead>
                      <tr style="background-color:lemonchiffon;">
                        <th style="width: 150px;">No.</th>
                        <th>Employee Name</th>
                        <th>Log Date</th>
                        <th>Time In</th>
                        <th>Time Out</th>
                        <th>Total Hours</th>

                      </tr>
                      </thead>
                        <tbody>
                            @foreach( $timesheets as $timesheet )
                           
                                <tr>
                                    <td>{{$timesheet->timesheet_index}}</td>
                                    <td>
                                       Employee No: <b>{{$timesheet->employee_id}}</b> <br/>
                                       Name: <b>{{$timesheet->employee_fullname}}</b> 
                                    </td>
                                    <td>{{$timesheet->log_in_date}}</td>
                                    <td>{{$timesheet->format_time_in}}</td>
                                    <td>{{$timesheet->format_time_out}}</td>
                                    <td>{{$timesheet->total_hours}}</td>
                                </tr>

                            @endforeach()


                           
                        </tbody>
                    </table>

                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
                {{-- {{ $timesheets->links() }} --}}

                <!-- total amount -->
                <div class="row">

                  <div class="col-xs-6">

                    <br >
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <th style="width:30%">Total Hours:</th>
                          {{-- <td  style="font-weight:bold;">{{$timesheets->count()}}</td> --}}
                        </tr>

                      </table>
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                  <div class="col-xs-12">
                    <a href="{{ action('export-timesheet-conso') }}" target="_blank" class="btn btn-success pull-right" style="margin: 5px;"><i class="fa fa-file-excel-o"></i> Excel </a>

                    <a href="#" target="_blank" class="btn btn-danger pull-right" style="margin: 5px;"><i class="fa fa-file-pdf-o"></i> PDF </a>

                    <a href="#" target="_blank" class="btn btn-warning pull-right" style="margin: 5px;"><i class="fa fa-print"></i> Print </a>

                    {{-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                      <i class="fa fa-download"></i> Generate PDF
                    </button> --}}
                  </div>
                </div>
            </form>
        </section>
        <!-- /.content -->

@endsection


<style>
    tr.group {
        background-color: red !important;
    },
    tr.group:hover {
        /* background-color: darkorange !important; */
    }
</style>




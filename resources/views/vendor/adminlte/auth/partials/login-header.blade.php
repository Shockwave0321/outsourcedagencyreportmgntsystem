<div class="panel panel-success flat">
    <div class="panel-body" style="padding: 0px; background-image: url({{ asset('img/banner.jpg') }}); background-size: cover; margin-bottom: -50px;">
        <img src="{{ asset('img/logo/china-bank-logo.png') }}" style="width:190px; height:120px; margin: 15px;"/>


        <div class="login-logo pull-right" style="margin: 15px; ">
            <h1 style="font-size: 1.5em; color: red; "><b>OAR</b><i class="text-gray">MS</i></h1>
            <h5 style="color: red; margin-right: 15px;">Outsourced Agency Reports Management System</h5>
        </div>
        
    </div>
</div>
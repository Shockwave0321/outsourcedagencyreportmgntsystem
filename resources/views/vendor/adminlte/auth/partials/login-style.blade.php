<style>
    body {
    background-color:white !important;
    font-family: 'Open Sans', sans-serif!important;
    font-size:11px;
    }
    .well{
        background-color:#fff!important;
        border-radius:0!important;
        border:black solid 1px;
    }

    .well.login-box {
        border:#d1d1d1 solid 1px;
    }
    .well.login-box legend {
    font-size:26px;
    text-align:center;
    font-weight:300;
    }
    .well.login-box label {
    font-weight:300;
    font-size:13px;
    
    }
    .well.login-box input[type="text"] {
    box-shadow:none;
    border-color:#ddd;
    border-radius:0;
    }

    .well.welcome-text{
        font-size:21px;
    }

    div.fixed {
        position: fixed;
        width: 100%;
        bottom: 10px;
        text-align: center auto;
    }

</style>
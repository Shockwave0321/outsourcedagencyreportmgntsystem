@extends('adminlte::layouts.auth')

@include('adminlte::auth.partials.login-style')

@section('htmlheader_title')
Approver Log in
@endsection

@section('content')
<body class="hold-transition login-page">
    <div id="app">

        @include('adminlte::auth.partials.login-header')

        <div class="login-box">
           
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        <div class="login-box-body well">
        
        <div class="form-group center">
            <small class="text-gray"><b>APPROVER | LOGIN</b></small>
        </div>

        <form action="{{ route('approver.login.submit') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="username/ID" name="userid"/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="password" name="password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> {{ trans('adminlte_lang::message.remember') }}
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-dropbox btn-block btn-flat"><i class="fa fa-sing-in"></i> Login</button>
                </div><!-- /.col -->
            </div>
        </form>

    </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->
    </div>
    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

<div class="footer fixed">
    <div class="md-col-12 col-xs-5">
        <strong>Copyright &copy; 2019 | <a style="color:red">China Bank Corporation &reg;</a></strong>
    </div>
</div>

@endsection

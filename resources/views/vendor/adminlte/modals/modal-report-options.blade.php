{{-- edit user --}}
<div class="modal modal-default fade" id="modal-report-options">
        <div class="modal-dialog" style="width:400px;">
            <div class="modal-content">
    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Report Filter Options</h4>
                    </div>
                    {{-- <form method="GET" enctype="multipart/form-data" action="{{ route('report.employee.list.per.agency') }}"> --}}
                        <form method="GET" enctype="multipart/form-data" action="">
                        {{ csrf_field() }}
                        <div class="modal-body">
                          
                        <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Report Options:</label><br/>
                                        <select type="text" id="select-report-options" style="display:none;"
                                        data-minimum-results-for-search="Infinity" class="form-control"
                                        name="edit-gender">
                                            <option>Employee List by Agency</option>
                                            <option>Employee List by Bank</option>
                                            <option>Employee List by Branch</option>
                                            <option>Employee Timesheets by Agency</option>
                                            <option>Employee Timesheet by Employee Id</option>
                                            <option>Branch List</option>
                                        </select>
                                    </div>
                                </div>
                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="dob" class="col-form-label">Date:</label>
                                        <div class="input-group" style="width: 360px;">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" onkeydown="return false"
                                            id="date-report-range" name="date-report-range">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="firstname" class="col-form-label">Agency:</label><br/>
                                            <select type="text" id="report-option-agency" name="select-report-agency" style="display:none;"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="report-option-agency">
                                               @foreach ($agencies as $agency)
                                                    <option>{{ $agency->agency_name }}</option>
                                               @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="firstname" class="col-form-label">Employee:</label><br/>
                                            <select type="text" id="select-report-employees" style="display:none;"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="edit-gender">
                                                <option>KOBE BRYANT</option>
                                                <option>MICHAEL JORDAN</option>
                                                <option>KEVIN DURANT</option>
                                                <option>LEBRON JAMES</option>
                                                <option>STEPHEN CURRY</option>
                                                <option>KAWHI LEONARD</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
            
                        </div>
                        
    
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="button" id="btn-generate-report" class="btn btn-success pull-right"><i class="fa fa-gears"></i> Generate Report</button>
                        </div>
                    </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
    
    {{-- <style>
        .modal{
            overflow:hidden;
            width: 800px;
        }
        .modal-body{
            overflow-y:scroll;
        }
    </style>
     --}}
{{-- create new branch --}}
<div class="modal modal-default fade" id="modal-create-new-timelog">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Time Log Entry</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ action('TimesheetManagerController@insertNewTimeLog') }}">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- agency name --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Select Personnel:</label>
                                        <select class="form-control select2" style="width: 100%;" name="select-personnel" id="select-personnel" required>
                                            @foreach ($personnels as $personnel)
                                                    <option value="{{ $personnel->id }}">{{ $personnel->employee_fullname }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    {{-- Morning Time Log --}}
                                    <div class="row">

                                        <div class="col-md-4">

                                            <label for="dob" class="col-form-label"><b>Log Date</b> (Morning):</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control pull-right" onkeydown="return false" id="timelog-am" name="timelog-am">
                                                <div class="input-group-addon bg-orange">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div><br>

                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label><b>Time In</b> (Morning):</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control timepicker" name="am-time-in">                          
                                                        <div class="input-group-addon bg-orange">
                                                        <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label><b>Time Out</b> (Morning):</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control timepicker" name="am-time-out">                          
                                                        <div class="input-group-addon bg-orange">
                                                        <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        {{-- end: morning time log --}}

                                        <div class="col-md-4">

                                            <label for="dob" class="col-form-label"><b>Log Date</b> (Afternoon):</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control pull-right" onkeydown="return false" id="timelog-pm" name="timelog-pm">
                                                <div class="input-group-addon bg-olive">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div><br>

                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label><b>Time In</b> (Afternoon):</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control timepicker" name="pm-time-in">                          
                                                        <div class="input-group-addon bg-olive">
                                                        <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label><b>Time Out</b> (Afternoon):</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control timepicker" name="pm-time-out">                          
                                                        <div class="input-group-addon bg-olive">
                                                        <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <label for="dob" class="col-form-label"><b>Log Date</b> (OT):</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control pull-right" onkeydown="return false" id="timelog-ot" name="timelog-ot">
                                                <div class="input-group-addon bg-purple">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div><br>

                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label><b>Time In</b> (OT):</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control timepicker" name="ot-time-in">                          
                                                        <div class="input-group-addon bg-purple">
                                                        <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="bootstrap-timepicker">
                                                <div class="form-group">
                                                    <label><b>Time Out</b> (OT):</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control timepicker" name="ot-time-out">                          
                                                        <div class="input-group-addon bg-purple">
                                                        <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div> 
                                    {{-- end: row1 --}}
                                    

                                  
                                </div>

                            </div>
                            {{-- end: branch fullname --}}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-google pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                        <button type="submit" class="btn btn-success"><i class="fa fa-send"></i> Submit for Appproval</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- edit branch --}}
<div class="modal modal-danger fade" id="modal-edit-timelog">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Branch</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- photo, fullname --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Branch Code:</label>
                                        <input type="text" class="form-control" id="edit-branch-code" name="edit-branch-code">
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Branch Name:</label>
                                        <input type="text" class="form-control" id="edit-branch-name" name="edit-branch-name">
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Address:</label>
                                        <input type="text" class="form-control" id="edit-address" name="edit-address" ></input>
                                    </div>

                                    <div class="form-group" style="margin-left: -15px;">
                                        <div class="col-sm-12">
                                            <label for="cstatus" class="col-form-label">Bank Group:</label>
                                            <select type="text" id="edit-select-bankgroup" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="edit-select-bankgroup" >
                                                <option value="CBC">CBC</option>
                                                <option value="CBS">CBS</option>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="text" id="edit-branchid" name="edit-branchid" hidden/>

                                </div>

                            </div>
                            {{-- end: branch fullname --}}

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dropbox"><i class="fa fa-save"></i> Save Changes</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<style>
    .modal{
        overflow:hidden;
        width: 800px;
    }
    .modal-body{
        overflow-y:scroll;
    }
</style>

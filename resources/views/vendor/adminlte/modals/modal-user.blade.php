{{-- edit user --}}
<div class="modal modal-warning fade" id="modal-edit-user">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ action('SecurityAdminController@updateUser') }}">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- photo, fullname --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">User Fullname:</label>
                                        <input type="text" class="form-control" id="edit-user-fullname" name="edit-user-fullname">
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Email:</label>
                                        <input type="text" class="form-control" id="edit-user-email" name="edit-user-email">
                                    </div>

                                    <div class="form-group">
                                        <label for="branched" class="col-form-label">Branch:</label>                                            
                                        <div class="input-group">
                                            <select type="text" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            id="edit-user-branch" name="edit-user-branch">
                                                @foreach ($branches as $branch)
                                                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>      
                                                @endforeach                                                                                             
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Role:</label>
                                        <div class="input-group">
                                            <select type="text" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            id="edit-user-role" name="edit-user-role">
                                                <option value="1">Maker</option>      
                                                <option value="2">HO Maker</option>                                                                                      
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">UserID:</label>
                                        <input type="text" class="form-control" id="edit-userid" name="edit-userid">
                                    </div>

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Password:</label>
                                        <input type="password" class="form-control" id="edit-user-password" name="edit-user-password">
                                    </div>

                                    <input type="text" id="edit-id" name="edit-id" hidden/>

                                </div>

                            </div>
                            {{-- end: branch fullname --}}

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dropbox"><i class="fa fa-save"></i> Save Changes</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<style>
    .modal{
        overflow:hidden;
        width: 800px;
    }
    .modal-body{
        overflow-y:scroll;
    }
</style>

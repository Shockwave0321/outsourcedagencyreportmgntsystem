{{-- create new agency --}}
<div class="modal modal-default fade" id="modal-create-agency">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Outsourced Agency</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ action('AgencyController@insertNewAgency') }}">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- photo, fullname --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    {{-- <div class="form-group">
                                        <label for="firstname" class="col-form-label">Agency Code:</label>
                                        <input type="text" class="form-control" id="create-agency-code" name="create-agency-code">
                                    </div> --}}

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Agency Name:</label>
                                        <input type="text" class="form-control" id="create-agency-name" name="create-agency-name">
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Address:</label>
                                        <input type="text" class="form-control" id="create-address" name="create-address" ></input>
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Services:</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select services"
                                        id="create-services" name="createservices[]" style="width: 100%;">
                                            @foreach($servicestypes as $service)
                                                <option>{{ $service->service_description }}</option>
                                            @endforeach()
                                        </select>
                                    </div>

                                </div>



                            </div>
                            {{-- end: agency fullname --}}

                            {{--tin#, sec reg# --}}
                            <div class="row">

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">TIN No.:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="glyphicon glyphicon-credit-card"></i>
                                            </div>
                                            <input type="text" class="form-control" id="create-tin-no" name="create-tin-no" data-inputmask='"mask": "99-9999999"' data-mask>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <label>Reg. Date :</label>

                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="create-reg-date" name="create-reg-date">
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Sec. Reg. No:</label>
                                        <input type="text" class="form-control" id="create-sec-reg-no" name="create-sec-reg-no" ></input>
                                    </div>

                                </div>

                            </div>
                            {{-- end: agency fullname --}}

                            {{-- contact --}}
                            <div class="row">

                                {{-- 1st column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="agency" class="col-form-label">Mobile#:</label>
                                        <input type="text" class="form-control" id="create-mobile-no" name="create-mobile-no">
                                    </div>

                                    <div class="form-group">
                                        <label for="branched" class="col-form-label">Landline#:</label>
                                        <input type="text" class="form-control" id="create-landline-no" name="create-landline-no">
                                    </div>

                                </div>
                                {{-- end of first column --}}

                                {{-- 2nd column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="shift" class="col-form-label">Email1:</label>
                                        <input type="text" class="form-control" id="create-email1" name="create-email1">
                                    </div>

                                    <div class="form-group">
                                        <label for="supervisor" class="col-form-label">Email2:</label>
                                        <input type="text" class="form-control" id="create-email2" name="create-email2">
                                    </div>

                                </div>
                                {{-- end of 2nd column --}}


                            </div>
                            {{-- end of agency --}}


                            {{-- contact info --}}
                            <div class="row">

                                {{-- 1st column --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="mobileno" class="col-form-label">Name of Owner:</label>
                                        <input type="text" class="form-control" id="create-owners" name="create-owners">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox icheck">
                                            <label>
                                                <input type="checkbox" name="accredited"> Accredited?
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                {{-- end of first column --}}

                            </div>
                            {{-- end: contact info --}}


                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Create agency</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- edit agency --}}
<div class="modal modal-default fade" id="modal-edit-agency">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Outsourced Agency</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ action('AgencyController@updateAgency') }}">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- photo, fullname --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Agency Code:</label>
                                        <input type="text" class="form-control" id="edit-agency-code" name="edit-agency-code" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Agency Name:</label>
                                        <input type="text" class="form-control" id="edit-agency-name" name="edit-agency-name">
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Address:</label>
                                        <input type="text" class="form-control" id="edit-address" name="edit-address" ></input>
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Services:</label>
                                        <select class="form-control select2" multiple="multiple" data-placeholder="Select services"
                                        id="edit-select-services" name="edit-select-services[]" style="width: 100%;">
                                            @foreach($agencyServices as $service)
                                               
                                                <option value="{{$service->id}}" {{ in_array($service->id, [$service->services]) ? 'selected="selected"' : null }}>{{ $service->service_description }}</option>
                                            @endforeach()
                                           
                                        </select>
                                    </div>

                                </div>

                            </div>
                            {{-- end: agency fullname --}}

                            {{--tin#, sec reg# --}}
                            <div class="row">

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">TIN No.:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="glyphicon glyphicon-credit-card"></i>
                                            </div>
                                            <input type="text" class="form-control" id="edit-tin-no" name="edit-tin-no" data-inputmask='"mask": "99-9999999"' data-mask>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Reg. Date:</label>
                                        <input type="text" class="form-control" id="edit-reg-date" name="edit-reg-date" ></input>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Sec. Reg. No:</label>
                                        <input type="text" class="form-control" id="edit-sec-reg-no" name="edit-sec-reg-no" ></input>
                                    </div>

                                </div>

                            </div>
                            {{-- end: agency fullname --}}

                            {{-- contact --}}
                            <div class="row">

                                {{-- 1st column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="agency" class="col-form-label">Mobile#:</label>
                                        <input type="text" class="form-control" id="edit-mobile-no" name="edit-mobile-no">
                                    </div>

                                    <div class="form-group">
                                        <label for="branched" class="col-form-label">Landline#:</label>
                                        <input type="text" class="form-control" id="edit-landline-no" name="edit-landline-no">
                                    </div>

                                </div>
                                {{-- end of first column --}}

                                {{-- 2nd column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="shift" class="col-form-label">Email1:</label>
                                        <input type="text" class="form-control" id="edit-email1" name="edit-email1">
                                    </div>

                                    <div class="form-group">
                                        <label for="supervisor" class="col-form-label">Email2:</label>
                                        <input type="text" class="form-control" id="edit-email2" name="edit-email2">
                                    </div>

                                </div>
                                {{-- end of 2nd column --}}


                            </div>
                            {{-- end of agency --}}


                            {{-- contact info --}}
                            <div class="row">

                                {{-- 1st column --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="mobileno" class="col-form-label">Name of Owner:</label>
                                        <input type="text" class="form-control" id="edit-owner" name="edit-owner">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox icheck">
                                            <label for="mobileno" class="col-form-label">Purpose:</label>
                                            <input type="text" class="form-control" id="edit-purpose" name="edit-purpose">
                                        </div>
                                    </div>

                                </div>
                                {{-- end of first column --}}

                            </div>
                            {{-- end: contact info --}}

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> edit agency</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


{{-- upload employee using excel template --}}
<div class="modal modal-default fade" id="modal-upload-agency-template">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Bulk Upload - Agency Template</h4>
                </div>
                {{-- <form method="POST" enctype="multipart/form-data" action="{{ action('UploadController@importFile') }}" > --}}
                    <form method="POST" enctype="multipart/form-data" action="{{ action('ImportController@importAgencies') }}" >
                    @csrf
                    <div class="modal-body" >

                         
                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12">
            
                                {{-- <a href="{{ route('export.file',['type'=>'xls']) }}">Download Excel xls</a> |
            
                                <a href="{{ route('export.file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |
            
                                <a href="{{ route('export.file',['type'=>'csv']) }}">Download CSV</a> | --}}
            
                            </div>
            
                        </div>     

                            <div class="row">
            
                            <div class="col-xs-12 col-sm-12 col-md-12">
            
                                    <div class="form-group">
                                                
                                        <input type="file" name="template-file" class="form-control" placeholder="select file..."/>
            
                                    </div>
            
                                </div>
            
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                     
                                </div>
            
                            </div>
       

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<style>
    .modal{
        overflow:hidden;
        width: 800px;
    }
    .modal-body{
        overflow-y:scroll;
    }
</style>

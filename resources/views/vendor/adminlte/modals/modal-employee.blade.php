{{-- create new employee --}}
<div class="modal modal-default fade" id="modal-create-employee">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Outsourced Employee</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ action('EmployeeController@insertNewEmployee') }}">
                    {{ csrf_field() }}
                    <div class="modal-body" >
                       
                            {{-- photo, fullname --}}
                            <div class="row">

                                {{-- photo --}}
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <img src="/img/user-avatar.png" height="210px; width: 130px;" />
                                    </div>
                                </div>

                                {{-- fullname --}}
                                <div class="col-md-7">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Firstname: <em style="color: red;">*</em></label>
                                        <input type="text" class="form-control" id="create-firstname" name="create-firstname">
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Middlename: <em style="color: red;">*</em></label>
                                        <input type="text" class="form-control" id="create-middlename" name="create-middlename">
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Lastname: <em style="color: red;">*</em></label>
                                        <input type="text" class="form-control" id="create-lastname" name="create-lastname" ></input>
                                    </div>

                                </div>



                            </div>
                            {{-- end: employee fullname --}}

                            {{-- bdate, gender, cstatus --}}
                            <div class="row">
                                {{-- bdate --}}
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="dob" class="col-form-label">DOB:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" onkeydown="return false" id="create-bdate" name="create-bdate">
                                        </div>

                                    </div>

                                </div>

                                {{-- gender --}}
                                <div class="col-md-4">

                                    <div class="form-group" style="margin-left: -15px;">
                                        <div class="col-sm-12">
                                            <label for="gender" class="col-form-label">Gender:</label>
                                            <select type="text" id="select-gender" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            id="create-gender" name="create-gender">
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                {{-- cstatus --}}
                                <div class="col-md-4">

                                    <div class="form-group" style="margin-left: -15px;">
                                        <div class="col-sm-12">
                                            <label for="cstatus" class="col-form-label">Civil Status:</label>
                                            <select type="text" id="select-cstatus" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            id="create-civil-status" name="create-civil-status" >
                                                <option value="S">Single</option>
                                                <option value="M">Married</option>
                                                <option value="X">Separated</option>
                                                <option value="W">Widowed</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            {{-- end: employee fullname --}}

                            {{-- employee address --}}
                            {{-- address1 --}}
                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="address2" class="col-form-label">Address 1:</label>
                                        <input type="text" class="form-control" id="address1" name="address1" ></input>
                                    </div>

                                </div>

                            </div>

                            {{-- address2 --}}
                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="address2" class="col-form-label">Address 2:</label>
                                        <input type="text" class="form-control" id="create-address2" name="create-address2" ></input>
                                    </div>

                                </div>

                            </div>
                            {{-- end: employee address --}}

                            {{-- agency --}}
                            <div class="row">

                                {{-- 1st column --}}
                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="agency" class="col-form-label">Assigned Agency:</label>
                                        <input type="text" class="form-control" id="create-agency" name="create-agency">
                                    </div>

                                    <div class="form-group">
                                        <label for="branched" class="col-form-label">Assigned Branched/Bank:</label>
                                        <input type="text" class="form-control" id="create-branch" name="create-branch">
                                    </div>

                                </div>
                                {{-- end of first column --}}

                                {{-- 2nd column --}}
                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="shift" class="col-form-label">Shift/Schedule:</label>
                                        <input type="text" class="form-control" id="create-shift" name="create-shift">
                                    </div>

                                    <div class="form-group">
                                        <label for="supervisor" class="col-form-label">Supervisor:</label>
                                        <input type="text" class="form-control" id="create-supervisor" name="create-supervisor">
                                    </div>

                                </div>
                                {{-- end of 2nd column --}}


                            </div>
                            {{-- end of agency --}}


                            {{-- contact info --}}
                            <div class="row">

                                {{-- 1st column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="mobileno" class="col-form-label">Mobile No.:</label>
                                        <input type="text" class="form-control" id="create-mobile-no" name="create-mobile-no">
                                    </div>

                                    <div class="form-group">
                                        <label for="landlineno" class="col-form-label">Home Landline:</label>
                                        <input type="text" class="form-control" id="create-landline-no" name="create-landline-no">
                                    </div>

                                </div>
                                {{-- end of first column --}}

                                {{-- 2nd column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="email1" class="col-form-label">Email 1: </label>
                                        <input type="text" class="form-control" id="create-email1" name="create-email1" placeholder="Default">
                                    </div>

                                    <div class="form-group">
                                        <label for="email2" class="col-form-label">SSS No: <em style="color: red;">*</em></label>
                                        <input type="text" class="form-control" id="create-sssno" name="create-sssno">
                                    </div>

                                </div>
                                {{-- end of 2nd column --}}


                            </div>
                            {{-- end: contact info --}}

                            {{-- rate,contract --}}
                            <div class="row">
                                {{-- 1st column --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rate" class="col-form-label">Rate per hour:</label>
                                        <input type="text" class="form-control" id="create-rate" name="create-rate">
                                    </div>

                                </div>
                                {{-- end of first column --}}

                                {{-- 2nd column --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contract" class="col-form-label">Contract Duration:</label>
                                        <input type="text" class="form-control" id="create-contract-duration" name="create-contract-duration">
                                    </div>

                                </div>
                                {{-- end of first column --}}

                            </div>

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Create Employee</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- edit employee --}}
<div class="modal modal-default fade" id="modal-edit-employee">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Outsourced Employee</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ action('EmployeeController@updateEmployee') }}">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- fullname --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Firstname:</label>
                                        <input type="text" class="form-control" id="edit-firstname" name="edit-firstname">
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Middlename:</label>
                                        <input type="text" class="form-control" id="edit-middlename" name="edit-middlename">
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Lastname:</label>
                                        <input type="text" class="form-control" id="edit-lastname" name="edit-lastname" >
                                    </div>

                                </div>

                                

                            </div>
                            {{-- end: employee fullname --}}

                            {{-- bdate, gender, cstatus --}}
                            <div class="row">
                               
                                {{-- bdate --}}
                                <div class="col-md-4">

                                        <label for="dob" class="col-form-label">DOB:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" onkeydown="return false" id="edit-bdate" name="edit-bdate">
                                        </div>
    
                                    </div>
    
                                    {{-- gender --}}
                                    <div class="col-md-4">
                                        
                                        <div class="form-group" style="margin-left: -15px;">
                                            <div class="col-xs-12">
                                            <label for="gender" class="col-form-label">Gender:</label>
                                                <div class="input-group">
                                                    <select type="text" id="edit-select-gender" style="display:none"
                                                    data-minimum-results-for-search="Infinity" class="form-control"
                                                    name="edit-gender">
                                                        <option value="M">Male</option>
                                                        <option value="F">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
    
                                    </div>
    
                                    {{-- cstatus --}}
                                    <div class="col-md-4">
    
                                        <div class="form-group" style="margin-left: -15px;">
                                            <div class="col-sm-12">
                                                <label for="cstatus" class="col-form-label">Civil Status:</label>
                                                <select type="text" id="edit-select-cstatus" style="display:none"
                                                data-minimum-results-for-search="Infinity" class="form-control"
                                                name="edit-civil-status" >
                                                    <option value="S">Single</option>
                                                    <option value="M">Married</option>
                                                    <option value="X">Separated</option>
                                                    <option value="W">Widowed</option>
                                                </select>
                                            </div>
                                        </div>
    
                                    </div>
                            </div>
                            {{-- end: employee fullname --}}

                            {{-- employee address --}}
                            {{-- address1 --}}
                            <div class="row">
                                <br>
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="address2" class="col-form-label">Address 1:</label>
                                        <input type="text" class="form-control" id="edit-address1" name="edit-address1" ></input>
                                    </div>

                                </div>

                            </div>

                            {{-- address2 --}}
                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="address2" class="col-form-label">Address 2:</label>
                                        <input type="text" class="form-control" id="edit-address2" name="edit-address2" ></input>
                                    </div>

                                </div>

                            </div>
                            {{-- end: employee address --}}

                            {{-- agency --}}
                            <div class="row">

                                {{-- 1st column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="agency" class="col-form-label">Assigned Agency:</label>
                                        <select type="text" id="edit-select-agency" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="edit-select-agency" >
                                            @foreach ($agencies as $agency)
                                                <option value="{{$agency->id}}">{{$agency->agency_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="branche" class="col-form-label">Assigned Branched/Bank:</label>
                                        <select type="text" id="edit-select-branch" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="edit-select-branch" >
                                            @foreach ($branches as $branch)
                                                <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                            @endforeach
                                        </select>                                   
                                    </div>

                                </div>
                                {{-- end of first column --}}
                            </div>

                            <div class="row">
                                {{-- 2nd column --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="shift" class="col-form-label">Reg. Working Days:</label>
                                        <input type="text" class="form-control" id="edit-workingdays" name="edit-workingdays">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="shift" class="col-form-label">Reg. Working Hours:</label>
                                        <input type="text" class="form-control" id="edit-workinghrs" name="edit-workinghrs">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="shift" class="col-form-label">Reg. Overtime Hours:</label>
                                        <input type="text" class="form-control" id="edit-overtimehrs" name="edit-overtimehrs">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="supervisor" class="col-form-label">Service:</label>
                                        <select type="text" id="edit-select-service" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="edit-select-service" >
                                            @foreach ($services as $serv)
                                                <option value="{{$serv->id}}">{{$serv->service_description}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                {{-- end of 2nd column --}}


                            </div>
                            {{-- end of agency --}}


                            {{-- contact info --}}
                            <div class="row">

                                {{-- 1st column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="mobileno" class="col-form-label">Mobile No.:</label>
                                        <input type="text" class="form-control" id="edit-mobile-no" name="edit-mobile-no">
                                    </div>

                                    <div class="form-group">
                                        <label for="landlineno" class="col-form-label">Home Landline:</label>
                                        <input type="text" class="form-control" id="edit-landline-no" name="edit-landline-no">
                                    </div>

                                </div>
                                {{-- end of first column --}}

                                {{-- 2nd column --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="email1" class="col-form-label">Email 1:</label>
                                        <input type="text" class="form-control" id="edit-email1" name="edit-email1">
                                    </div>

                                    <div class="form-group">
                                        <label for="email2" class="col-form-label">Email 2:</label>
                                        <input type="text" class="form-control" id="edit-email2" name="edit-email2">
                                    </div>

                                </div>
                                {{-- end of 2nd column --}}


                            </div>
                            {{-- end: contact info --}}

                            {{-- rate,contract --}}
                            <div class="row">
                                {{-- 1st column --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contract" class="col-form-label">Status:</label>
                                        <select type="text" id="edit-select-emp-status" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="edit-select-emp-status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                            
                                        </select>
                                    </div>

                                </div>
                                {{-- end of first column --}}

                                {{-- 2nd column --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contract" class="col-form-label">Reason:</label>
                                        <select type="text" id="edit-select-status-reason" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="edit-select-status-reason">
                                            <option value="R">Resigned</option>
                                            <option value="A">AWOL</option>
                                            <option value="B">Black Listed</option>
                                            <option value="P">Prolonged Leave</option>
                                        </select>
                                    </div>

                                </div>
                                {{-- end of first column --}}

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="mobileno" class="col-form-label">Purpose of update:</label>
                                        <input type="text" class="form-control" id="edit-purpose" name="edit-purpose" placeholder="Reason/purpose of update ex. wrong lastname" required>
                                    </div>
                                </div>
                            </div>

                            <input type="text" id="edit-employeeid" name="edit-employeeid" hidden/>
                            <input type="text" id="edit-employee-no" name="edit-employee-no" hidden/>
                            <input type="text" id="edit-branch-id" name="edit-branch-id" hidden/>
                            <input type="text" id="edit-agency-id" name="edit-agency-id" hidden/>
                            <input type="text" id="edit-service-id" name="edit-service-id" hidden/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-saved"></i> Save Changes</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


{{-- upload employee using excel template --}}
<div class="modal modal-default fade" id="modal-upload-employee-template">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Bulk Upload - Employee Template</h4>
                </div>
                {{-- <form method="POST" enctype="multipart/form-data" action="{{ action('UploadController@importFile') }}" > --}}
                    <form method="POST" enctype="multipart/form-data" action="{{ action('ImportController@import') }}" >
                    @csrf
                    <div class="modal-body" >

                         
                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12">
            
                                {{-- <a href="{{ route('export.file',['type'=>'xls']) }}">Download Excel xls</a> |
            
                                <a href="{{ route('export.file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |
            
                                <a href="{{ route('export.file',['type'=>'csv']) }}">Download CSV</a> | --}}
            
                            </div>
            
                        </div>     

                            <div class="row">
            
                            <div class="col-xs-12 col-sm-12 col-md-12">
            
                                    <div class="form-group">
                                                
                                        <input type="file" name="template-file" class="form-control" placeholder="select file..."/>
            
                                    </div>
            
                                </div>
            
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                     
                                </div>
            
                            </div>
       

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<style>
    .modal{
        overflow:hidden;
        width: 800px;
    }
    .modal-body{
        overflow-y:scroll;
    }
</style>

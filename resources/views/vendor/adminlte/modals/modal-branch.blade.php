{{-- create new branch --}}
<div class="modal modal-default fade" id="modal-create-branch">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Create New Branch</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ action('SystemAdminController@insertNewBranch') }}">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- agency name --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">PCC Code:</label>
                                        <input type="text" class="form-control" id="create-branch-code" name="create-branch-code">
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Branch Name:</label>
                                        <input type="text" class="form-control" id="create-branch-name" name="create-branch-name">
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Address:</label>
                                        <input type="text" class="form-control" id="create-address" name="create-address" ></input>
                                    </div>

                                    <div class="form-group" style="margin-left: -15px;">
                                        <div class="col-sm-12">
                                            <label for="cstatus" class="col-form-label">Bank Group:</label><br/>
                                            <select type="text" id="create-select-bankgroup" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="create-select-bankgroup" >
                                                @foreach ($banks as $bank)
                                                    <option value="{{ $bank->bank_code }}">{{ $bank->bank_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            {{-- end: branch fullname --}}

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Create branch</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- edit branch --}}
<div class="modal modal-danger fade" id="modal-edit-branch">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Branch</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ action('SystemAdminController@updateBranch') }}">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- photo, fullname --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Branch Code:</label>
                                        <input type="text" class="form-control" id="edit-branch-code" name="edit-branch-code">
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Branch Name:</label>
                                        <input type="text" class="form-control" id="edit-branch-name" name="edit-branch-name">
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Address:</label>
                                        <input type="text" class="form-control" id="edit-address" name="edit-address" ></input>
                                    </div>

                                    <div class="form-group" style="margin-left: -15px;">
                                        <div class="col-sm-12">
                                            <label for="cstatus" class="col-form-label">Bank Group:</label>
                                            <select type="text" id="edit-select-bankgroup" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            name="edit-select-bankgroup" >
                                                <option value="CBC">CBC</option>
                                                <option value="CBS">CBS</option>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="text" id="edit-branchid" name="edit-branchid" hidden/>

                                </div>

                            </div>
                            {{-- end: branch fullname --}}

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dropbox"><i class="fa fa-save"></i> Save Changes</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<style>
    .modal{
        overflow:hidden;
        width: 800px;
    }
    .modal-body{
        overflow-y:scroll;
    }
</style>

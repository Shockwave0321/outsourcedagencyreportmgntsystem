{{-- create new branch --}}
<div class="modal modal-warning fade" id="modal-create-bank">
        <div class="modal-dialog">
            <div class="modal-content">
    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Create New Bank</h4>
                    </div>
                    <form method="POST" enctype="multipart/form-data" action="{{ action('SystemAdminController@insertNewBank') }}">
                        {{ csrf_field() }}
                        <div class="modal-body" >
    
                                {{-- agency name --}}
                                <div class="row">
    
                                    {{-- fullname --}}
                                    <div class="col-md-12">
    
                                        <div class="form-group">
                                            <label for="firstname" class="col-form-label">Bank Code:</label>
                                            <input type="text" class="form-control" id="create-bank-code" name="create-bank-code">
                                        </div>
    
                                        <div class="form-group">
                                            <label for="middlename" class="col-form-label">Bank Name:</label>
                                            <input type="text" class="form-control" id="create-bank-name" name="create-bank-name">
                                        </div>
    
                                    </div>
    
                                </div>
                                {{-- end: branch fullname --}}
    
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Create Bank</button>
                        </div>
                    </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
    {{-- edit branch --}}
    <div class="modal modal-danger fade" id="modal-edit-bank">
        <div class="modal-dialog">
            <div class="modal-content">
    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Branch</h4>
                    </div>
                    <form method="POST" enctype="multipart/form-data" action="{{ action('SystemAdminController@updateBank') }}">
                        {{ csrf_field() }}
                        <div class="modal-body" >
    
                                {{-- photo, fullname --}}
                                <div class="row">
    
                                    {{-- fullname --}}
                                    <div class="col-md-12">
    
                                        <div class="form-group">
                                            <label for="firstname" class="col-form-label">Branch Code:</label>
                                            <input type="text" class="form-control" id="edit-bank-code" name="edit-bank-code">
                                        </div>
    
                                        <div class="form-group">
                                            <label for="middlename" class="col-form-label">Branch Name:</label>
                                            <input type="text" class="form-control" id="edit-bank-name" name="edit-bank-name">
                                        </div>
    
                                        
                                        <input type="text" id="edit-bankid" name="edit-bankid" hidden/>
    
                                    </div>
    
                                </div>
                                {{-- end: branch fullname --}}
    
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-dropbox"><i class="fa fa-save"></i> Save Changes</button>
                        </div>
                    </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
    
    <style>
        .modal{
            overflow:hidden;
            width: 800px;
        }
        .modal-body{
            overflow-y:scroll;
        }
    </style>
    
{{-- reject approval --}}
<div class="modal modal-danger fade" id="modal-reject-request">
    <div class="modal-dialog" style="width: 300px;">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Enter Reason for Rejection</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="">
                    {{ csrf_field() }}
                    <div class="modal-body" >

                            {{-- agency name --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    {{-- <div class="form-group">
                                        <div class="col-md-12">
                                            <label for="cstatus" class="col-form-label"><em style-="font-style: italic">Select from list:</em></label><br/>
                                            <select type="text" id="select-reject-reason" name="select-reject-reason" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control">
                                                <option value="CBC">Incomplete Data</option>
                                                <option value="CBS">Invalid Entry</option>
                                                <option value="CBS">Not Applicable</option>
                                            </select>
                                        </div>
                                    </div> --}}

                                </div>

                            </div>
                            {{-- end: branch fullname --}}

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> Submit</button>
                    </div>

                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- maker approver: approval of employee --}}
<div class="modal modal-info fade" id="modal-review-approval">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Review Approval</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ route('approve.request') }}">
                    {{ csrf_field() }} 
                    <div class="modal-body" >

                            {{-- photo, fullname --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Requesting Branch:</label>
                                        <input type="text" class="form-control" id="approval-branch" name="approval-branch" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Module:</label>
                                        <input type="text" class="form-control" id="approval-module" name="approval-module" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Purpose:</label>
                                        <textarea type="text" class="form-control" rows="4" id="approval-purpose" name="approval-purpose" readonly></textarea>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12" style="margin-left: -15px;">
                                            <label for="cstatus" class="col-form-label"><em style-="font-style: italic">Select from list:</em></label><br/>
                                            <select type="text" id="select-reject-reason" name="select-reject-reason" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control">
                                                <option value="CBC">Incomplete Data</option>
                                                <option value="CBS">Invalid Entry</option>
                                                <option value="CBS">Not Applicable</option>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="text" id="approval-batch-no" name="approval-batch-no" style="color:darkblue;" hidden/>
                                    <input type="text" id="approval-action-type" name="approval-action-type" style="color:darkblue;" hidden/>
                                    <input type="text" id="approval-employee-no" name="approval-employee-no" style="color:darkblue;" hidden/>
                                    <input type="text" id="approval-branch-id" name="approval-branch-id" style="color:darkblue;" hidden/>
                                    <input type="text" id="approval-user-id" name="approval-user-id" style="color:darkblue;" hidden/>

                                </div>

                            </div>
                            {{-- end: branch fullname --}}

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Approve</button>
                    <button type="submit" class="btn btn-flickr"><i class="fa fa-thumbs-down"></i> Reject</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- HO approver: approval of agency --}}
<div class="modal modal-info fade" id="modal-review-agency-approval">
    <div class="modal-dialog">
        <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Review Approval of Agency</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ route('approve.agency.request') }}">
                    {{ csrf_field() }} 
                    <div class="modal-body" >

                            {{-- photo, fullname --}}
                            <div class="row">

                                {{-- fullname --}}
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="firstname" class="col-form-label">Requesting Branch:</label>
                                        <input type="text" class="form-control" id="approval-branch" name="approval-branch" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="middlename" class="col-form-label">Module:</label>
                                        <input type="text" class="form-control" id="approval-module" name="approval-module" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="lastname" class="col-form-label">Purpose:</label>
                                        <textarea type="text" class="form-control" rows="4" id="approval-purpose" name="approval-purpose" readonly></textarea>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12" style="margin-left: -15px;">
                                            <label for="cstatus" class="col-form-label"><em style-="font-style: italic">Select from list:</em></label><br/>
                                            <select type="text" id="select-reject-reason" name="select-reject-reason" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control">
                                                <option value="CBC">Incomplete Data</option>
                                                <option value="CBS">Invalid Entry</option>
                                                <option value="CBS">Not Applicable</option>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="text" id="approval-batch-no" name="approval-batch-no" style="color:darkblue;" hidden/>
                                    <input type="text" id="approval-action-type" name="approval-action-type" style="color:darkblue;" hidden/>
                                    <input type="text" id="approval-agency-code" name="approval-agency-code" style="color:darkblue;" hidden/>
                                    <input type="text" id="approval-branch-id" name="approval-branch-id" style="color:darkblue;" hidden/>
                                    <input type="text" id="approval-user-id" name="approval-user-id" style="color:darkblue;" hidden/>

                                </div>

                            </div>
                            {{-- end: branch fullname --}}

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-thumbs-up"></i> Approve</button>
                    <button type="submit" class="btn btn-flickr"><i class="fa fa-thumbs-down"></i> Reject</button>
                    </div>
                </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<style>
    .modal{
        overflow:hidden;
        width: 400px;
    }
    .modal-body{
        overflow-y:scroll;
    }
</style>

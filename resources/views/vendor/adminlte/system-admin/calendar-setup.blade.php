@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Calendar Setup
@endsection

@section('main-content')

<div class="container-fluid spark-screen">

		<div class="row">

            {{-- begin: calendar days --}}
			<div class="col-md-8">

                {{-- box: begin --}}
				<div class="box box-info">
					<div class="box-header with-border">
                        <h3 class="box-title">Manage Calendar Days</h3>
                        <br><br>
                        <button class="btn btn-success" data-toggle="modal" data-target="#modal-create-bank" ><i class="fa fa-calendar-plus-o"></i> New Calendar Day</button>
                    </div>

					<!-- /.box-header -->
					<div class="box-body">
                            <table id="branch-table" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Date</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
        
                                    <tbody class="tbody-branch-list">
                                        {{-- @foreach ($banks as $bank)
                                            <tr>
                                                <td style="width:50px;">{{$bank->id}}</td>
                                                <td>{{$bank->bank_code}}</td>
                                                <td>{{$bank->bank_name}}</td>
                                             
                                                <td style="text-align: center;">
                                                    <button class="btn btn-foursquare" id="btn-edit-bank"
                                                    data-bankid="{{$bank->id}}" data-bankcode="{{$bank->bank_code}}"
                                                    data-bankname="{{$bank->bank_name}}">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach --}}
                                    </tbody>
        
                                </table>
                      
					</div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                            {{-- <button type="submit" class="btn btn-primary pull-right">Update Changes</button> --}}
                    </div>
				</div>

            </div>
            {{-- end: password parameters --}}

        </div>

    </div>


@endsection()

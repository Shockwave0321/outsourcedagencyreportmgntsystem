@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Sysadmin | Banks
@endsection


@section('main-content')

	<div class="container-fluid spark-screen">

		@include('vendor.adminlte.layouts.partials.flashalert')

		<div class="row">
			<div class="col-md-12">

                {{-- box: employee list --}}
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">List of Banks</h3>
						<br><br>
						<button class="btn btn-success" data-toggle="modal" data-target="#modal-create-bank" ><i class="fa fa-plus"></i> Create New Bank</button>
					</div>

					<!-- /.box-header -->
					<div class="box-body">
						<table id="branch-table" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
                                    <th>ID</th>
                                    <th>Bank Code</th>
									<th>Bank Name</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody class="tbody-branch-list">
								@foreach ($banks as $bank)
                                    <tr>
                                        <td style="width:50px;">{{$bank->id}}</td>
                                        <td>{{$bank->bank_code}}</td>
                                        <td>{{$bank->bank_name}}</td>
                                     
                                        <td style="text-align: center;">
                                            <button class="btn btn-foursquare" id="btn-edit-bank"
                                            data-bankid="{{$bank->id}}" data-bankcode="{{$bank->bank_code}}"
                                            data-bankname="{{$bank->bank_name}}">
                                                <i class="fa fa-edit"></i> Edit
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
							</tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>

                @include('vendor.adminlte.modals.modal-bank')

			</div>
		</div>
    </div>

@endsection

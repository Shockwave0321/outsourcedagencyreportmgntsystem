@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Dashboard
@endsection


@section('main-content')

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                        <h3>Branches<sup style="font-size: 20px"></sup></h3>

                        <p>Maintenance</p>
                        </div>
                        <div class="icon">
                        <i class="fa fa-building"></i>
                        </div>
                        <a href="{{ url('branch-list') }}" class="small-box-footer">Enter <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                        <h3>Banks<sup style="font-size: 20px"></sup></h3>

                        <p>Maintenance</p>
                        </div>
                        <div class="icon">
                        <i class="fa fa-bank"></i>
                        </div>
                        <a href="{{ url('bank-list') }}" class="small-box-footer">Enter <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                        <h3>Calendar</h3>

                        <p>Maintenance</p>
                        </div>
                        <div class="icon">
                        <i class="fa fa-calendar-check-o"></i>
                        </div>
                        <a href="{{ url('calendar-setup') }}" class="small-box-footer">Enter <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                        <h3>Services</h3>

                        <p>Maintenance</p>
                        </div>
                        <div class="icon">
                        <i class="fa fa-cubes"></i>
                        </div>
                        <a href="/exception/upload-report" class="small-box-footer">Enter <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

            </div>
            <!-- /.row -->

          <!-- Main row -->
          <div class="row">

          </div>
          <!-- /.row (main row) -->

        </section>
        <!-- /.content -->





@endsection

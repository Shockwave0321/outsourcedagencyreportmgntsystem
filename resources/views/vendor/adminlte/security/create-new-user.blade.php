@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Create | User
@endsection


@section('main-content')

<div class="container-fluid spark-screen">

    @include('vendor.adminlte.layouts.partials.flashalert')

    <div class="col-md-12">

            <div class="box box-success">

                <div class="box-header with-border">
                        <h3 class="box-title text-green"><i class="fa fa-user-plus"></i> Register New User</h3>
                </div>
        
                <div class="box-body">
            
                    <div class="modal-body" >
                                                
                        <form method="POST" action="{{ route('insert.new.user') }}">
                            @csrf
                           
                            <div class="col-md-4">
                                <div class="form-group">                                  
                                    <label for="name" class="col--form-label text-md-left">User Fullname</label>    
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="complete name" required autofocus>
                        
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                </div>
                        
                                <div class="form-group">
                                    <label for="branched" class="col-form-label">Branch:</label>                                            
                                    <div class="input-group">
                                        <select type="text" style="display:none"
                                        data-minimum-results-for-search="Infinity" class="form-control"
                                        id="branch" name="branch">
                                            @foreach ($branches as $branch)
                                                <option value="{{$branch->id}}">{{$branch->branch_name}}</option>      
                                            @endforeach                                                                                             
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="branched" class="col-form-label">Select User Type:</label>                                            
                                    <div class="input-group">
                                        <select type="text" style="display:none"
                                        data-minimum-results-for-search="Infinity" class="form-control"
                                        id="usertype" name="usertype">
                                            <option value="maker">MAKER</option>     
                                            <option value="approver">APPROVER</option>                                                                                             
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="branched" class="col-form-label">Select Role:</label>                                            
                                    <div class="input-group">
                                        <select type="text" style="display:none"
                                        data-minimum-results-for-search="Infinity" class="form-control"
                                        id="role" name="role">
                                            <option value="1">MAKER</option>     
                                            <option value="2">HO MAKER</option> 
                                            <option value="1">APPROVER</option>     
                                            <option value="2">HO APPROVER</option>                                                                                             
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">                                  
                                    <label for="userid" class="col--form-label text-md-left">User ID <em class="text-red">*</em></label>    
                                        
                                        <input id="userid" type="text" class="form-control{{ $errors->has('userid') ? ' is-invalid' : '' }}" name="userid" value="{{ old('userid') }}"
                                        placeholder="username / user ID" required autofocus>
                        
                                        @if ($errors->has('userid'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('userid') }}</strong>
                                            </span>
                                        @endif
                                </div>
                    
                                <div class="form-group ">
                                    <label for="password" class="col-form-label text-md-left">{{ __('Password') }} <em class="text-red">*</em></label>
                                                        
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" 
                                    placeholder="atleast 6 characters long" required>
                    
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback alert-danger" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        
                                <div class="form-group ">
                                    <label for="password-confirm" class="col-form-label text-md-left">{{ __('Confirm Password') }}</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>                          
                                </div>
                            </div>
                    
                            <br>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4 mt-5">
                                    <button type="submit" class="btn btn-dropbox pull-right">
                                        <i class="fa fa-pencil"></i> {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                
            </div>

    </div>


</div> 

@endsection()
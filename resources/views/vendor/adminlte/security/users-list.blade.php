@extends('adminlte::layouts.app')

@section('htmlheader_title')
	User Management
@endsection


@section('main-content')

	<div class="container-fluid spark-screen">

		@include('vendor.adminlte.layouts.partials.flashalert')

		<div class="row">
			<div class="col-md-12">

                {{-- box: employee list --}}
				<div class="box box-info">
					<div class="box-header with-border">
                        <h3 class="box-title text-aqua">User Management</h3>
					</div>

					<!-- /.box-header -->
					<div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-left">
                                    <a class="btn btn-success" href="{{route('create.user')}}" ><i class="fa fa-user-plus"></i> Create New User</a>
                                </div>
                                <div class="pull-right">
                                    <div class="form-group">                                          
                                        <div class="input-group">
                                            <select type="text" style="display:none"
                                            data-minimum-results-for-search="Infinity" class="form-control"
                                            id="filter-usertype" name="filter-usertype">
                                                <option value="maker">MAKER</option>     
                                                <option value="approver">APPROVER</option>                                                                                             
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>

						<table id="users-table" class="table table-bordered table-striped table-responsive table-hover">
							<thead>
								<tr>
                        
                                    <th>User ID</th>
									<th>FullName</th>
                                    <th>Branch/HO Unit</th>
                                    <th>Role</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody class="tbody-users-list">
								@foreach ($users as $user)
                                    <tr>
                                     
                                        <td>{{$user->userid}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->branch->branch_name}}</td>
                                        @if ($user->role == 1)
                                            <td><span class="badge bg-orange badge-primary">MAKER</span></td>
                                        @else
                                            <td><span class="badge bg-purple badge-secondary">HO MAKER</span></td>
                                        @endif
                                      
                                        @if ($user->status == 1)
                                            <td><span class="badge bg-green badge-secondary">ACTIVE</span></td>
                                        @else
                                            <td><span class="badge bg-maroon badge-secondary">INACTIVE</span></td>
                                        @endif
                                        <td style="text-align: center;">
                                            <button class="btn btn-foursquare" id="btn-edit-user" data-id={{$user->id}}
                                                data-userid="{{$user->userid}}" data-branchid="{{$user->branch_id}}"
                                                data-name="{{$user->name}}" data-role="{{$user->role}}" data-password="{{$user->password}}"
                                                data-status="{{$user->status}}" data-email="{{$user->email}}">
                                                <i class="fa fa-edit"></i> Edit
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach

                                @foreach ($approvers as $user)
                                <tr>
                                     
                                        <td>{{$user->userid}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->branch->branch_name}}</td>
                                        @if ($user->role == 1)
                                            <td><span class="badge bg-orange badge-primary">APPROVER</span></td>
                                        @else
                                            <td><span class="badge bg-purple badge-secondary">HO APPROVER</span></td>
                                        @endif
                                      
                                        @if ($user->status == 1)
                                            <td><span class="badge bg-green badge-secondary">ACTIVE</span></td>
                                        @else
                                            <td><span class="badge bg-maroon badge-secondary">INACTIVE</span></td>
                                        @endif
                                        <td style="text-align: center;">
                                            <button class="btn btn-foursquare" id="btn-edit-user" data-id={{$user->id}}
                                                data-userid="{{$user->userid}}" data-branchid="{{$user->branch_id}}"
                                                data-name="{{$user->name}}" data-role="{{$user->role}}" data-password="{{$user->password}}"
                                                data-status="{{$user->status}}" data-email="{{$user->email}}">
                                                <i class="fa fa-edit"></i> Edit
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
							</tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>

                @include('vendor.adminlte.modals.modal-user')

			</div>
		</div>
    </div>

@endsection

@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Security | Dashboard
@endsection


@section('main-content')

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <div class="col-lg-5 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                        <h3>User Management<sup style="font-size: 20px"></sup></h3>

                        <p>Maintenance</p>
                        </div>
                        <div class="icon">
                        <i class="fa fa-users"></i>
                        </div>
                        <a href="{{ route('get.users') }}" class="small-box-footer">Enter <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-5 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-purple">
                        <div class="inner">
                        <h3>Security Setup<sup style="font-size: 20px"></sup></h3>

                        <p>Maintenance</p>
                        </div>
                        <div class="icon">
                        <i class="fa fa-shield"></i>
                        </div>
                        <a href="{{ route('secadmin.security.parameters') }}" class="small-box-footer">Enter <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-5 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-maroon">
                        <div class="inner">
                        <h3>Session Logs</h3>

                        <p>Monitoring</p>
                        </div>
                        <div class="icon">
                        <i class="fa fa-desktop"></i>
                        </div>
                        <a href="#" class="small-box-footer">Enter <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-5 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-orange">
                        <div class="inner">
                        <h3>Database Task</h3>

                        <p>Manage Database</p>
                        </div>
                        <div class="icon">
                        <i class="fa fa-database"></i>
                        </div>
                        <a href="#" class="small-box-footer">Enter <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

            </div>
            <!-- /.row -->

          <!-- Main row -->
          <div class="row">

          </div>
          <!-- /.row (main row) -->

        </section>
        <!-- /.content -->





@endsection

@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Employees
@endsection

@section('main-content')

	<div class="container-fluid spark-screen">

		@include('vendor.adminlte.layouts.partials.flashalert')

		<div class="row">
			<div class="col-md-12">

                {{-- box: employee list --}}
				<div class="box box-warning">
					<div class="box-header">
						<h3 class="box-title">List of Pending Updates</h3>
						<br><br>

                    </div>

					<!-- /.box-header -->
					<div class="box-body">
						<table id="employee-table" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th>Approval No.</th>
									<th>Module</th>
                                    <th>Action Type</th>
                                    <th>Purpose</th>
                                    <th>Status</th>
                                    <th>Action</th>
								</tr>
							</thead>

							<tbody class="tbody-employee-list">
								@foreach ($pendings as $pending)
                                    <tr>
                                        <td style="width:130px;">{{$pending->approval_batch_no}}</td>
                                        <td>{{$pending->module}}</td>
                                        <td>{{$pending->action_type}}</td>
                                        <td>{{$pending->purpose}}</td>
										@if($pending->status == 1)
                                            <td style="width:70px; text-align: center;"><span class="badge bg-green badge-secondary">Approved</span></td>
                                        @elseif($pending->status == 2)
											<td style="width:70px; text-align: center;"><span class="badge btn-danger badge-secondary">Disapproved</span></td>
										@else
											<td style="width:70px; text-align: center;"><span class="badge btn-info badge-secondary">Waiting for Approval</span></td>
										@endif
                                        <td style="text-align: center;">
                                            <button class="btn btn-foursquare" id="btn-edit-employee">
                                                <i class="fa fa-close"></i> Cancel
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
							</tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>

			</div>
		</div>
	</div>

@endsection

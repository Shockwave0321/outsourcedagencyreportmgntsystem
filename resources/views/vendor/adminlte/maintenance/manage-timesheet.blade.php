@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Manage Timesheet
@endsection


@section('main-content')

	<div class="container-fluid spark-screen">

		@include('vendor.adminlte.layouts.partials.flashalert')

		<div class="row">
			<div class="col-md-12">

                {{-- box: employee list --}}
				<div class="box box-warning">
					<div class="box-header">
						<h3 class="box-title text-orange">Timesheet Management</h3>
						<br><br>
						<button class="btn btn-success" data-toggle="modal" data-target="#modal-create-new-timelog" ><i class="fa fa-clock-o"></i> New Time Log Entry</button>
					</div>

					<!-- /.box-header -->
					<div class="box-body">

                        <table id="branch-table" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
                                    <th>Timesheet No</th>
                                    <th>Name of Personnel</th>
                                    <th>Date Created</th>
                                    <th>Status</th>
                                    <th>Action</th>
								</tr>
							</thead>

							<tbody class="tbody-branch-list">
								@foreach ($timesheets as $data)
                                    <tr>
                                        <td>{{ $data->pcc_code.'-'.$data->timesheet_index }}</td>
                                        <td>{{ $data->employee_fullname }}</td>
                                        <td>{{ $data->created_at }}</td>
                                        <td>{{ $data->status }}</td>   
                                        <td style="text-align: center;">
                                            <button class="btn btn-foursquare" id="btn-edit-branch">
                                                <i class="fa fa-edit"></i> Edit
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
							</tbody>

						</table>

					</div>
					<!-- /.box-body -->
				</div>

                @include('vendor.adminlte.modals.modal-timesheet-entry')

			</div>
		</div>
    </div>

@endsection

@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Upload Template
@endsection

@section('main-content')

<div class="box box-danger">

    <div class="box-header with-border">Upload Template - Employee</div>

        <div class="panel-body"> 

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
fuck
                    <a href="{{ route('export.file',['type'=>'xls']) }}">Download Excel xls</a> |

                    <a href="{{ route('export.file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |

                    <a href="{{ route('export.file',['type'=>'csv']) }}">Download CSV</a> |

                </div>

            </div>     

            
            <form method="POST" enctype="multipart/form-data" action="{{ route('import.excel.employee') }}" >
                @csrf

                <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                        <div class="form-group">
                                    
                            <input type="file" name="template-file" class="form-control" placeholder="select file..."/>

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                
                    <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>

                    </div>

                </div>

            </form>

        </div>

    </div>

</div>

@endsection
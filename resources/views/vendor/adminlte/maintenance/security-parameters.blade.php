@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Parameters
@endsection

@section('main-content')

<div class="container-fluid spark-screen">

		<div class="row">

            {{-- begin: password parameters --}}
			<div class="col-md-6">

                {{-- box: parameters --}}
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Passwords</h3>
					</div>

					<!-- /.box-header -->
					<div class="box-body">

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="firstname" class="col-form-label">Password Length:</label>
                                <input type="number" class="form-control" id="password-param-length" name="password-param-length">
                            </div>

                            <div class="form-group">
                                <label for="firstname" class="col-form-label">Password History:</label>
                                <input type="number" class="form-control" id="password-param-length" name="password-param-length">
                            </div>

                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Password Expiration:</label>
                                <input type="number" class="form-control" id="password-param-enctype" name="password-param-enctype">
                            </div>

                            <div class="form-group">
                                <label for="firstname" class="col-form-label">No. of Attempts:</label>
                                <input type="number" class="form-control" id="password-param-length" name="password-param-length">
                            </div>

                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Account Inactivity:</label>
                                <input type="number" class="form-control" id="password-param-enctype" name="password-param-enctype">
                            </div>

                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Encryption Type:</label>
                                <input type="text" class="form-control" id="password-param-enctype" name="password-param-enctype">
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Key Value Pairs:</label>
                                <input type="text" class="form-control" id="password-param-keyvalue" name="password-param-keyvalue" ></input>
                            </div>

                        </div>

					</div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Update Changes</button>
                    </div>
				</div>

            </div>
            {{-- end: password parameters --}}

            {{-- begin: password parameters --}}
            <div class="col-md-6">

                {{-- box: parameters --}}
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Access Rights</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">


                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="firstname" class="col-form-label">Password History:</label>
                                <input type="text" class="form-control" id="password-param-length" name="password-param-length">
                            </div>

                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Password Expiration:</label>
                                <input type="text" class="form-control" id="password-param-enctype" name="password-param-enctype">
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-form-label">Password History:</label>
                                <input type="text" class="form-control" id="password-param-keyvalue" name="password-param-keyvalue" ></input>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Update Changes</button>
                    </div>
                </div>

            </div>
            {{-- end: password parameters --}}

        </div>

        <div class="row">

             {{-- begin: user maintenance --}}
			<div class="col-md-6">

                {{-- box: parameters --}}
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">User Maintenance</h3>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="firstname" class="col-form-label">Firstname:</label>
                                <input type="text" class="form-control" id="password-param-length" name="password-param-length">
                            </div>

                            <div class="form-group">
                                <label for="firstname" class="col-form-label">Middlename:</label>
                                <input type="text" class="form-control" id="password-param-length" name="password-param-length">
                            </div>

                            <div class="form-group">
                                <label for="middlename" class="col-form-label">Lastname:</label>
                                <input type="text" class="form-control" id="password-param-enctype" name="password-param-enctype">
                            </div>

                            <div class="form-group">
                                <label for="firstname" class="col-form-label">Default Password:</label>
                                <input type="text" class="form-control" id="password-param-length" name="password-param-length">
                            </div>

                            <hr>
                            <button type="submit" class="btn btn-primary pull-right">Create User</button>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
            {{-- end: puser maintenance --}}

        </div>


    </div>


@endsection()

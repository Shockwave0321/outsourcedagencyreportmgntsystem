@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Branches
@endsection


@section('main-content')

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

				{{--
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					Outsourced Branches List
					<small>CHINA BANK</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li><a href="#"><i class="fa fa-plane"></i> Branches</a></li>
				</ol>
				</section>
				--}}

                <div class="row">

                    <!-- /.col -->
                    <div class="col-md-12">
                      <div class="box box-danger">
                        <div class="box-body no-padding">
                          <!-- THE CALENDAR -->
                          <div id="calendar"></div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /. box -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->

                @include('vendor.adminlte.modals.modal-branch')

			</div>
		</div>
    </div>

@endsection

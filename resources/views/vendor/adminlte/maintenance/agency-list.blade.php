@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Agencies
@endsection


@section('main-content')

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">

                {{-- box: employee list --}}
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">Outsourced Agencies</h3>
						<br><br>
						<a href="/create-new-agency" class="btn btn-success"><i class="fa fa-plus"></i> Create New Agency</a>
						<button class="btn btn-success" data-toggle="modal" data-target="#modal-upload-agency-template" ><i class="fa fa-cloud-upload"></i> Upload Template</button>
					</div>

					<!-- /.box-header -->
					<div class="box-body">
						<table id="agency-table" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
                                    <th>ID</th>
                                    <th>Agency Code</th>
									<th>Agency Name</th>
									<th>Address</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody class="tbody-agency-list">
								@foreach ($agencies as $agency)
                                    <tr>
                                        <td style="width:50px;">{{$agency->id}}</td>
                                        <td>{{$agency->agency_code}}</td>
                                        <td>{{$agency->agency_name}}</td>
                                        <td>{{$agency->address1}}</td>
                                        <td>{{$agency->is_active}}</td>
                                        <td style="text-align: center;">
                                            <button class="btn btn-foursquare" id="btn-edit-agency"
                                            data-agencycode="{{$agency->agency_code}}" data-agencycode="{{$agency->agency_code}}"
                                            data-agencyname="{{$agency->agency_name}}" data-address="{{$agency->address1}}"
                                            data-tin="{{$agency->tin_no}}" data-regdate="{{$agency->reg_date}}"
                                            data-secregno="{{$agency->sec_reg_no}}" data-mobileno="{{$agency->mobile_no}}"
                                            data-landlineno="{{$agency->landline_no}}" data-email1="{{$agency->email1}}"
                                            data-email2="{{$agency->email2}}" data-owner="{{$agency->principal_owners}}"
                                            data-status="{{$agency->is_active}}">
                                            <i class="fa fa-edit"></i> Edit
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
							</tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>

                @include('vendor.adminlte.modals.modal-agency')

			</div>
		</div>
    </div>

@endsection

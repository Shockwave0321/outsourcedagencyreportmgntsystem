@extends('adminlte::layouts.app')

@section('htmlheader_title')
	Branches
@endsection


@section('main-content')

	<div class="container-fluid spark-screen">

		@include('vendor.adminlte.layouts.partials.flashalert')

		<div class="row">
			<div class="col-md-12">

                {{-- box: employee list --}}
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">Branches / HO Units</h3>
						<br><br>
						<button class="btn btn-success" data-toggle="modal" data-target="#modal-create-branch" >Create New branch</button>
					</div>

					<!-- /.box-header -->
					<div class="box-body">
						<table id="branch-table" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
                                    <th>ID</th>
                                    <th>PCC Code</th>
									<th>Branch Name</th>
									<th>Bank</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody class="tbody-branch-list">
								@foreach ($branches as $branch)
                                    <tr>
                                        <td style="width:50px;">{{$branch->id}}</td>
                                        <td>{{$branch->pcc_code}}</td>
                                        <td>{{$branch->branch_name}}</td>
                                        <td>{{$branch->bank}}</td>
                                        <td>{{$branch->is_active}}</td>
                                        <td style="text-align: center;">
                                            <button class="btn btn-foursquare" id="btn-edit-branch"
                                            data-branchid="{{$branch->id}}" data-branchcode="{{$branch->pcc_code}}"
                                            data-branchname="{{$branch->branch_name}}" data-address="{{$branch->address}}"
                                            data-bankgroup="{{$branch->bank}}">
                                                <i class="fa fa-edit"></i> Edit
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
							</tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>

                @include('vendor.adminlte.modals.modal-branch')

			</div>
		</div>
    </div>

@endsection

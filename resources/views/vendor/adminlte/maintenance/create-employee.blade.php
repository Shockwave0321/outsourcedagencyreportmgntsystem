@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Employees
@endsection


@section('main-content')

<div class="container-fluid spark-screen">

    @include('vendor.adminlte.layouts.partials.flashalert')

    <div class="box box-success">
        <div class="box-header with-border">
                <h3 class="box-title text-green"><i class="fa fa-user-plus"></i> Create New Personnel</h3>
        </div>
        <form method="POST" enctype="multipart/form-data" action="{{ action('EmployeeController@insertNewEmployee') }}">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                            <div class="modal-body" >
                                
                                    {{-- photo, fullname --}}
                                    <div class="row">
            
                                        {{-- fullname --}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="firstname" class="col-form-label">Firstname: <em style="color: red;">*</em></label>
                                                <input type="text" class="form-control" id="create-firstname" name="create-firstname" value="{{ old('create-firstname') }}">
                                            </div>
            
                                            <div class="form-group">
                                                <label for="middlename" class="col-form-label">Middlename: <em style="color: red;">*</em></label>
                                                <input type="text" class="form-control" id="create-middlename" name="create-middlename" value="{{ old('create-middlename') }}">
                                            </div>
            
                                            <div class="form-group">
                                                <label for="lastname" class="col-form-label">Lastname: <em style="color: red;">*</em></label>
                                                <input type="text" class="form-control" id="create-lastname" name="create-lastname" value="{{ old('create-lastname') }}">
                                            </div>
                                        </div>
            
                                        {{-- bdate, status, gender--}}
                                        <div class="col-md-4">

                                            {{-- bdate --}}                 
                                            <div class="form-group">
                                                <label for="dob" class="col-form-label">DOB:</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right"  value="{{ old('create-bdate') }}" onkeydown="return false" id="create-bdate" name="create-bdate"> 
                                                </div>                                   
                                            </div>                                                                            
                
                                            {{-- gender --}}
                                            <div class="form-group">
                                                <label for="gender" class="col-form-label">Gender:</label>
                                                <div class="input-group">
                                                    <select type="text" style="display:none"
                                                    data-minimum-results-for-search="Infinity" class="form-control"
                                                    id="select-gender" name="create-gender">
                                                        {{-- <option value="M" {{ old('select-gender') == "M" ? "selected" : "" }}>Male</option>
                                                        <option value="F" {{ old('select-gender')== "F" ? "selected" : ""}}>Female</option> --}}

                                                        <option value="M" {{ old('select-gender','M')=='Male' ? 'selected' : ''  }}>Male</option>
                                                        <option value="F"  {{ old('select-gender','F')=='Female' ? 'selected' : ''  }}>Female</option>
                                                    </select> 
                                                </div>
                                            </div>      
                
                                            {{-- cstatus --}}
                                            <div class="form-group">
                                                <label for="cstatus" class="col-form-label">Civil Status:</label>
                                                <div class="input-group">
                                                    <select type="text" id="select-cstatus" style="display:none"
                                                    data-minimum-results-for-search="Infinity" class="form-control"
                                                    id="create-civil-status" name="create-civil-status" >
                                                        <option value="S">Single</option>
                                                        <option value="M">Married</option>
                                                        <option value="X">Separated</option>
                                                        <option value="W">Widowed</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        {{-- end: bdate, gender,status --}}

                                        {{-- contacts --}}
                                        <div class="col-md-4">
                                            
                                            <div class="form-group">
                                                <label for="mobileno" class="col-form-label">Mobile No.:</label>
                                                <input type="text" class="form-control" id="create-mobile-no" name="create-mobile-no" value="{{ old('create-mobile-no') }}">
                                            </div>
            
                                            <div class="form-group">
                                                <label for="landlineno" class="col-form-label">Home Landline:</label>
                                                <input type="text" class="form-control" id="create-landline-no" name="create-landline-no" value="{{ old('create-landline-no') }}">
                                            </div>

                                            <div class="form-group">
                                                <label for="email1" class="col-form-label">Email: </label>
                                                <input type="text" class="form-control" id="create-email1" name="create-email1" value="{{ old('create-email1') }}" placeholder="primary email">
                                            </div>

                                        </div>
                
                                    </div>
                                    {{-- end: contacts --}}

                                    {{-- employee address --}}
                                    <div class="row">
            
                                        <div class="col-md-8">
            
                                            <div class="form-group">
                                                <label for="address2" class="col-form-label">Address 1:</label>
                                                <input type="text" class="form-control" id="create-address1" name="create-address1" value="{{ old('create-address1') }}"></input>
                                            </div>
            
                                            <div class="form-group">
                                                <label for="address2" class="col-form-label">Address 2:</label>
                                                <input type="text" class="form-control" id="create-address2" name="create-address2" value="{{ old('create-address2') }}"></input>
                                            </div>
            
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="email2" class="col-form-label">SSS No: <em style="color: red;">*</em></label>
                                                <input type="text" class="form-control" id="create-sssno" name="create-sssno" value="{{ old('create-sssno') }}">
                                            </div>

                                            <div class="form-group">
                                                <label for="email2" class="col-form-label">TIN No: <em style="color: red;">*</em></label>
                                                <input type="text" class="form-control" id="create-tinno" name="create-tinno" value="{{ old('create-tinno') }}">
                                            </div>
                                        </div>
            
                                    </div>
                                    {{-- end: employee address --}}
            
                                    {{-- agency --}}
                                    <div class="row">
            
                                        {{-- 1st column --}}
                                        <div class="col-md-4">
            
                                            <div class="form-group">
                                                <label for="agency" class="col-form-label">Agency: <em style="color: red;">*</em></label>                                     
                                                <div class="form-group">
                                                    <select type="text" style="width: 100%;" class="form-control select2"
                                                    id="create-agency" name="create-agency">
                                                        @foreach ($agencies as $agency)
                                                            <option value="{{$agency->id}}">{{$agency->agency_name}}</option>      
                                                        @endforeach
                                                        
                                                    </select>
                                                </div>
                                            </div>
            
                                        </div>
                                        {{-- end of first column --}}
            
                                        {{-- 2nd column --}}
                                        <div class="col-md-4">
            
                                            <div class="form-group">
                                                <label for="shift" class="col-form-label">Service Type: <em style="color: red;">*</em></label>
                                                <div class="input-group">
                                                    <select type="text" style="display:none"
                                                    data-minimum-results-for-search="Infinity" class="form-control"
                                                    id="create-service-type" name="create-service-type">
                                                        @foreach ($services as $service)
                                                            <option value="{{$service->id}}">{{$service->service_description}}</option>      
                                                        @endforeach                                                                                             
                                                    </select>
                                                </div>
                                            </div>
            
                                          
                                        </div>
                                        {{-- end of 2nd column --}}

                                        {{-- date started --}}
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="dob" class="col-form-label">Date Started:</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" onkeydown="return false" 
                                                    id="create-date-started" name="create-date-started" value="{{ old('create-date-started') }}"> 
                                                </div>                                   
                                            </div> 
                                        </div>
                                        {{-- end: date started --}}
                      
                                    </div>
                                    {{-- end of agency --}}  

                                    {{-- branch --}}
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="branched" class="col-form-label">Branched / HO Unit: <em style="color: red;">*</em></label>                                            
                                                <select type="text" class="form-control select2" style="width: 100%;" id="create-branch" name="create-branch" >
                                                    @foreach ($branches as $branch)
                                                        <option value="{{$branch->id}}">{{$branch->branch_name}}</option>      
                                                    @endforeach                                                                                             
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label for="shift" class="col-form-label">Reg. Working Days:</label>
                                                <div class="input-group">
                                                    <select type="text" style="display:none"
                                                    data-minimum-results-for-search="Infinity" class="form-control"
                                                    id="create-working-days" name="create-working-days" value="{{ old('create-working-days') }}">
                                                        <option value="M-F">M-F</option>   
                                                        <option value="MWF">MWF</option>
                                                        <option value="TTH">TTH</option>
                                                        <option value="DAILY">M-F</option>
                                                        <option value="M-SAT">M-F</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label for="shift" class="col-form-label">Reg. Overtime Hrs. :</label>
                                                <div class="input-group">
                                                    <select type="text" style="display:none"
                                                    data-minimum-results-for-search="Infinity" class="form-control"
                                                    id="create-overtime-hrs" name="create-overtime-hrs" value="{{ old('create-overtime-hrs') }}">
                                                        <option value="10:00 PM - 11:00 PM">10:00 PM - 11:00 PM</option>   
                                                        <option value="10:00 PM - 12:00 PM<">10:00 PM - 12:00 PM</option>
                                                        <option value="10:00 PM - 1:00 AM">10:00 PM - 1:00 AM</option>
                                                        <option value="10:00 PM - 2:00 AM">10:00 PM - 2:00 AM</option>
                                                        <option value="10:00 PM - 3:00 AM">10:00 PM - 3:00 AM</option>
                                                        <option value="10:00 PM - 4:00 AM">10:00 PM - 4:00 AM</option>
                                                        <option value="10:00 PM - 5:00 AM">10:00 PM - 5:00 AM</option>
                                                        <option value="10:00 PM - 6:00 AM">10:00 PM - 6:00 AM</option>
                                                        <option value="10:00 PM - 7:00 AM">10:00 PM - 7:00 AM</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>

                                    </div>

                                    <div class="row">
                                        
                                        <div class="col-md-8">

                                            <div class="col-md-6">
                                                <label>Reg. Working Hr. (From):</label>
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control timepicker2" 
                                                            id="create-working-hrs-from" name="create-working-hrs-from" value="{{ old('create-working-hrs-from') }}">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>                                             
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <label>Reg. Working Hr. (To):</label>
                                                <div class="bootstrap-timepicker">
                                                    <div class="form-group">                                             
                                                        <div class="input-group">
                                                            <input type="text" class="form-control timepicker2" 
                                                            id="create-working-hrs-to" name="create-working-hrs-to" value="{{ old('create-working-hrs-to') }}">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </div>                                             
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </div>

                                    </div>
                            
                            </div>
                                                   
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="form-group">
                        <p style="font-style: italic; color: gray;">Note: All fields mark with an asterisk (*) are required.</p>
                    </div>
                </div>

            </div>
            

            <div class="box-footer">
                    <button type="submit" class="btn btn-dropbox pull-right"><i class="fa fa-save"></i> Create Personnel</button>
            </div>
       
        </form>   
    </div>
{{-- container  --}}
</div> 

@endsection()
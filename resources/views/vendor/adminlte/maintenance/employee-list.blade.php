@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Employees
@endsection

@section('main-content')

	<div class="container-fluid spark-screen">

		@include('vendor.adminlte.layouts.partials.flashalert')

		<div class="row">
			<div class="col-md-12">

                {{-- box: employee list --}}
				<div class="box box-danger">
					<div class="box-header">
						<h3 class="box-title">Agency Personnel List</h3>
						<br><br>
					
						<button class="btn btn-success" onclick="location.href='employee/create'"><i class="fa fa-user-plus"></i> Create New Personnel</button>
						<button class="btn btn-success" data-toggle="modal" data-target="#modal-upload-employee-template"><i class="fa fa-file-excel-o"></i> Template Upload</button>
					</div>

					<!-- /.box-header -->
					<div class="box-body">
						<table id="employee-table" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Personnel Name</th>
									<th>Type of Service</th>
									<th>Agency</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody class="tbody-employee-list">
								@foreach ($employees as $employee)
                                    <tr>
                                        <td style="width:50px;">{{$employee->id}}</td>
                                        <td>{{$employee->employee_fullname}}</td>
                                        <td>{{$employee->service_description}}</td>
										<td>{{$employee->agency_name}}</td>
										@if($employee->is_active == 1)
											<td style="width:70px; text-align: center;"><span class="badge bg-green badge-secondary">Active</span></td>
										@else
											<td style="width:70px; text-align: center;"><span class="badge bg-default badge-secondary">Inactive</span></td>
										@endif
										
                                        <td style="text-align: center;">
                                            <button class="btn btn-foursquare" id="btn-edit-employee"
											data-employeeid="{{$employee->id}}" data-employeeno="{{$employee->employee_no}}"
											data-firstname="{{$employee->firstname}}" data-middlename="{{$employee->middlename}}"
                                            data-lastname="{{$employee->lastname}}" data-agency="{{$employee->agency_id}}"
                                            data-supervisor="{{$employee->supervisor}}" data-position="{{$employee->position}}"
                                            data-branch="{{$employee->branch_id}}" data-contractduration="{{$employee->contract_duration}}"
											data-workingdays="{{$employee->reg_working_days}}" data-workinghrs="{{$employee->reg_working_hrs}}"
											data-overtimehrs="{{$employee->reg_overtime_hrs}}" data-isovertime="{{$employee->is_approved_overtime}}" 
											data-bdate="{{$employee->bdate}}"
                                            data-gender="{{$employee->gender}}" data-cstatus="{{$employee->civil_status}}"
                                            data-address1="{{$employee->address1}}" data-address2="{{$employee->address2}}"
                                            data-email1="{{$employee->email1}}" data-email2="{{$employee->email2}}"
											data-mobileno="{{$employee->mobile_no}}" data-landline="{{$employee->home_landline}}" 
											data-status="{{$employee->is_active}}" data-service="{{$employee->service_type}}"
											data-branchid="{{$employee->branch_id}}" data-agencyid="{{$employee->agency_id}}"
											data-serviceid="{{$employee->service_type}}">
                                                <i class="fa fa-edit"></i> Edit
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
							</tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>

                @include('vendor.adminlte.modals.modal-employee')

			</div>
		</div>
	</div>

@endsection

<?php

namespace App\Exports;

use App\Models\Employee;
use App\Models\Agency;
use App\Models\ViewEmployeeTimesheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

class TimesheetExport implements FromView, WithHeadingRow, WithStartRow
{

    public function view(): view
    {

        libxml_use_internal_errors(true);

        $timesheets = collect(ViewEmployeeTimesheet::all());

        return view('adminlte::reports.report-timesheet-conso', compact('timesheets'));

    }

    public function startRow(): int
    {
        return 7;
    } 

    public function headingRow(): int
    {
        return 6;
    }


}
<?php
namespace App\Exports;

use App\Models\AgenciesUploadException;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\WithHeadings;
// use Maatwebsite\Excel\Concerns\WithMapping;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AgencyUploadException implements FromView, WithHeadingRow, WithStartRow
{

    public function view(): view
    {

        libxml_use_internal_errors(true);

        $exceptions = collect(AgenciesUploadException::all());

        return view('adminlte::reports.report-agency-upload-exception', compact('exceptions'));

    }

    // public function headings(): array
    // {
    //     return [
            // 'Accredited Agency Name', 
            // 'Lastname1',
            // 'Firstname1',
            // 'Middlename1',
            // 'Lastname2',
            // 'Firstname2',
            // 'Middlename2',
            // 'Agency Branch',
            // 'Services',
            // 'SEC Registration No.',
            // 'Date of Registration', 
            // 'Office Phone',
            // 'Mobile Phone',
            // 'Address 1',
            // 'Address 2',
            // 'Personal Email',
            // 'Work/Business Email',
            // 'TIN',
            // 'Accreditation No.',
            // 'Expiry Date',
            // 'Result'
    //     ];
    // }

    // public function map($exception): array
    // {
    //     return [
    //         $exception->agency_name,
    //         $exception->cp_lastname,
    //         $exception->cp_firstname,
    //         $exception->cp_middlename,
    //         $exception->po_lastname,
    //         $exception->po_firstname,
    //         $exception->po_middlename,
    //         $exception->branch,
    //         '',
    //         $exception->sec_reg_no, 
    //         $exception->reg_date, 
    //         $exception->landline_no,
    //         $exception->mobile_no,
    //         $exception->address1,
    //         $exception->address2,
    //         $exception->email1,
    //         $exception->email2, 
    //         $exception->tin_no,
    //         $exception->accreditation_no,
    //         $exception->accre_expiry_date,
    //         $exception->result,
    //         // Date::dateTimeToExcel($exception->created_at),
    //     ];
    // }

    public function headingRow(): int
    {
        return 7;
    }

    public function startRow(): int
    {
        return 8;
    } 


    // public function title(): string
    // {
    //     return 'Sheet1';
    // }

    // public function collection()
    // {
    //     return AgenciesUploadException::all();
    // }

}

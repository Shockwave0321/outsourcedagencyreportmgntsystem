<?php
namespace App\Exports;

use App\Models\EmployeeUploadException;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\WithHeadings;
// use Maatwebsite\Excel\Concerns\WithMapping;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UploadException implements FromView, WithHeadingRow, WithStartRow
{

    public function view(): view
    {

        libxml_use_internal_errors(true);

        $exceptions = collect(EmployeeUploadException::all());

        return view('adminlte::reports.report-employee-upload-exception', compact('exceptions'));

    }


    // public function headings(): array
    // {
    //     return [
    //         'First Name',
    //         'Middle Name', 
    //         'Last Name',
    //         'Profit Cost Center Code',
    //         'Branch/Unit Name',
    //         'Assigned Accredited Agency',
    //         'Services',
    //         'Bank',
    //         'Reg. Working Days',
    //         'Reg. Working Hrs.',
    //         'Reg. Overtime Hrs.',
    //         'Date of Birth',
    //         'Gender',
    //         'Civil Status', 
    //         'Mobile Phone', 
    //         'Home Phone',
    //         'Address 1',
    //         'Address 2',
    //         'Personal Email',
    //         'Work/Business Email',
    //         'SSS',
    //         'TIN',
    //         'Start Date',
    //         'ExceptionID'
    //     ];
    // }

    // public function map($exception): array
    // {
    //     return [
    //         $exception->firstname,
    //         $exception->middlename,
    //         $exception->lastname,
    //         $exception->pcc_code,
    //         $exception->branch_name, 
    //         $exception->agency_name,
    //         $exception->service_type,
    //         $exception->bank,
    //         $exception->reg_working_days,
    //         $exception->reg_working_hrs,
    //         $exception->reg_overtime_hrs,
    //         $exception->bdate,
    //         $exception->gender,
    //         $exception->civil_status, 
    //         $exception->mobile_no, 
    //         $exception->home_landline,
    //         $exception->address1,
    //         $exception->address2,
    //         $exception->email1,
    //         $exception->email2,
    //         $exception->sss_no,
    //         $exception->tin_no,
    //         $exception->date_started,
    //         $exception->id
    //         // Date::dateTimeToExcel($exception->created_at),
    //     ];
    // }

    public function headingRow(): int
    {
        return 7;
    }

    public function startRow(): int
    {
        return 8;
    } 

    // public function collection()
    // {
    //     return EmployeeUploadException::all();
    // }
}

<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ViewEmployeeTimesheet extends Model
{
    public function getEmployeeFullnameAttribute()
    {
        if ($this->middlename == '')
        {
            return $this->firstname.' '.$this->lastname;
        }
        else
        {
            return $this->firstname.' '.mb_substr($this->middlename,0,1).'. '.$this->lastname;
        }

    }

    public function getEmployeeFullnameFormalAttribute()
    {
        if ($this->middlename == '')
        {
            return $this->lastname.' '.$this->firstname;
        }
        else
        {
            return $this->lastname.', '.$this->firstname.' '.mb_substr($this->middlename,0,1).'. ';
        }

    }

    public function getTotalHoursAttribute()
    {
        $timeIn = strtotime($this->time_in);
        $timeOut = strtotime($this->time_out);
        $totalHrs = abs($timeIn - $timeOut) / 3600;

        return sprintf('%0.2f', $totalHrs);
    }

    public function getSubTotalHoursAttribute()
    {
        $totalhours = 12;
        return $totalhours;
    }

    public function getLogInDateAttribute()
    {
        return Carbon::parse($this->time_in)->format('m/d/Y');
    }

    public function getDayNameAttribute()
    {
        return Carbon::parse($this->time_in)->format('l');
    }

    public function getFormatTimeInAttribute()
    {
        return Carbon::parse($this->time_in)->format('h:i A');
    }

    public function getFormatTimeOutAttribute()
    {
        return Carbon::parse($this->time_out)->format('h:i A');
    }


}

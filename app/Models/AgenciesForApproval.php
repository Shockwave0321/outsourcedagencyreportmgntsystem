<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgenciesForApproval extends Model
{
    public $fillable = [
        'agency_code',
        'agency_name', 
        'cp_firstname', 
        'cp_middlename', 
        'cp_lastname', 
        'po_firstname', 
        'po_middlename', 
        'po_lastname', 
        'branch',
        'address1',
        'address2',
        'tin_no', 
        'reg_date',
        'sec_reg_no',
        'landline_no', 
        'mobile_no', 
        'email1',
        'email2', 
        'accreditation_no',
        'accre_expiry_date',
    ];

}

<?php

namespace App\Models;
use App\Models\Branch;
// use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Approver extends Authenticatable
{
    // use Notifiable;

    protected $guard = 'approver';

    protected $fillable = [
        'name','email','password','role'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

}

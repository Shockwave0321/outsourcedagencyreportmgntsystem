<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SysAdmin extends Authenticatable
{
    protected $guard = 'sysadmin';

    protected $fillable = [
        'name','email','password','role'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Timesheet extends Model
{
    public $fillable = [
        'employee_id',
        'time_in',
        'time_out',
        'is_approve_ot_reg'
    ];

    public function employees()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function getTotalHoursAttribute(){
        $timeIn = strtotime($this->time_in);
        $timeOut = strtotime($this->time_out);
        $totalHrs = abs($timeIn - $timeOut) / 3600;

        return sprintf('%0.2f', $totalHrs);
    }

    public function getLogInDateAttribute()
    {
        return Carbon::parse($this->time_in)->format('m/d/Y');
    }
    public function getFormatTimeInAttribute()
    {
        return Carbon::parse($this->time_in)->format('h:i:s A');
    }
    public function getFormatTimeOutAttribute()
    {
        return Carbon::parse($this->time_out)->format('h:i:s A');
    }


}

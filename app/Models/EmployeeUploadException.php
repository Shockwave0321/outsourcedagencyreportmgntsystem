<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeUploadException extends Model
{
    public $fillable = [
        'uuid',
        'employee_no',
        'firstname',
        'middlename', 
        'lastname',
        'bdate',
        'civil_status',
        'gender',
        'mobile_no',
        'home_landline',
        'email1',
        'address1',
        'address2',
        'sss_no',
        'tin_no',
        'agency_id',
        'service_type',
        'reg_working_days',
        'reg_working_hrs',
        'reg_overtime_hrs',
        'bank',
        'branch_id',
        'pcc_code',
        'date_started'
    ];
}

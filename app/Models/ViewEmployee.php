<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewEmployee extends Model
{
    public function getEmployeeFullnameAttribute()
    {
        if ($this->middlename == '')
        {
            return $this->firstname.' '.$this->lastname;
        }
        else
        {
            return $this->firstname.' '.mb_substr($this->middlename,0,1).'. '.$this->lastname;
        }
    }

    public function getEmployeeFullnameFormalAttribute()
    {
        if ($this->middlename == '')
        {
            return $this->lastname.' '.$this->firstname;
        }
        else
        {
            return $this->lastname.', '.$this->firstname.' '.mb_substr($this->middlename,0,1).'. ';
        }

    }

    public function getEmployeeStatusAttribute()
    {
        if ($this->is_active == 1)
        {
            return 'Active';
        }
        else
        {
            return 'Inactive';
        }
    }
}

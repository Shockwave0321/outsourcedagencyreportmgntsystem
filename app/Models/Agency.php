<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    public $fillable = [
        'agency_code',
        'agency_name', 
        'contact_person',
        'principal_owners', 
        'branch',
        'address1',
        'address2',
        'tin_no', 
        'reg_date',
        'sec_reg_no',
        'landline_no', 
        'mobile_no', 
        'email1',
        'email2', 
        'accreditation_no',
        'accre_expiry_date',
    ];

    public function employees()
    {
        return $this->belongsTo('\App\Models\Employee');
    }

    public function agency_services()
    {
        return $this->hasMany('\App\Models\AgencyService');
    }

    public function getIsActiveAttribute()
    {
        if ($this->is_active = 1)
        {
            return 'Active';
        }
        else
        {
            return 'Inactive';
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgencyService extends Model
{
    protected $fillable = [
        'services',
        'agency_id'
    ];

    public function agencies()
    {
        return $this->belongsTo('\App\Models\Agency');
    }

}

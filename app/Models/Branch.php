<?php

namespace App\Models;

use App\Models\Approver;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public function getIsActiveAttribute()
    {
        if ($this->is_active = 1)
        {
            return 'Active';
        }
        else
        {
            return 'Inactive';
        }
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function approver()
    {
        return $this->hasMany(Approver::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Traits\UuidTrait;

class Employee extends Model
{

    use UuidTrait;

    public $fillable = [
        'uuid',
        'employee_no',
        'firstname',
        'middlename', 
        'lastname',
        'bdate',
        'civil_status',
        'gender',
        'mobile_no',
        'home_landline',
        'email1',
        'address1',
        'address2',
        'sss_no',
        'tin_no',
        'agency_id',
        'service_type',
        'reg_working_days',
        'reg_working_hrs',
        'reg_overtime_hrs',
        'bank',
        'branch_id',
        'pcc_code',
        'date_started'
    ];

    public function timesheets()
    {
        return $this->hasMany(Timesheet::class, 'id');
    }

    public function agencies()
    {
        return $this->hasOne('App\Models\Agency');
    }

    public function service_types()
    {
        return $this->hasOne('App\Models\ServiceType', 'employee_no');
    }

    public function getEmployeeFullnameAttribute()
    {
        if ($this->middlename == '')
        {
            return $this->firstname.' '.$this->lastname;
        }
        else
        {
            return $this->firstname.' '.mb_substr($this->middlename,0,1).'. '.$this->lastname;
        }

    }

    public function getEmployeeFullnameFormalAttribute()
    {
        if ($this->middlename == '')
        {
            return $this->lastname.' '.$this->firstname;
        }
        else
        {
            return $this->lastname.', '.$this->firstname.' '.mb_substr($this->middlename,0,1).'. ';
        }

    }

    public function getEmployeeStatusAttribute()
    {
        if ($this->is_active == 1)
        {
            return 'Active';
        }
        else
        {
            return 'Inactive';
        }
    }
}

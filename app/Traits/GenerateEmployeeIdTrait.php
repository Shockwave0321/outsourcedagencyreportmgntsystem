<?php

namespace App\Traits;

trait GenerateEmployeeIdTrait
{

    public static function bootEmployeeIdGeneration()
    {
        static::saving(function ($model){
            $model->employee_no = $model->generateEmployeeId($model->id.date('Y'));
        });
    }

    public function generateEmployeeId($longint)
    {

        return date('Y');

    }
}
<?php

namespace App\Traits;

use App\Models\Employee;
use Cyberduck\LaravelExcel\Contract\ParserInterface;

trait Importer
{
    public function transform($row, $header)
    {
        $model = new Employee();
        $model->firstname= $row[0];
        $model->middlename = $row[1];
        // We can manunipulate the data before returning the object
        $model->bdate = new \Carbon($row[4]);
        
        return $model;  
    }
}
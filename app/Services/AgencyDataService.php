<?php

namespace App\Services;

use App\Repositories\AgencyRepository;
use App\Models\Agency;
use App\Models\ AgenciesForApproval;
use App\Models\AgenciesApprovalRequest;
use Illuminate\Http\Request;
use Auth;

class AgencyDataService
{

    public function __construct(AgencyRepository $agency)
    {
        $this->agency = $agency;
    }

    public function getAgencies()
    {
        $agency = Agency::all();
        
        return $agency;
    }

    public function checkDuplicateData(Request $request)
    {  
        $agency = Agency::where('agency_name',  $request->input('create-agency-name'))->first();

        return $agency;
    }

    public function checkDuplicateDataOnCurrentApproval(Request $request)
    {  
        $agency = AgenciesForApproval::where('agency_name',  $request->input('create-agency-name'))->first();

        return $agency;
    }

    public function create(Request $request)
    {

        if(!$this->checkDuplicateDataOnCurrentApproval($request)) {

            $agencyRequests = new AgenciesApprovalRequest();
            $agencyRequests->module = 'Agency Maintenance';
            $agencyRequests->action_type = 'insert';
            $agencyRequests->branch_id = Auth::user()->branch_id;
            $agencyRequests->user_id = Auth::user()->id;
            $agencyRequests->remarks = 'Manual creation of agency ('.$request->get('create-agency-name').')';
            $agencyRequests->save();

            $agency = new AgenciesForApproval();
            $agency->agency_name = $request->get('create-agency-name');
            $agency->address1 = $request->get('create-address1');
            $agency->address2 = $request->get('create-address2');
            $agency->tin_no = $request->get('create-tin-no');
            $agency->reg_date = $request->get('create-reg-date');
            $agency->sec_reg_no= $request->get('create-sec-reg-no');
            $agency->accreditation_no= $request->get('create-accre-no');
            $agency->accre_expiry_date= $request->get('create-accre-expiry-date');
            $agency->landline_no = $request->get('create-landline-no');
            $agency->mobile_no = $request->get('create-mobile-no');
            $agency->email1 = $request->get('create-email1');
            $agency->email2 = $request->get('create-email2');
            $agency->po_firstname = $request->get('create-po-firstname');
            $agency->po_middlename = $request->get('create-po-middlename');
            $agency->po_lastname = $request->get('create-po-lastname');
            $agency->cp_firstname = $request->get('create-cp-firstname');
            $agency->cp_middlename = $request->get('create-cp-middlename');
            $agency->cp_lastname = $request->get('create-cp-lastname');
            $agency->is_accredited = $request->get('create-accredited-value');
            $agency->save();

            $agencyId = $agencyRequests->id;

            if($request->has('createservices')) {

                $insertData = [];
                $multipleValues = $request->get('createservices');
                foreach($multipleValues as $value)
                {
                    $insertData['agency_id'] = $agencyId;
                    $insertData['services'] = $value;
                    \DB::table('agency_services')->insert($insertData);
                }

            }
            
            $latestId = $agency->id;
            
            if(strlen((string)$latestId) == 1) {
                $latestId = '000'.$latestId;
            } elseif(strlen((string)$latestId) == 2) {
                $latestId = '00'.$latestId;
            } elseif(strlen((string)$latestId) == 3) {
                $latestId = '0'.$latestId;
            } else {
                $latestId =  $agency->id;
            }

            AgenciesForApproval::where('id', $agency->id)->update([
                    'approval_batch_no' => $agencyRequests->id,
                    'agency_code' => substr($agency->agency_name, 0, 3).$latestId
                ]);

            return $agency;   

            }

    }

    public function update(Request $request)
    {

        $agencyRequests = new AgenciesApprovalRequest();
        $agencyRequests->module = 'Agency Maintenance';
        $agencyRequests->action_type = 'update';
        $agencyRequests->branch_id = Auth::user()->branch_id;
        $agencyRequests->user_id = Auth::user()->id;
        $agencyRequests->remarks = $request->get('edit-purpose');
        $agencyRequests->save();

        $agency = new AgenciesForApproval();
        $agency->agency_code = $request->get('edit-agency-code');
        $agency->agency_name = $request->get('edit-agency-name');
        $agency->address1 = $request->get('edit-address1');
        $agency->address2 = $request->get('edit-address2');
        $agency->tin_no = $request->get('edit-tin-no');
        $agency->reg_date = $request->get('edit-reg-date');
        $agency->sec_reg_no= $request->get('edit-sec-reg-no');
        $agency->accreditation_no= $request->get('edit-accre-no');
        $agency->accre_expiry_date= $request->get('edit-accre-expiry-date');
        $agency->landline_no = $request->get('edit-landline-no');
        $agency->mobile_no = $request->get('edit-mobile-no');
        $agency->email1 = $request->get('edit-email1');
        $agency->email2 = $request->get('edit-email2');
        $agency->po_firstname = $request->get('edit-po-firstname');
        $agency->po_middlename = $request->get('edit-po-middlename');
        $agency->po_lastname = $request->get('edit-po-lastname');
        $agency->cp_firstname = $request->get('edit-cp-firstname');
        $agency->cp_middlename = $request->get('edit-cp-middlename');
        $agency->cp_lastname = $request->get('edit-cp-lastname');
        $agency->is_accredited = $request->get('edit-accredited-value');
        $agency->save();

        AgenciesForApproval::where('id',  $agency->id)->update([
            'approval_batch_no' => $agencyRequests->id,
        ]);

        return $agency;


    }


}
<?php

namespace App\Services;

use App\Repositories\EmployeeRepository;
use App\Models\ViewEmployee;
use App\Models\Employee;
use App\Models\EmployeeForApproval;
use App\Models\ApprovalRequest;
use App\Models\AuditLog;
use Illuminate\Http\Request;
USE Auth;

class EmployeeService
{

    public function __construct(EmployeeRepository $employee)
    {
        $this->employee = $employee;
    }

    public function getEmployees()
    {
        $employee = ViewEmployee::all();
        
        return $employee;
    }

    public function checkDuplicateData(Request $request)
    {  
        $employee = Employee::where('firstname',  $request->input('create-firstname'))
        ->where('lastname',  $request->input('create-lastname'))
        ->where('sss_no',  $request->input('create-sssno'))
        ->where('bdate',  $request->input('create-bdate'))
        ->count();

        return $employee;
    }

    public function generateEmployeeNo()
    {

    }

    // request: create new emplyee
    public function create(Request $request)
    {
        
        $approval = new ApprovalRequest();
        $approval->module = 'Employee Maintenance';
        $approval->action_type = 'insert';
        $approval->branch_id = Auth::user()->branch_id;
        $approval->user_id = Auth::user()->id;
        $approval->remarks = 'Manual creation of personnel ('.$request->get('create-firstname').' '.$request->get('create-lastname').')'; //create an accessor for this later
        $approval->save();

        $employee = EmployeeForApproval::create([
            'firstname' => $request->get('create-firstname'),
            'middlename' => $request->get('create-middlename'),
            'lastname' => $request->get('create-lastname'),
            'bdate' => $request->get('create-bdate'),
            'civil_status' => $request->get('create-civil-status'),
            'gender' => $request->get('create-gender'),
            'mobile_no' => $request->get('create-mobile-no'),
            'home_landline' => $request->get('create-landline-no'),
            'email1' => $request->get('create-email1'),
            'address1' => $request->get('create-address1'),
            'address2' => $request->get('create-address2'),
            'sss_no' => $request->get('create-sssno'),
            'tin_no' => $request->get('create-tinno'),
            'agency_id' => $request->get('create-agency'),
            'service_type' => $request->get('create-service-type'),
            'reg_working_days' => $request->get('create-working-days'),
            'reg_working_hrs' => $request->get('create-working-hrs-from').' - '.$request->get('create-working-hrs-to'),
            'reg_overtime_hrs' => $request->get('create-overtime-hrs'),
            'branch_id' => $request->get('create-branch'),
            'date_started' => $request->get('create-date-started')
        ]);

        $forApproval = new EmployeeForApproval();
        $forApproval->firstname = $request->get('create-firstname');
        $forApproval->save();

        $employee_no = $forApproval->id;
        
        if(strlen((string)$employee_no) == 1) {
            $employee_no = '000'.$employee_no;
        } elseif(strlen((string)$employee_no) == 2) {
            $employee_no = '00'.$employee_no;
        } elseif(strlen((string)$employee_no) == 3) {
            $employee_no = '0'.$employee_no;
        } else {
            $employee_no = $employee->id;
        }

        //update employee details
        EmployeeForApproval::where('id', $employee->id)->update([
                'approval_batch_no' => $approval->id,
                'employee_no' => date('Y').$employee_no
            ]);

        EmployeeForApproval::where('id', $forApproval->id)->delete();

        return $employee; 

    }
    
    // request: update employee
    public function update(Request $request)
    {
        // $employee = Employee::where('id', $request->get('edit-employeeid'))->update([
        $apprequest = new ApprovalRequest();
        $apprequest->module = 'Employee Maintenance';
        $apprequest->action_type = 'update';
        $apprequest->field_name = '';//change this -> purpose of update, list of fields updated
        $apprequest->old_value = '';
        $apprequest->new_value = '';
        $apprequest->branch_id = Auth::user()->branch_id;
        $apprequest->user_id = Auth::user()->id;
        $apprequest->remarks = $request->get('edit-purpose');
        $apprequest->status = 0;
        $apprequest->save();

      
        $employee = new EmployeeForApproval();
        $employee->approval_batch_no = $apprequest->id;
        $employee->employee_no = $request->get('edit-employee-no');
        $employee->firstname = $request->get('edit-firstname');
        $employee->middlename = $request->get('edit-middlename');
        $employee->lastname = $request->get('edit-lastname');
        $employee->bdate = $request->get('edit-bdate');
        $employee->civil_status = $request->get('edit-civil-status');
        $employee->gender = $request->get('edit-gender');
        $employee->mobile_no = $request->get('edit-mobile-no');
        $employee->home_landline = $request->get('edit-landline-no');
        $employee->email1 = $request->get('edit-email1');
        $employee->address1 = $request->get('edit-address1');
        $employee->address2 = $request->get('edit-address2');
        $employee->agency_id = $request->get('edit-select-agency');
        $employee->reg_working_days = $request->get('edit-edit-working-days');
        $employee->reg_working_hrs = $request->get('create-working-hrs-from').' - '.$request->get('create-working-hrs-to');
        $employee->reg_overtime_hrs = $request->get('edit-edit-overtime-hrs');
        $employee->branch_id = $request->get('edit-select-branch');
        $employee->service_type = $request->get('edit-select-service');
        $employee->sss_no = $request->get('edit-sssno');
        $employee->tin_no = $request->get('edit-tinno');
        $employee->save();

        EmployeeForApproval::where('id',  $employee->id)->update([
                'employee_no' => $request->get('edit-employee-no'),
                'approval_batch_no' => $apprequest->id,
            ]);

        return $employee;
    }

    public function find($id)
    {
        $employee = Employee::find($id);

        return $employee;
    }

    public function delete(Request $request)
    {
        $employee = Employee::where('id', $request->get('edit-employeeid'))->delete();

        return $employee;
    }

    public function insertToAuditLog()
    {
        $auditlog = new ApprovalRequest();
    }

    public function insertToApprovalRequest()
    {
        $apprequest = new ApprovalRequest();
        $apprequest->module = 'Employee Maintenance';
        $apprequest->action_type = 'update';
        $apprequest->field_name = '';//change this -> purpose of update, list of fields updated
        $apprequest->old_value = '';
        $apprequest->new_value = '';
        $apprequest->branch_id = Auth::user()->branch_id;
        $apprequest->user_id = Auth::user()->id;
        $apprequest->status = 0;
        $apprequest->save();

        return $apprequest;
    }



}
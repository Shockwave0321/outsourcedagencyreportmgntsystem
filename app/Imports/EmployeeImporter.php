<?php

namespace App\Imports;

use App\Models\Employee;
use App\Models\EmployeeUploadException;
use App\Models\ApprovalRequest;
use App\Models\EmployeeForApproval;
use App\Models\ExceptionEntry;
use App\Models\Branch;
use App\Models\Agency;
use App\Models\ServiceType;
use Carbon\Carbon;
use Auth;
use Config;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithLimit;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;

HeadingRowFormatter::default('none');

class EmployeeImporter implements ToCollection, WithStartRow, WithHeadingRow, WithCalculatedFormulas
{
    use WithConditionalSheets;

    public $sheetNames;
    public $sheetData;
    public $invalidData;
    public $emitter;
	
    public function __construct(){
        $this->sheetNames = [];
        $this->sheetData = [];

        $this->invalidData =0;
        $this->emitter=0;
    }

    public function collection(Collection $collection)
    {
        $this->sheetData[] = $collection;

        foreach ($collection as $row => $value) 
        {

            $duplicate = Employee::where('firstname', $value['First Name'])
                        ->where('lastname', $value['Last Name'])
                        ->where('bdate', $value['Date of Birth'])
                        ->where('sss_no', $value['SSS'])
                        ->count();

            if(($duplicate) && ($value['First Name']) && ($value['Last Name'])){

                $exception = new EmployeeUploadException();
                $exception->firstname = $value['First Name'];
                $exception->middlename = $value['Middle Name']; 
                $exception->lastname = $value['Last Name'];
                $exception->reg_working_days = $value['Reg. Working Days'];
                $exception->reg_working_hrs = $value['Reg. Working Hrs.'];
                $exception->reg_overtime_hrs = $value['Reg. Overtime Hrs.'];
                $exception->bdate = $value['Date of Birth'];
                $exception->gender = substr($value['Gender'],0 ,1);
                $exception->civil_status = substr($value['Civil Status'],0 ,1); 
                $exception->mobile_no =$value['Mobile Phone']; 
                $exception->home_landline = $value['Home Phone'];
                $exception->address1 = $value['Address 1']; 
                $exception->address2 = $value['Address 2']; 
                $exception->email1 = $value['Personal Email'];
                $exception->email2 = $value['Work/Business Email']; 
                $exception->bank = $value['Bank'];
                $exception->pcc_code = $value['Profit Cost Center Code']; 
                $exception->agency_name =  $value['Assigned Accredited Agency'];
                $exception->branch_id =  Auth::user()->branch_id;
                // $exception->branch_name =  $value['Branch/Unit Name'];
                $exception->service_type = $value['Services']; 
                $exception->sss_no = $value['SSS']; 
                $exception->tin_no = $value['TIN'];
                $exception->date_started = $value['Start Date'];
                $exception->save();

                $this->invalidData += 1;

            } elseif(($value['First Name']) && ($value['Last Name']) && (!$value['Date of Birth']) && (!$value['SSS'])) { 
                    // (!$value['Branch/Unit Name']) && (!$value['Assigned Accredited Agency']) && (!$value['Services']) || 
                    // ($value['Profit Cost Center Code']) && ($value['Address 1']) && ($value['Gender']) && ($value['Civil Status'])) {
               
                $exception = new EmployeeUploadException();
                $exception->firstname = $value['First Name'];
                $exception->middlename = $value['Middle Name']; 
                $exception->lastname = $value['Last Name'];
                $exception->reg_working_days = $value['Reg. Working Days'];
                $exception->reg_working_hrs = $value['Reg. Working Hrs.'];
                $exception->reg_overtime_hrs = $value['Reg. Overtime Hrs.'];
                $exception->bdate = $value['Date of Birth'];
                $exception->gender = substr($value['Gender'], 0 ,1);
                $exception->civil_status = substr($value['Civil Status'], 0 ,1); 
                $exception->mobile_no =$value['Mobile Phone']; 
                $exception->home_landline = $value['Home Phone'];
                $exception->address1 = $value['Address 1']; 
                $exception->address2 = $value['Address 2']; 
                $exception->email1 = $value['Personal Email'];
                $exception->email2 = $value['Work/Business Email']; 
                $exception->bank = $value['Bank'];
                $exception->pcc_code = $value['Profit Cost Center Code']; 
                $exception->agency_name =  $value['Assigned Accredited Agency'];
                // $exception->branch_name =  $value['Branch/Unit Name'];
                $exception->service_type = $value['Services']; 
                $exception->sss_no = $value['SSS']; 
                $exception->tin_no = $value['TIN'];
                $exception->date_started = $value['Start Date'];
                $exception->save();

                $this->invalidData += 1;

            } elseif((!$value['First Name']) && (!$value['Last Name']) && (!$value['Date of Birth']) && (!$value['SSS']) || (!$value['Profit Cost Center Code']) || (!$value['Assigned Accredited Agency']) || (!$value['Services'])
            || (!$value['Branch/Unit Name'])) {

                
            } elseif(($value['First Name']) || ($value['Last Name']) || ($value['Date of Birth']) || ($value['SSS']) 
            || ($value['Profit Cost Center Code']) || ($value['Assigned Accredited Agency']) || ($value['Services'])
            || ($value['Branch/Unit Name']) ) {

                $this->emitter += 1;

                if ( $this->emitter == 1) {
                    $approval = new ApprovalRequest();
                    $approval->module = 'Template Upload';
                    $approval->action_type = 'upload';
                    $approval->branch_id = Auth::user()->branch_id;
                    $approval->user_id = Auth::user()->id;
                    $approval->remarks = 'Bulk Upload';
                    $approval->save();
                }

                $emp = EmployeeForApproval::create([
                    'lastname' => $value['Last Name'],
                    'firstname' => $value['First Name'],
                    'middlename' => $value['Middle Name'],                  
                    'reg_working_days' => $value['Reg. Working Days'],
                    'reg_working_hrs' => $value['Reg. Working Hrs.'],
                    'reg_overtime_hrs' => $value['Reg. Overtime Hrs.'], 
                    'bdate' => $value['Date of Birth'],
                    'gender' => substr($value['Gender'], 0,1),
                    'civil_status' => substr($value['Civil Status'], 0,1), 
                    'mobile_no' =>$value['Mobile Phone'], 
                    'home_landline' => $value['Home Phone'],
                    'address1' => $value['Address 1'], 
                    'address2' => $value['Address 2'], 
                    'email1' => $value['Personal Email'],
                    'email2' => $value['Work/Business Email'], 
                    'bank' => $value['Bank'],
                    'pcc_code' => $value['Profit Cost Center Code'],
                    'sss_no' => $value['SSS'], 
                    'tin_no' => $value['TIN'],
                    'date_started' => $value['Start Date'],
                ]);

                // generate employee no.
                $latest_id = $emp->id;

                if(strlen((string)$latest_id) == 1) {
                    $latest_id = '000'.$latest_id;
                } elseif(strlen((string)$latest_id) == 2) {
                    $latest_id = '00'.$latest_id;
                } elseif(strlen((string)$latest_id) == 3) {
                    $latest_id = '0'.$latest_id;
                } else {
                    $latest_id = $latest_id;
                }

                //get agency Id
                $assignedBranch = Branch::where('branch_name', $value['Branch/Unit Name'])->first();

                //get agency Id
                $assignedAgency = Agency::where('agency_name', $value['Assigned Accredited Agency'])->first();

                //get service type Id
                $serviceType = ServiceType::where('service_description','like', '%'.$value['Services'].'%')->first();

                //update employee details
                EmployeeForApproval::where('id', $emp->id)->update([                       
                        'approval_batch_no' => $approval->id,
                        'employee_no' => date('Y').$latest_id,
                        'branch_id' => $assignedBranch->id,
                        'agency_id' => $assignedAgency->id,
                        'service_type' => $serviceType->id,
                    ]);

                //remove in the exception table
                EmployeeUploadException::where('id', $value['ExceptionID'])->delete();

            } else {

            }


        }


    }

    // public function sheets(): array
    // {
    //     $sheets = ['Sheet2'];

    //     return $sheets;
    // }

    // public function limit(): int
    // {
    //     return 100;
    // }

    public function headingRow(): int
    {
        return 7;
    }

    public function startRow(): int
    {
        return 8;
    } 

    public function conditionalSheets(): array
    {
        return [
            'Sheet1' => new EmployeeImporter(),
          
        ];
    }

}
<?php

namespace App\Imports;

use App\Models\Employee;
// use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
// use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class ImportEmployeeExcel implements ToCollection
// ToModel, WithHeadingRow
{

    public function collection(Collection $rows)
    {
        $counter = 1;
        foreach ($rows as $row => $val ) 
        {
            Employee::create([
                'firstname' => $val->firstname,
                // 'middlename' => $val->middlename, 
                // 'lastname' => $row[2],
                // 'shift_schedule' => $row[3], 
                // 'bdate' => $row[4], 
                // 'gender' => $row[5],
            ]);

            if($counter == count($rows)) continue; // this will skip to next iteration if last element encountered.
            $counter++;


        }
    }


    // public function model(array $row) 
    // {
    //     return new Employee([
    //        'firstname'     => $row[0],
    //        'middlename'     => $row[1], 
    //        'lastname'     => $row[2],
    //        'shift_schedule' => $row[3], 
    //        'bdate' => $row[4], 
    //        'gender' => $row[5],
    //        'civil_status' => $row[6], 
    //        'mobile_no' => $row[7], 
    //        'home_landline' => $row[8],
    //        'address1' => $row[9], 
    //        'address2' => $row[10], 
    //        'email1' => $row[11],
    //        'email2' => $row[12], 
    //        'branch_id' => $row[13], 
    //        'agency_id' => $row[14],
    //        'service_type' => $row[15], 
    //        'sss_no' => $row[16], 
    //        'tin_no' => $row[17],
    //        'date_started' => $row[18],
    //        'is_active' => 1,
    //        'created_at' => '',
    //        'updated_at' => ''
    //     ]);
    // }

    // public function headingRow(): int
    // {
    //     return 0;
    // }
}
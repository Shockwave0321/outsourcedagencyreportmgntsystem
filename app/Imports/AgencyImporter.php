<?php

namespace App\Imports;

use App\Models\Agency;
use App\Models\AgenciesUploadException;
use App\Models\AgenciesApprovalRequest;
use App\Models\AgenciesForApproval;
use Auth;
use Config;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\AfterSheet;

// HeadingRowFormatter::default('none');

class AgencyImporter implements ToCollection, WithStartRow, WithHeadingRow
{
    use WithConditionalSheets;
    // use RegistersEventListeners;

    public $sheetNames;
    public $sheetData;
    public $invalidData;
    public $emitter;
	
    public function __construct(){
        $this->sheetNames = [];
        $this->sheetData = [];

        $this->invalidData =0;
        $this->emitter=0;
    }

    public function collection(Collection $collection)
    {
        $this->sheetData[] = $collection;

        foreach ($collection as $row => $value) 
        {
            
            $duplicate = Agency::where('agency_name', $value['Accredited Agency Name'])->first();
            $duplicateOnApproval = AgenciesForApproval::where('agency_name', $value['Accredited Agency Name'])->first();

            if(($duplicate) || ($duplicateOnApproval)){
             
                $exception = new AgenciesUploadException();
                $exception->agency_name = $value['Accredited Agency Name'];
                $exception->cp_firstname = $value['Firstname1']; 
                $exception->cp_middlename = $value['Firstname1'];
                $exception->cp_lastname = $value['Middlename1'];
                $exception->branch = $value['Agency Branch'];
                $exception->address1 = $value['Address 1']; 
                $exception->address2 = $value['Address 2'];
                $exception->tin_no = $value['TIN'];
                $exception->reg_date = $value['Date of Registration']; 
                $exception->sec_reg_no = $value['SEC Registration No.']; 
                $exception->accreditation_no = $value['Accreditation No.']; 
                $exception->accre_expiry_date = $value['Expiry Date']; 
                $exception->mobile_no =$value['Mobile Phone']; 
                $exception->landline_no = $value['Office Phone'];
                $exception->email1 = $value['Personal Email'];
                $exception->email2 = $value['Work/Business Email']; 
                $exception->po_firstname = $value['Firstname2']; 
                $exception->po_middlename = $value['Firstname2'];
                $exception->po_lastname = $value['Middlename2'];
                $exception->branch_id = Auth::user()->branch_id;
                $exception->result = 'Duplicate Entry';
                $exception->save();

                $this->invalidData += 1;

            } elseif ( ($value['Accredited Agency Name']) 
                && (!$value['Accreditation No.']) && (!$value['SEC Registration No.']) ) {

                $exception = new AgenciesUploadException();
                $exception->agency_name = $value['Accredited Agency Name'];
                $exception->cp_firstname = $value['Firstname1']; 
                $exception->cp_middlename = $value['Firstname1'];
                $exception->cp_lastname = $value['Middlename1'];
                $exception->branch = $value['Agency Branch'];
                $exception->address1 = $value['Address 1']; 
                $exception->address2 = $value['Address 2'];
                $exception->tin_no = $value['TIN'];
                $exception->reg_date = $value['Date of Registration']; 
                $exception->sec_reg_no = $value['SEC Registration No.']; 
                $exception->accreditation_no = $value['Accreditation No.']; 
                $exception->accre_expiry_date = $value['Expiry Date']; 
                $exception->mobile_no =$value['Mobile Phone']; 
                $exception->landline_no = $value['Office Phone'];
                $exception->email1 = $value['Personal Email'];
                $exception->email2 = $value['Work/Business Email']; 
                $exception->po_firstname = $value['Firstname2']; 
                $exception->po_middlename = $value['Firstname2'];
                $exception->po_lastname = $value['Middlename2'];
                $exception->branch_id = Auth::user()->branch_id;
                $exception->result = 'Empty/Invalid Data';
                $exception->save();

                $this->invalidData += 1;

            // } elseif ( (!$value['Accredited Agency Name']) 
            //         || (!$value['Accreditation No.']) || ($value['SEC Registration No.']) ) {

            } elseif ( ($value['Accredited Agency Name']) 
                    && (!$value['Accreditation No.']) && (!$value['SEC Registration No.']) ) {

                $exception = new AgenciesUploadException();
                $exception->agency_name = $value['Accredited Agency Name'];
                $exception->cp_firstname = $value['Firstname1']; 
                $exception->cp_middlename = $value['Firstname1'];
                $exception->cp_lastname = $value['Middlename1'];
                $exception->branch = $value['Agency Branch'];
                $exception->address1 = $value['Address 1']; 
                $exception->address2 = $value['Address 2'];
                $exception->tin_no = $value['TIN'];
                $exception->reg_date = $value['Date of Registration']; 
                $exception->sec_reg_no = $value['SEC Registration No.']; 
                $exception->accreditation_no = $value['Accreditation No.']; 
                $exception->accre_expiry_date = $value['Expiry Date']; 
                $exception->mobile_no =$value['Mobile Phone']; 
                $exception->landline_no = $value['Office Phone'];
                $exception->email1 = $value['Personal Email'];
                $exception->email2 = $value['Work/Business Email']; 
                $exception->po_firstname = $value['Firstname2']; 
                $exception->po_middlename = $value['Firstname2'];
                $exception->po_lastname = $value['Middlename2'];
                $exception->branch_id = Auth::user()->branch_id;
                $exception->result = 'Empty/Invalid Data';
                $exception->save();

                $this->invalidData += 1;

            } elseif ( ($value['Accredited Agency Name']) 
                    && ($value['Accreditation No.']) && ($value['SEC Registration No.']) ) {       
                    
                $this->emitter += 1;

                if ( $this->emitter == 1) {
                    $approval = new AgenciesApprovalRequest();
                    $approval->module = 'Template Upload';
                    $approval->action_type = 'upload';
                    $approval->branch_id = Auth::user()->branch_id;
                    $approval->user_id = Auth::user()->id;
                    $approval->remarks = 'Bulk Upload';
                    $approval->save();
                }

                $agency = AgenciesForApproval::create([
                    'agency_code' => substr($value['Accredited Agency Name'], 0, 5),
                    'agency_name' => $value['Accredited Agency Name'], 
                    'cp_firstname' => $value['Firstname1'],
                    'cp_middlename' => $value['Firstname1'],
                    'cp_lastname' => $value['Middlename1'],
                    'po_firstname' => $value['Firstname2'], 
                    'po_middlename' => $value['Firstname2'],
                    'po_lastname' => $value['Middlename2'],
                    'branch' => $value['Agency Branch'],
                    'address1' => $value['Address 1'],
                    'address2' => $value['Address 2'],
                    'tin_no' => $value['TIN'], 
                    'reg_date' => $value['Date of Registration'],
                    'sec_reg_no' => $value['SEC Registration No.'],
                    'landline_no' => $value['Office Phone'], 
                    'mobile_no' =>$value['Mobile Phone'], 
                    'email1' => $value['Personal Email'],
                    'email2' => $value['Work/Business Email'], 
                    'accreditation_no' => $value['Accreditation No.'],
                    'accre_expiry_date' => $value['Expiry Date'],
                    'is_active' => 1,
                ]);

                // $insertData = [];
                // $multipleValues = $request->get('createservices');
                // foreach($multipleValues as $value)
                // {
                //     $insertData['agency_id'] = $agency->id;
                //     $insertData['services'] = $value;
                //     \DB::table('agency_services')->insert($insertData);
                // }

                
                $latestId = $agency->id;
                
                if(strlen((string)$latestId) == 1) {
                    $latestId = '000'.$latestId;
                } elseif(strlen((string)$latestId) == 2) {
                    $latestId = '00'.$latestId;
                } elseif(strlen((string)$latestId) == 3) {
                    $latestId = '0'.$latestId;
                } else {
                    $latestId =  $agency->id;
                }

                AgenciesForApproval::where('id', $agency->id)->update([
                        'approval_batch_no' => $approval->id,
                        'agency_code' => substr($value['Accredited Agency Name'], 0, 3).$latestId
                    ]);

                // return $agency;   

            }

        }


    }

    public function headingRow(): int
    {
        return 7;
    }

    public function startRow(): int
    {
        return 8;
    } 

    public function conditionalSheets(): array
    {
        return [
            'Sheet1' => new AgencyImporter(),
        ];
    }


}
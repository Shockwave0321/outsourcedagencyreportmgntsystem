<?php

namespace App\Http\Middleware;
use Closure;
use Auth;


class MakerMiddleware
{
 
    function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role = 'maker') {
			return $next($request);
		}
		else {
			return redirect('/unauthorized');
		}
    }
 
}

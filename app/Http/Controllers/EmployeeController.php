<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Agency;
use App\Models\Branch;
use App\Models\Servicetype;
use App\Models\AgencyService;
use App\Models\ViewPendingRequest;
use App\Http\Requests\EmployeeStoreRequest;
use App\Repositories\EmployeeRepository;
use App\Services\EmployeeService;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Alert;

class EmployeeController extends Controller
{
 
    protected $employeeservice;

    public function __construct(EmployeeService $employeeservice)
    {
        $this->middleware('auth');
        // $this->middleware('maker');
        $this->employeeservice = $employeeservice;    
    }

    protected function guard(){
        return Auth::guard('guest');
    }

    public function getEmployees()
    {
        $branches = Branch::all();
        $services = ServiceType::all();
        $agencies = Agency::all();
        $employees = $this->employeeservice->getEmployees();
        return view('vendor.adminlte.maintenance.employee-list', 
            compact(['employees','branches','services','agencies'])
        );
    }


    public function createNewEmployee()
    {
        $branches = Branch::all();
        $services = ServiceType::all();
        $agencies = Agency::all();
        return view('vendor.adminlte.maintenance.create-employee', compact(['branches','services','agencies']));
    }

    public function insertNewEmployee(EmployeeStoreRequest $request)
    {
        $validated = $request->validated();
    
        $count = $this->employeeservice->checkDuplicateData($request);   

        if ($count > 0)
        {
            return back()->with('duplicate-entry', ' Employee already exist in the database!');
        } 
        else 
        {
            $this->employeeservice->create($request);

            Alert::success('Employee was successfuly saved!');

            return back();
        }

    }

    public function showDetails(Employee $employee)
    {
        $this->employeeservice->find($id);
    }

    public function edit(Employee $employee)
    {
        //
    }

    public function updateEmployee(Request $request)
    {

        $employee = $this->employeeservice->update($request);

        return back()->with('submit-update-for-approval','Update details successfully submitted for approval!');
    }

    public function showPendingRequests()
    {
        $pendings = ViewPendingRequest::where('user_id', Auth::user()->id)->where('status', '<>', 3)->get();
        
        return view('adminlte::maintenance.employee-pending-requests', compact('pendings'));
    }

    public function cancelRequests()
    {
        EmployeeForApproval::where('approval_batch_no', 3)->update(['status', 3]);
        
        return back()->with('cancel-request', 'Your request was cancelled.');
    }

    public function destroy(Employee $employee)
    {
        //
    }
}

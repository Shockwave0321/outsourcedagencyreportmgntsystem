<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Approver;
use App\Models\SecAdmin;
use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use Auth;
use Validator;
use Alert;

class SecurityAdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:secadmin');  
    }

    public function index()
    {
        return view('adminlte::auth.secadmin-login');
    }

    public function showDashboard()
    {
        return view('adminlte::security.dashboard');
    }

    public function showSecurityParameters()
    {
        return view('vendor.adminlte.maintenance.security-parameters');
    }

    public function getUsers()
    {
        $users = User::with('branch')->get();
        
        $approvers = Approver::with('branch')->get();

        $branches = Branch::all();

        return view('adminlte::security.users-list', compact(['users','branches','approvers']));
    }

    public function createUser()
    {
        $branches = Branch::all();

        return view('adminlte::security.create-new-user', compact('branches'));
    }

    public function insertNewUser(UserStoreRequest $data)
    {
        
        $validated = $data->validated();

        if($data['usertype'] == 'maker') {
         
            $user = new User();
            $user->name = $data['name'];
            $user->userid = $data['userid'];
            $user->email = $data['userid'].'@cbc.mail';
            $user->password = bcrypt($data['password']);
            $user->role = $data['role'];
            $user->branch_id = $data['branch'];
            $user->save();

        } else {
            
            $user = new Approver();
            $user->name = $data['name'];
            $user->userid = $data['userid'];
            $user->email = $data['userid'].'@cbc.mail';
            $user->password = bcrypt($data['password']);
            $user->role = $data['role'];
            $user->branch_id = $data['branch'];
            $user->save();

        }

        Alert::success('User was successfuly created!');
        
        return back();

    }

    public function updateUser(Request $request)
    {
        User::where('id', $request->get('edit-id'))->where('userid', $request->get('edit-userid'))->update([
            'name' => $request->get('edit-user-fullname'),
            'userid' => $request->get('edit-userid'),
            // 'password' => bcrypt($request->get('edit-user-password')),
            'email' => $request->get('edit-user-email'),
            'role' => $request->get('edit-user-role'),
            'branch_id' => $request->get('edit-user-branch'),
            ]);

        return back()->with('user-updated', 'User successfully updated!');
    }
    

}

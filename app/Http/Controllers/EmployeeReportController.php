<?php

namespace App\Http\Controllers;

use PdfReport;
use PDF;
use Auth;
use App\User;
use App\Models\Branch;
use App\Models\Agency;
use App\Models\ViewEmployee;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class EmployeeReportController extends Controller
{
    public function getReportEmployeeList()
    {
        $employees = ViewEmployee::where('branch_id', Auth::user()->branch_id)->paginate(5);

        $branch = Branch::where('id', Auth::user()->branch_id)->firstOrFail();

        $agencies = Agency::all();

        return view('vendor.adminlte.reports.report-employee-list', compact(['employees','agencies']))->with('branch', $branch);
    }

    public function getReportEmployeeListByAgency()
    {
        $employees = ViewEmployee::where('branch_id', Auth::user()->branch_id)->where('agency_name', '')->get();

        $branch = Branch::where('id', Auth::user()->branch_id)->firstOrFail();

        $agencies = Agency::all();

        return view('vendor.adminlte.reports.report-employee-list', compact(['employees','agencies']))->with('branch', $branch);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Agency;
use App\Models\Branch;
use App\Models\Servicetype;
use App\Models\AgencyService;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\EmployeeStoreRequest;
use Validator;
use Alert;
use Auth;

class DataController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('maker');
    }

    public function dashboard()
    {
        return view('adminlte::home');
    }


    public function updateEmployeeStatus(Request $request, Employee $employee)
    {
        $employee::where('id', $request->get('update-status-employeeid-'))->update('is_active', 1);
    }


    // -------------------------------//
    // AGENCY
    // -------------------------------//

    public function getAgencies()
    {
        $agencies = Agency::all();
        $servicestypes = ServiceType::all();

        return view('vendor.adminlte.maintenance.agency-list', compact(['agencies','servicestypes']));
    }

    public function insertNewAgency(Request $request)
    {
       
    }

    public function updateAgency(Request $request, Agency $agency)
    {
        $agency::where('id', $request->get('edit-agencyid'))->update([
            'agency_code' => $request->get('edit-agency-code'),
            'agency_name' => $request->get('edit-agency-name'),
            'address1' => $request->get('edit-address'),
            'tin_no' => $request->get('edit-tin-no'),
            'reg_date' => $request->get('edit-reg-date'),
            'sec_reg_no' => $request->get('edit-sec-reg-no'),
            'landline_no' => $request->get('edit-landline-no'),
            'mobile_no' => $request->get('edit-mobile-no'),
            'email1' => $request->get('edit-email1'),
            'email2' => $request->get('edit-email2'),
            'principal_owners' => $request->get('edit-owners')
        ]);

        return back()->with('success','Employee details was successfully updated!');
    }


    // -------------------------------//
    // BRANCHES
    // -------------------------------//

    public function getBranches()
    {
        $branches = Branch::all();
        return view('vendor.adminlte.maintenance.branch-list', compact('branches'));
    }

    public function insertNewbranch(Request $request)
    {
        $branch = new Branch();
        $branch->branch_code = $request->get('create-branch-code');
        $branch->branch_name = $request->get('create-branch-name');
        $branch->bank = $request->get('create-select-bankgroup');
        $branch->address = $request->get('create-address');
        $branch->save();

        return back()->with('success', 'successfully added branch.');
    }

    public function updateBranch(Request $request, Branch $branch)
    {
        $branch::where('id', $request->get('edit-branchid'))->update([
            'branch_code' => $request->get('edit-branch-code'),
            'branch_name' => $request->get('edit-branch-name'),
            'address' => $request->get('edit-address'),
            'bank' => $request->get('edit-select-bankgroup')
        ]);

        return back()->with('success','Branch details was successfully updated!');
    }

    public function showCalendarSetup()
    {
        // $branches = Branch::all();
        return view('vendor.adminlte.maintenance.calendar-setup');
    }

    public function showSecurityParameters()
    {
        // $branches = Branch::all();
        return view('vendor.adminlte.maintenance.security-parameters');
    }


}

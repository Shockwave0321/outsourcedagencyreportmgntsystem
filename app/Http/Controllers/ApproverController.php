<?php

namespace App\Http\Controllers;
use App\Models\Approver;
use App\Models\Employee;
use App\Models\ApprovalRequest;
use App\Models\EmployeeForApproval;
use App\Models\ViewEmployeeForApproval;
use App\Models\ViewPendingRequest;
use App\Models\AuditLog;
use App\Models\Agency;
use App\Models\AgencyService;
use App\Models\AgenciesApprovalRequest;
use App\Models\AgenciesForApproval;
use App\Models\ViewPendingAgenciesRequest;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class ApproverController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:approver');  
    }

    public function index()
    {

        if(Auth::user()->role == 1) {
            // maker approver
            $pendings = ViewPendingRequest::orderBy('approval_batch_no', 'desc')->get();

            return view('adminlte::approver.approver-page', compact('pendings'));

        } else {
            // HO approver
            $pendings = ViewPendingAgenciesRequest::orderBy('approval_batch_no', 'desc')->get();

            return view('adminlte::approver.ho-approver-page', compact('pendings'));

        }

    }

    public function getPendingRequests()
    {
        // $approval = EmployeeForApproval::where('approval_batch_no', 43)->get();
        // return response()->json($approval);

        $approval = EmployeeForApproval::where('approval_batch_no', 43)->get();

            foreach ($approval as $row => $value) 
            {
                $employee = new Employee();
                $employee->firstname = $value->firstname;
                $employee->save();
            }
            return response()->json($approval);

    }

    public function approveRequest(Request $request)
    {

        if($request->get('approval-action-type') == 'insert') {

             //single insert
            $approval_id = $request->get('approval-batch-no');
            $emp_no = $request->get('approval-employee-no');
            $user_id = $request->get('approval-user-id');
            $branch_id = $request->get('approval-branch-id');
            $module = $request->get('approval-module');

            $forApproval = EmployeeForApproval::where('approval_batch_no',  $approval_id)->first();

             $employee = Employee::create([
                'employee_no' => $emp_no,
                'firstname' =>  $forApproval->firstname,
                'middlename' => $forApproval->middlename,
                'lastname' => $forApproval->lastname,
                'bdate' => $forApproval->bdate,
                'civil_status' => $forApproval->civil_status,
                'gender' => $forApproval->gender,
                'mobile_no' => $forApproval->mobile_no,
                'home_landline' => $forApproval->home_landline,
                'email1' => $forApproval->email1,
                'address1' => $forApproval->address1,
                'address2' => $forApproval->address2,
                'sss_no' => $forApproval->sss_no,
                'tin_no' => $forApproval->tin_no,
                'agency_id' => $forApproval->agency_id,
                'service_type' => $forApproval->service_type,
                'reg_working_days' => $forApproval->reg_working_days,
                'reg_working_hrs' => $forApproval->reg_working_days,
                'reg_overtime_hrs' => $forApproval->reg_overtime_hrs,
                'branch_id' => $forApproval->branch_id

            ]);

            $req = ApprovalRequest::where('id', $approval_id)->update(['status' => 1]);

            $req = EmployeeForApproval::where('approval_batch_no', $approval_id)->delete();

            $log = AuditLog::create([
                    'user_id' => $user_id,
                    'branch_id' => $branch_id,
                    'module' => $module,
                    'action' => 'insert',
                ]);

            return redirect('/approver-dashboard')->with('request-approved', 'Successfully approved!');

        } elseif($request->get('approval-action-type') == 'update') {
           
            //single update       
            $approval_id = $request->get('approval-batch-no');
            $emp_no = $request->get('approval-employee-no');
            $user_id = $request->get('approval-user-id');
            $branch_id = $request->get('approval-branch-id');
            $module = $request->get('approval-module');

            $forApproval = EmployeeForApproval::where('approval_batch_no',  $approval_id)->first();

            $emp = Employee::where('employee_no', $emp_no)->update([
                    'firstname' =>  $forApproval->firstname,
                    'middlename' => $forApproval->middlename,
                    'lastname' => $forApproval->lastname,
                    'bdate' => $forApproval->bdate,
                    'civil_status' => $forApproval->civil_status,
                    'gender' => $forApproval->gender,
                    'mobile_no' => $forApproval->mobile_no,
                    'home_landline' => $forApproval->home_landline,
                    'email1' => $forApproval->email1,
                    'address1' => $forApproval->address1,
                    'address2' => $forApproval->address2,
                    'agency_id' => $forApproval->agency_id,
                    'reg_working_days' => $forApproval->reg_working_days,
                    'reg_working_hrs' => $forApproval->reg_working_days,
                    'reg_overtime_hrs' => $forApproval->reg_overtime_hrs,
                    'branch_id' => $forApproval->branch_id,
                    'service_type' => $forApproval->service_type,
                    'sss_no' => $forApproval->sss_no,
                    'tin_no' => $forApproval->tin_no
                ]);

            $req = ApprovalRequest::where('id', $approval_id)->update(['status' => 1]);

            $req = EmployeeForApproval::where('approval_batch_no', $approval_id)->delete();

            $log = AuditLog::create([
                    'user_id' => $user_id,
                    'branch_id' => $branch_id,
                    'module' => $module,
                    'action' => 'update',
                ]);

            return redirect('/approver-dashboard')->with('request-approved', 'Successfully approved!');

        } else { 
            //from upload
            $approval = EmployeeForApproval::where('approval_batch_no', $request->get('approval-batch-no'))->get();

            foreach ($approval as $row => $value) 
            {
                $employee = new Employee();
                $employee->employee_no = $value->employee_no;
                $employee->firstname = $value->firstname;
                $employee->middlename = $value->middlename; 
                $employee->lastname = $value->lastname;
                $employee->reg_working_days = $value->reg_working_days;
                $employee->reg_working_hrs = $value->reg_working_days;
                $employee->reg_overtime_hrs = $value->reg_overtime_hrs;
                $employee->bdate = $value->birthdate;
                $employee->gender = $value->gender;
                $employee->civil_status = $value->civil_status; 
                $employee->mobile_no =$value->mobile_no; 
                $employee->home_landline = $value->home_landline;
                $employee->address1 = $value->address1; 
                $employee->address2 = $value->address2; 
                $employee->email1 = $value->address2;
                $employee->email2 = $value->address2; 
                $employee->branch_id = $value->branch_id; 
                $employee->agency_id = $value->agency_id;
                $employee->service_type = $value->service_type; 
                $employee->sss_no = $value->sss_no; 
                $employee->tin_no = $value->tin_no;
                $employee->save();
            }

            $approval = EmployeeForApproval::where('approval_batch_no', $request->get('approval-batch-no'))->delete();

            return redirect('/approver-dashboard')->with('upload-approved', 'Upload was successfully approved!');

        }
        
    }

    public function showReviewEmployeeDetails($employeeNo)
    {
        $employee = ViewEmployeeForApproval::where('employee_no', $employeeNo)->firstOrFail()();

        return view('adminlte::approver.review-employee-details')->with('employee', $employee);
    }



    // HO APPROVER

    public function approvedRequestedAgency(Request $request)
    {

        $approval_id = $request->get('approval-batch-no');
        $agency_code = $request->get('approval-agency-code');
        $user_id = $request->get('approval-user-id');
        $branch_id = $request->get('approval-branch-id');
        $module = $request->get('approval-module');

        if($request->get('approval-action-type') == 'insert') {

            $forApproval = AgenciesForApproval::where('approval_batch_no',  $approval_id)->first();

            $agency = new Agency();
            $agency->agency_code = $forApproval->agency_code;
            $agency->agency_name = $forApproval->agency_name;
            $agency->address1 = $forApproval->address1;
            $agency->address2 = $forApproval->address2;
            $agency->tin_no = $forApproval->tin_no;
            $agency->reg_date = $forApproval->reg_date;
            $agency->sec_reg_no= $forApproval->sec_reg_no;
            $agency->accreditation_no= $forApproval->accreditation_no;
            $agency->accre_expiry_date= $forApproval->accre_expiry_date;
            $agency->landline_no = $forApproval->landline_no;
            $agency->mobile_no = $forApproval->mobile_no;
            $agency->email1 = $forApproval->email1;
            $agency->email2 = $forApproval->email2;
            $agency->po_firstname = $forApproval->po_firstname;
            $agency->po_middlename = $forApproval->po_middlename;
            $agency->po_lastname = $forApproval->po_lastname;
            $agency->cp_firstname = $forApproval->cp_firstname;
            $agency->cp_middlename = $forApproval->cp_middlename;
            $agency->cp_lastname = $forApproval->cp_lastname;
            $agency->save();

            $req = AgenciesApprovalRequest::where('id', $approval_id)->update(['status' => 1]);

            $req = AgenciesForApproval::where('approval_batch_no', $approval_id)->delete();

            $log = AuditLog::create([
                    'user_id' => $user_id,
                    'branch_id' => $branch_id,
                    'module' => $module,
                    'action' => 'insert',
                ]);

            return redirect('/approver-dashboard')->with('agency-upload-approved', 'Agencies was approved and added to database.');

        } elseif($request->get('approval-action-type') == 'update') {

            $forApproval = AgenciesForApproval::where('approval_batch_no',  $approval_id)->first();

            Agency::where('agency_code', $forApproval->agency_code)->update([
                'agency_name' => $forApproval->agency_name,
                'address1' => $forApproval->address1,
                'address2' => $forApproval->address2,
                'tin_no' => $forApproval->tin_no,
                'reg_date' => $forApproval->reg_date,
                'sec_reg_no' => $forApproval->sec_reg_no,
                'accreditation_no' => $forApproval->accreditation_no,
                'accre_expiry_date' => $forApproval->accre_expiry_date,
                'landline_no' => $forApproval->landline_no,
                'mobile_no' => $forApproval->mobile_no,
                'email1' => $forApproval->email1,
                'email2' => $forApproval->email2,
                'po_firstname' => $forApproval->po_firstname,
                'po_middlename' => $forApproval->po_middlename,
                'po_lastname' => $forApproval->po_lastname,
                'cp_firstname' => $forApproval->cp_firstname,
                'cp_middlename' => $forApproval->cp_middlename,
                'cp_lastname' => $forApproval->cp_lastname,
            ]);

            $req = AgenciesApprovalRequest::where('id', $approval_id)->update(['status' => 1]);

            $req = AgenciesForApproval::where('approval_batch_no', $approval_id)->delete();

            $log = AuditLog::create([
                    'user_id' => $user_id,
                    'branch_id' => $branch_id,
                    'module' => $module,
                    'action' => 'insert',
                ]);

            return redirect('/approver-dashboard')->with('request-approved', 'Successfully approved!');


        } else { // from upload

            $forApproval = AgenciesForApproval::where('approval_batch_no', $request->get('approval-batch-no'))->get();

            foreach ($forApproval as $row => $value) 
            {
                $agency = new Agency();
                $agency->agency_code = $value->agency_code;
                $agency->agency_name = $value->agency_name;
                $agency->address1 = $value->address1;
                $agency->address2 = $value->address2;
                $agency->tin_no = $value->tin_no;
                $agency->reg_date = $value->reg_date;
                $agency->sec_reg_no= $value->sec_reg_no;
                $agency->accreditation_no= $value->accreditation_no;
                $agency->accre_expiry_date= $value->accre_expiry_date;
                $agency->landline_no = $value->landline_no;
                $agency->mobile_no = $value->mobile_no;
                $agency->email1 = $value->email1;
                $agency->email2 = $value->email2;
                $agency->po_firstname = $value->po_firstname;
                $agency->po_middlename = $value->po_middlename;
                $agency->po_lastname = $value->po_lastname;
                $agency->cp_firstname = $value->cp_firstname;
                $agency->cp_middlename = $value->cp_middlename;
                $agency->cp_lastname = $value->cp_lastname;
                $agency->save();
            }

            AgenciesApprovalRequest::where('id', $approval_id)->update(['status' => 1]);

            AgenciesForApproval::where('approval_batch_no', $approval_id)->delete();

            $log = AuditLog::create([
                    'user_id' => $user_id,
                    'branch_id' => $branch_id,
                    'module' => $module,
                    'action' => 'upload',
                ]);

            return redirect('/approver-dashboard')->with('agency-upload-approved', 'Agencies was approved and added to database.');

        }


    }

    public function showReviewAgencyDetails($agencyCode)
    {
        $agency = ViewPendingAgenciesRequest::where('agency_code', $agencyCode)->firstOrFail();
       
        $services_arr = collect(AgencyService::where('agency_id', 5)->get());

        return view('adminlte::approver.review-agency-details', compact('services_arr'))->with('agency', $agency);
    }

}

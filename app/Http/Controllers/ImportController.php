<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Imports\EmployeeImporter;
use App\Models\EmployeeUploadException;
use App\Exports\UploadException;
use App\Imports\AgencyImporter;
use App\Models\AgenciesUploadException;
use App\Exports\AgencyUploadException;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\Models\Agency;
use Auth;

class ImportController extends Controller 
{
    // import employees
    public function import(Request $request)
    {

        if($request->hasFile('template-file')){

            $Import = new EmployeeImporter();

            $Import->onlySheets('Sheet1');

            Excel::import($Import, $request->file('template-file'));

            // return response()->json($Import->sheetData);

            if($Import->invalidData > 0) {

                // encapsulate this to a class later
                $clear = EmployeeUploadException::where('branch_id', Auth::user()->id)->delete();

                return Excel::download(new UploadException, 'employee-upload-exception-report.xlsx');

                return back()->with('warning-upload','Upload with exception report!');

            } else {


                return back()->with('success-upload','Employee template successfully uploaded!.');

            } 

        } else {

            return back()->with('danger-empty-file','please check file format or file content.');
        }


    }

    public function goToExceptionPage()
    {
        $exceptions = ViewExceptionEntry::where('status', 1)
                    ->where('branch_id', Auth::user()->branch_id)
                    ->orderBy('id', 'desc')->get();
                return view('adminlte::exceptions.employee-upload-exception', compact('exceptions'), 
                    ['warning-upload' =>  'Upload with exception report.']);

    }

    // import agencies
    public function importAgencies(Request $request)
    {

        if($request->hasFile('template-file')){

            $Import = new AgencyImporter();

            $Import->onlySheets('Sheet1');

            Excel::import($Import, $request->file('template-file'));

            // return response()->json($Import->sheetData);
            
            if($Import->invalidData > 0) {

                // encapsulate this to a class later
                // $clear = AgenciesUploadException::where('branch_id', Auth::user()->id)->delete();
                
                return Excel::download(new AgencyUploadException(), 'upload-exception-report.xlsx');

                return back()->with('warning-upload','Upload with exception report!');

            } else {

                return back()->with('success-upload','Employee template successfully uploaded!.');

            } 

        } 


    }
    
}

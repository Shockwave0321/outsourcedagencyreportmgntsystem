<?php

namespace App\Http\Controllers;
use App\Models\SysAdmin;
use App\Models\Bank;
use App\Models\Branch;
use App\Models\Servicetype;
use App\Models\AgencyService;
use Illuminate\Http\Request;
use Auth;

class SystemAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sysadmin');  
    }

    public function index()
    {
        return view('adminlte::auth.sysadmin-login');
    }

    public function showDashboard()
    {
        return view('adminlte::system-admin.dashboard');
    }

    // -------------------------------//
    // AGENCY
    // -------------------------------//

    public function getAgencies()
    {
        $agencies = Agency::all();
        $servicestypes = ServiceType::all();

        return view('vendor.adminlte.maintenance.agency-list', compact(['agencies','servicestypes']));
    }

    public function insertNewAgency(Request $request)
    {
        $this->agencydataservice->create($request);
        return back()->with('success', 'successfully added agency.');
    }

    public function updateAgency(Request $request, Agency $agency)
    {
        $agency::where('id', $request->get('edit-agencyid'))->update([
            'agency_code' => $request->get('edit-agency-code'),
            'agency_name' => $request->get('edit-agency-name'),
            'address1' => $request->get('edit-address'),
            'tin_no' => $request->get('edit-tin-no'),
            'reg_date' => $request->get('edit-reg-date'),
            'sec_reg_no' => $request->get('edit-sec-reg-no'),
            'landline_no' => $request->get('edit-landline-no'),
            'mobile_no' => $request->get('edit-mobile-no'),
            'email1' => $request->get('edit-email1'),
            'email2' => $request->get('edit-email2'),
            'principal_owners' => $request->get('edit-owners')
        ]);

        return back()->with('success','Employee details was successfully updated!');
    }


    // -------------------------------//
    // BRANCHES
    // -------------------------------//

    public function getBranches()
    {
        $branches = Branch::all();
        $banks = Bank::all();
        return view('vendor.adminlte.maintenance.branch-list', compact(['branches', 'banks']));
    }

    public function insertNewbranch(Request $request)
    {
        $branch = new Branch();
        $branch->pcc_code = $request->get('create-branch-code');
        $branch->branch_name = $request->get('create-branch-name');
        $branch->sol_code = $request->get('create-sol-code');
        $branch->bank = $request->get('create-select-bankgroup');
        $branch->address = $request->get('create-address');
        $branch->save();

        return back()->with('success-insert-branch', 'New branch successfully added to database.');
    }

    public function updateBranch(Request $request, Branch $branch)
    {
        $branch::where('id', $request->get('edit-branchid'))->update([
            'pcc_code' => $request->get('edit-branch-code'),
            'branch_name' => $request->get('edit-branch-name'),
            'address' => $request->get('edit-address'),
            'sol_code' => $request->get('edit-sole-code'),
            'bank' => $request->get('edit-select-bankgroup')
        ]);

        return back()->with('success-updated-branch','Branch details was successfully updated!');
    }

    public function showCalendarSetup()
    {
        // $branches = Branch::all();
        return view('vendor.adminlte.system-admin.calendar-setup');
    }

    public function showSecurityParameters()
    {
        return view('vendor.adminlte.maintenance.security-parameters');
    }

    public function getBanks()
    {
        $banks = Bank::all();

        return view('adminlte::system-admin.bank-list', compact('banks'));
    }

    public function insertNewBank(Request $request)
    {
        $bank = new Bank();
        $bank->bank_code = $request->get('create-bank-code');
        $bank->bank_name = $request->get('create-bank-name');
        $bank->save();

        return back()->with('success-insert-bank', 'New bank successfully added to database.');
    }

    public function updateBank(Request $request)
    {
        Bank::where('id', $request->get('edit-bankid'))->update([
            'bank_code' => $request->get('edit-bank-code'),
            'bank_name' => $request->get('edit-bank-name'),
        ]);

        return back()->with('success-updated-bank','Bank details was successfully updated!');
    }

}

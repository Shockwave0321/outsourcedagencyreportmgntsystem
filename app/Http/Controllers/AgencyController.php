<?php

namespace App\Http\Controllers;

use App\Services\AgencyDataService;
use App\Models\Agency;
use App\Models\ServiceType;
use App\Models\AgencyService;
use App\Http\Requests\AgencyStoreRequest;
use Illuminate\Http\Request;

class AgencyController extends Controller
{ 
    protected function guard(){
        return Auth::guard('guest');
    }

    protected $agencydataservice;

    public function __construct(AgencyDataService $agencydataservice)
    {
        $this->agencydataservice = $agencydataservice;
    }

    public function getAgencies()
    {
        $agencies = $this->agencydataservice->getAgencies();
        $servicestypes = ServiceType::all();
        $agencyServices = AgencyService::all();
        return view('vendor.adminlte.maintenance.agency-list', compact(['agencies','servicestypes', 'agencyServices']));
    }

    public function createAgency()
    {
        $servicestypes = ServiceType::all();

        $agencyServices = AgencyService::all();

        return view('adminlte::approver.create-agency', compact(['servicestypes', 'agencyServices']));
    }

    public function insertNewAgency(AgencyStoreRequest $request)
    {
        $validated = $request->validated();

        $count = $this->agencydataservice->checkDuplicateData($request); 

        $countApproval = $this->agencydataservice->checkDuplicateDataOnCurrentApproval($request);
        
        if($count) {

            return back()->with('duplicate-agency-name', 'Agency Name already exist in the database!');

        } else {

            if($countApproval) {

                return back()->with('duplicate-agency-name', 'Agency Name already exist in the approval request.');

            } else {

                $this->agencydataservice->create($request);

                return back()->with('success-insert-agency-for-approval', 'successfully added agency for approval.');

            }

        }

    }

    public function updateAgency(Request $request, Agency $agency)
    {
        $this->agencydataservice->update($request);

        return back()->with('success-update-agency-for-approval','Agency successfully sent for approval!');
    }


}

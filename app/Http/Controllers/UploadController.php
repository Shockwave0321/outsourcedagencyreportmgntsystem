<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\DuplicateEntryEmployee;
use Illuminate\Http\Request;
use App\Imports\ImportEmployeeExcel;
// use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Excel;

class UploadController extends Controller
{

    public function importExportView()
    {
       return view('vendor.adminlte.maintenance.upload-template-employees');
    }
   
    // save report to excel
    public function exportFile($type){

        $employees = Employee::get()->toArray();

        return \Excel::create('hdtuto_demo', function($excel) use ($employees) {

            /** Set the spreadsheet title, creator, and description */
            $excel->setTitle( 'your_file_title' );
            $excel->setCreator( 'creator_name' )->setCompany( 'company_name' );
            $excel->setDescription( 'This is the description of the exported excel file' );

            $excel->sheet('employee-sheet', function($sheet) use ($employees)

            {

                /** Manipulating Rows */
                $sheet->row( '1', array(
                    'Employee Timesheets'
                ) );

                $sheet->fromArray($employees);

                 // password protect
                $sheet->protect('password', function(\PHPExcel_Worksheet_Protection $protection) {
                    $protection->setSort(true);
                });
            });

        })->download($type);

    }


   
    // upload excel
    public function importFile(Request $request){

        if($request->hasFile('template-file')){

            $path = $request->file('template-file')->getRealPath();

            $data = \Excel::load($path)->first();

            if($data->count()){

                foreach ($data as $key => $value) {

                    $latest_id = Employee::orderBy('id', 'desc')->first();

                    if(strlen((string)$latest_id) == 1) {
                        $latest_id = '000'.$latest_id;
                    } elseif(strlen((string)$latest_id) == 2) {
                        $latest_id = '00'.$latest_id;
                    } elseif(strlen((string)$latest_id) == 3) {
                        $latest_id = '0'.$latest_id;
                    } else {
                        $latest_id = $latest_id;
                    }

                    $duplicate = Employee::where('firstname', $value->firstname)
                        ->where('lastname', $value->lastname)
                        ->where('bdate', $value->bdate)
                        ->where('sss_no', $value->sss_no)
                        ->count();

                    if(($duplicate) || (!$value->firstname) || (!$value->lastname)){

                        $employee = new DuplicateEntryEmployee();
                        $employee->module = 'excel template';
                        $employee->firstname = $value->firstname;
                        $employee->middlename = $value->middlename;
                        $employee->lastname = $value->lastname;
                        $employee->user_id = Auth::user()->id;
                        $employee->branch_id = Auth::user()->branch_id;
                        $employee->save();

                    } else {
                    
                        $arr[] = [
                            'employee_no' => $latest_id,
                            'firstname' => $value->firstname, 
                            'middlename' => $value->middlename, 
                            'lastname' => $value->lastname,
                            'shift_schedule' => $value->shift_schedule, 
                            'bdate' => $value->bdate, 
                            'gender' => $value->gender,
                            'civil_status' => $value->civil_status, 
                            'mobile_no' => $value->mobile_no, 
                            'home_landline' => $value->home_landline,
                            'address1' => $value->address1, 
                            'address2' => $value->address2, 
                            'email1' => $value->email1,
                            'email2' => $value->email2, 
                            'branch_id' => $value->branch_id, 
                            'agency_id' => $value->agency_id,
                            'service_type' => $value->service_type, 
                            'sss_no' => $value->sss_no, 
                            'tin_no' => $value->tin_no,
                            'is_active' => 1
                        ];
                    }

                   
                }

                if(!empty($arr)){

                    \DB::table('employees')->insert($arr);

                    return back()->with('success-upload','Successfully uploaded!');

                }

            }

        }

        return back()->with('duplicate-upload','No data was uploaded! Please check the exception report.');

    } 


}

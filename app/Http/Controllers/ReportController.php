<?php

namespace App\Http\Controllers;

use PdfReport;
use PDF;
use Auth;
use App\User;
use App\Models\Agency;
use App\Models\Employee;
use App\Models\Timesheet;
use App\Models\Branch;
use App\Models\ApprovalRequest;
use App\Models\ExceptionEntry;
use App\Models\ViewExceptionEntry;
use App\Models\ViewEmployeeTimesheet;

use App\Exports\TimesheetExport;
use App\Exports\UploadException;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('maker');
    }

    protected function guard(){
        return Auth::guard('guest');
    }

    

    public function getReportEmployeeTimesheet()
    {
        $employee = Employee::where('id', 6)->first();

        $timesheets = ViewEmployeeTimesheet::where('employee_id', 6)->paginate(15);

        $personnels = Employee::where('branch_id', Auth::user()->branch_id)->get();

        $agency = Agency::where('id', 6)->firstOrFail();

        $branch = Branch::where('id', Auth::user()->branch_id)->firstOrFail();

        return view('adminlte::reports.display.timesheet-per-personnel', compact(['timesheets','personnels']))
                ->with('employee', $employee)->with('branch', $branch)->with('agency', $agency);

    }

    public function generateTimesheetConso()
    {
        $employee = Employee::where('id', 1)->first();

        $timesheets = ViewEmployeeTimesheet::paginate(10);

        $branch = Branch::where('id', Auth::user()->branch_id)->firstOrFail();

        return view('adminlte::reports.display.timesheet-conso', compact('timesheets'))->with('employee', $employee)->with('branch', $branch);
    }


    public function getEmployees(Request $request, $agency_id)
    {
        $employees = Employee::with('agencies')->where('agency_id', $agency_id)->get();
        return view('vendor.adminlte.reports.report-employee-timesheet', compact(['timesheets', 'employees']));
    }


    //pdf generator
    public function displayReport(Request $request) {
        // Retrieve any filters
        $fromDate = $request->get('date-report-range');
        $toDate = $request->get('date-report-range');
        $perAgency = $request->get('select-report-agency');

        // Report title
        $title = 'Employee Timesheet';

        // For displaying filters description on header
        $meta = [
            'For the Month of' => $fromDate,
            'Per Agency' => $perAgency
        ];

        $queryBuilder =  ViewEmployeeTimesheet::select(\DB::raw('CONCAT(lastname, ", ", firstname) AS fullname'), 'time_in','time_out')->orderBy('timesheet_index');
                        // ->join('timesheets', 'employees.id', '=', 'timesheets.employee_id');
                        // ->where('activated', '=', '1')
                        // ->where('users_groups.group_id', '=', $group)
                        // ->orderBy('last_name')
                        // ->lists('full_name', 'id');

        // Set Column to be displayed
        $columns = [
            'Name' => 'fullname',
            'Time In' => 'time_in', // if no column_name specified, this will automatically seach for snake_case of column name (will be registered_at) column from query result
            'Time Out' => 'time_out',
            'Total Hours' => function($result) {
                // return ($result->time_in > 100000) ? 'Rich Man' : 'Normal Guy';
                $timeIn = strtotime($result->time_in);
                $timeOut = strtotime($result->time_out);
                $totalHrs = abs($timeIn - $timeOut) / 3600;
                $result = $totalHrs;

                return sprintf('%0.2f', $result);
            }
        ];

        /*
            Generate Report with flexibility to manipulate column class even manipulate column value (using Carbon, etc).

            - of()         : Init the title, meta (filters description to show), query, column (to be shown)
            - editColumn() : To Change column class or manipulate its data for displaying to report
            - editColumns(): Mass edit column
            - showTotal()  : Used to sum all value on specified column on the last table (except using groupBy method). 'point' is a type for displaying total with a thousand separator
            - groupBy()    : Show total of value on specific group. Used with showTotal() enabled.
            - limit()      : Limit record to be showed
            - make()       : Will producing DomPDF / SnappyPdf instance so you could do any other DomPDF / snappyPdf method such as stream() or download()
        */
        return PdfReport::of($title, $meta, $queryBuilder, $columns)
                        ->editColumn('Registered At', [
                            'displayAs' => function($result) {
                                return $result->created_at->format('d M Y');
                            }
                        ])
                        ->editColumn('Total Balance', [
                            'displayAs' => function($result) {
                                return thousandSeparator($result->balance);
                            }
                        ])
                        ->editColumns(['Total Balance', 'Status'], [
                            'class' => 'right bold'
                        ])
                        ->showTotal([
                            'Total Balance' => 'point' // if you want to show dollar sign ($) then use 'Total Balance' => '$'
                        ])
                        ->limit(20)
                        ->stream(); // or download('filename here..') to download pdf
    }

    public function generatePDF(){
        $timesheets = Timesheet::all();
      
        $pdf = PDF::loadView('vendor.adminlte.maintenance.security-parameters');
        return $pdf->download('timesheet.pdf');

    }

    public function showEmployeeUploadException()
    {
        $exceptions = ViewExceptionEntry::where('status', 1)->get();
        return view('adminlte::exceptions.employee-upload-exception', compact('exceptions'));
    }

    // employee timesheet consolidation report
    public function exportTimesheetConso()
    {
        return Excel::download(new TimesheetExport, 'employee-timesheet-consolidation.xlsx');
    }

    public function exportExceptionFromUpload()
    {
        return Excel::download(new UploadException, 'upload-exception-report.xlsx');
        
    }


}

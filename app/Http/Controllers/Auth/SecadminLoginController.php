<?php
namespace App\Http\Controllers\Auth;
use App\Models\SecAdmin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class SecadminLoginController extends Controller
{

    protected function guard(){
        return Auth::guard('secadmin');
    }

    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest:secadmin', ['except' => ['logout']]);
    }

    public function loginSecAdmin(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'userid'   => 'required',
        'password' => 'required|min:6'
      ]);

        if(Auth::guard('secadmin')->attempt(['userid'=> $request->userid, 'password'=> $request->password], $request->remember)){
           
            // return redirect()->intended(route('secadmin.login.submit'));
            return redirect('/secadmin-dashboard'); 
            
        } else {

            return redirect()->back()->withInput($request->only('userid', 'remember'))->withErrors([
                'approve' => 'These credentials do not match our records.',
            ]);
        }

    }

    public function logoutSecadmin()
    {
        Auth::guard('secadmin')->logout();
        return redirect()->route('secadmin-login');
    }
}
<?php
namespace App\Http\Controllers\Auth;
use App\Models\Approver;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class ApproverLoginController extends Controller
{

    protected function guard(){
        return Auth::guard('approver');
    }

    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest:approver', ['except' => ['logout']]);
    }

    public function loginApprover(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'userid'   => 'required',
        'password' => 'required|min:6'
      ]);

        if(Auth::guard('approver')->attempt(['userid'=> $request->userid, 'password'=> $request->password], $request->remember)){
           
            return redirect()->route('approver.dashboard'); 
            
        } else {

            return redirect()->back()->withInput($request->only('userid', 'remember'))->withErrors([
                'approve' => 'These credentials do not match our records.',
            ]);
        }

    }

    public function logoutApprover()
    {
        Auth::guard('approver')->logout();
        return redirect()->route('approver-login');
    }
}
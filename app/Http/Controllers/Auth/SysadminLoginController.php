<?php
namespace App\Http\Controllers\Auth;
use App\Models\Sysadmin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class SysadminLoginController extends Controller
{

    protected function guard(){
        return Auth::guard('sysadmin');
    }

    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest:sysadmin', ['except' => ['logout']]);
    }

    public function loginSysAdmin(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'userid'   => 'required',
        'password' => 'required|min:6'
      ]);

        if(Auth::guard('sysadmin')->attempt(['userid'=> $request->userid, 'password'=> $request->password], $request->remember)){
           
            // return redirect()->intended(route('sysadmin.login.submit'));
            return redirect('/sysadmin-dashboard'); 
            
        } else {

            return redirect()->back()->withInput($request->only('userid', 'remember'))->withErrors([
                'approve' => 'These credentials do not match our records.',
            ]);
        }

    }

    public function logoutSysadmin()
    {
        Auth::guard('sysadmin')->logout();
        return redirect()->route('sysadmin-login');
    }
}
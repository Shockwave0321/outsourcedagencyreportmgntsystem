<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\ViewEmployeeTimesheet;
use App\Models\Branch;
use App\Models\Timesheet;
use Auth;
use Alert;

class TimesheetManagerController extends Controller
{
    public function showTimesheetList()
    {
        $personnels = Employee::where('branch_id', Auth::user()->branch_id)->get();

        $timesheets = ViewEmployeeTimesheet::where('branch_id', Auth::user()->branch_id)->get();

        $branch = Branch::where('id', Auth::user()->branch_id)->firstOrFail();

        return view('adminlte::maintenance.manage-timesheet', compact(['timesheets','personnels']))->with('branch', $branch);
    }

    public function insertNewTimeLog(Request $request)
    {
        Timesheet::create([
            'employee_id' => $request->get('select-personnel'),
            'am_time_in' => $request->get('timelog-am').' '.$request->get('am-time-in'),
            'am_time_out' => $request->get('timelog-am').' '.$request->get('am-time-out'),
            'pm_time_in' => $request->get('timelog-pm').' '.$request->get('pm-time-in'),
            'pm_time_out' => $request->get('timelog-pm').' '.$request->get('pm-time-out'),
            'ot_time_in' => $request->get('timelog-ot').' '.$request->get('ot-time-in'),
            'ot_time_out' => $request->get('timelog-ot').' '.$request->get('ot-time-out'),
        ]);


        Alert::success('Time Log was successfuly created for approval!');
    
        return back();
    
    }

    public function approveOTReg($id)
    {
        Timesheet::where('id', $id)->update(['is_approve_ot_reg' => 1]);
    }

}

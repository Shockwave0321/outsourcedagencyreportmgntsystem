<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgencyStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'createservices' => 'required',
            'create-agency-name' => 'required',
            'create-tin-no' => 'required',
            'create-accre-no' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'createservices.required' => 'Enter atleast 1 service!',
            'create-agency-name.required' => 'Name of agency  is required!',
            'create-tin-no.required' => 'Agency must have TIN Number!',
            'create-accre-no.required' => 'Agency must have an Accreditation Number!!'
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'create-firstname' => 'required',
            'create-lastname' => 'required',
            'create-sssno' => 'required',
            'create-bdate' => 'required',
            'create-branch' => 'required',
            'create-agency' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'create-firstname.required' => 'Firstname is required!',
            'create-lastname.required' => 'Lastname is required!',
            'create-sssno.required' => 'SSS Number is required!',
            'create-bdate.required' => 'Birth Date is required!',
            'create-branch.required' => 'Branch is required!',
            'create-agency.required' => 'Agency is required!'
        ];
    }
}

<?php
 
 namespace  App\Repositories;

 use App\Models\Employee;
 
 class EmployeeRepository 
 {

    protected $employee;

    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function all()
    {
        return $this->employee->all();
    }

    public function create($attributes)
    {
        return $this->employee->create($attributes);
    }

    public function update()
    {
        return $this->employee->update();
    }

    public function delete($id)
    {
        return $this->employee->find($id)->delete();
    }

    public function find($id)
    {
        return $this->employee->find($id);
    }

 }
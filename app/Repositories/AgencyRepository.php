<?php
 
 namespace  App\Repositories;

 use App\Models\Agency;
 
 class AgencyRepository 
 {

    protected $agency;

    public function __construct(agency $agency)
    {
        $this->agency = $agency;
    }

    public function all()
    {
        return $this->agency->all();
    }

    public function create($attributes)
    {
        return $this->agency->create($attributes);
    }

    public function update()
    {
        return $this->agency->update();
    }

 }